function createUserListItem(user) {
	var $tr = $("<tr></tr>");
	var $td_username = $("<td class='hide'>" + user.username + "</td>");
	var $td_name = $("<td>" + user.name + "</td>");
	var $td_phone = $("<td>" + user.phone + "</td>");
	var $td_privilege = $("<td></td>");
	for (i = 0; i < user.roles.length; i++){
		var $privilege = $("<span class='label label-info table-label'>" + user.roles[i] + "</span>");
		$td_privilege.append($privilege);
	}
	$tr.append($td_username);
	$tr.append($td_name);
	$tr.append($td_phone);
	$tr.append($td_privilege);
	return $tr;
}