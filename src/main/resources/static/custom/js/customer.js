function createCustomerItem(name, phone, birthday) {
	'use strict';
	var $tr = $("<tr></tr>");
	var $td_name = $("<td>" + name + "</td>");
	var $td_phone = $("<td>" + phone + "</td>");
	var $td_birthday = $("<td>" + birthday + "</td>");
	$tr.append($td_name);
	$tr.append($td_phone);
	$tr.append($td_birthday);
	return $tr;
}