function requestRegisterCode() {
	var $errorLabel = $("#error");
	var phone = $("#phone").val();
	if (phone == null) {
		$errorLabel.text("手机号码不能为空");
		return false;
	}
		
	var regPhone = /^1\d{10}$/;
	if (regPhone.test(phone) == false) {
		$errorLabel.text("手机号码不正确");
		return false;
	}
	
	var overlay = loadingToast("发送中");
	$.post("/register/sendCode/" + phone, {}, function (data) {
		//alert(JSON.stringify(data));
		if (data.returnState == "OK") {
			var registerCode = data.returnData.code;
			$("#registerCode").val(registerCode);
			updateSuccessToast(overlay, "发送成功");
		} else {
			updateErrorToast(overlay, "发送失败");
			$errorLabel.text(data.returnMsg);
		}
	});
}

function registerCheck() {
	var $errorLabel = $("#error");
	$errorLabel.text("");
	/* username check */
	var user = $("#username");
	var regUsername=/^[a-zA-Z][a-zA-Z0-9]{5,14}$/;
	if (regUsername.test(user.val()) == false) {
		$errorLabel.text("用户名必须为6到15位数字、字母组成");
		return false;
	}
	
	/* pwd check */
	var pwd = $("#pwd");
	var regPwd = /^[a-zA-Z0-9]{4,10}$/;
	if (regPwd.test(pwd.val()) == false) {
		$errorLabel.text("密码不能包含非法字符，长度4-10之间")
		return false;
	}
	
	var rePwd = $("#repwd");
	if (rePwd.val() != pwd.val()) {
		$errorLabel.text("两次输入的密码不一致");
		return false;
	}
	
	var corpName = $("#corpName");
	if (corpName.val().length <= 0) {
		$errorLabel.text("公司名称不能为空");
		return false;
	}
	
	var corpAddress = $("#corpAddress");
	if (corpAddress.val().length <= 0) {
		$errorLabel.text("公司地址不能为空");
		return false;
	}
	
	var name = $("#name");
	if (name.val().length <= 0) {
		$errorLabel.text("联系人姓名不能为空");
		return false;
	}
	
	var phone = $("#phone");
	var regPhone = /^1\d{10}$/;
	if (regPhone.test(phone.val()) == false) {
		$errorLabel.text("手机号码不正确");
		return false;
	}
	
	var email = $("#email");
	var regMail = /^\w+@\w+(\.[a-zA-Z]{2,3}){1,2}$/;
	if (regMail.test(email.val()) == false) {
		$errorLabel.text("邮箱格式不正确");
		return false;
	}
	
	return true;
}