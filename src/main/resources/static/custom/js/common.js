function checkUsername(username) {
	var regUsername=/^[a-zA-Z][a-zA-Z0-9]{5,14}$/;
	return regUsername.test(username);
}

function checkPassword(pwd) {
	var regPwd = /^[a-zA-Z0-9]{4,10}$/;
	return regPwd.test(pwd);
}

function checkPhone(phone) {
	var regPhone = /^1\d{10}$/;
	return regPhone.test(phone);
}

function checkEmail(email) {
	var regMail = /^\w+@\w+(\.[a-zA-Z]{2,3}){1,2}$/;
	return regMail.test(email);
}

function historyback() {
	if (document.referrer == "" || document.referrer.search("/login") != -1) {
		window.location = "/index"
	} else {
		javascript :history.back(-1);
	}
}

function add_caret(name, price, number, introduction) {
	 var li = $("<li></li>");
	 var img_div = $('<div class="shopcart-img"> \
			 			<img src="/dist/img/default-50x50.gif"/> \
					 </div>');
	 var title_div = $('<p class="shopcart-title" th:inline="text">' + name + '<span class="label label-warning pull-right">' + price + ' x' + number + '</span></p>');
	 var intr_div = $('<span th:text="${caret.product.introduction}">' + introduction + '</span>');
	 var info_div = $("<div class='shopcart-info'></div>");
	 info_div.append(title_div);
	 info_div.append(intr_div);
	 var item_div = $("<div class='shopcart-item'></div>");
	 item_div.append(img_div);
	 item_div.append(info_div);
	 li.append(item_div);
	 
	 $("#caretmenu").append(li);
	 var num = $("#caretmenu").children().length;
	 $("#caret_number").text("购物车中有" + num + "件商品");
	 $("#caret_label").text(num);
}

$.fn.serializeObject = function()   
{   
   var o = {};   
   var a = this.serializeArray();   
   $.each(a, function() {   
       if (o[this.name]) {   
           if (!o[this.name].push) {   
               o[this.name] = [o[this.name]];   
           }   
           o[this.name].push(this.value || '');   
       } else {   
           o[this.name] = this.value || '';   
       }   
   });   
   return o;   
};

function loadingToast(msg, duration) { 
    var opts = { 
        lines: 13, 
        // The number of lines to draw 
        length: 11, 
        // The length of each line 
        width: 5, 
        // The line thickness 
        radius: 17, 
        // The radius of the inner circle 
        corners: 1, 
        // Corner roundness (0..1) 
        rotate: 0, 
        // The rotation offset 
        color: '#FFF', 
        // #rgb or #rrggbb 
        speed: 1, 
        // Rounds per second 
        trail: 60, 
        // Afterglow percentage 
        shadow: false, 
        // Whether to render a shadow 
        hwaccel: false, 
        // Whether to use hardware acceleration 
        className: 'spinner', 
        // The CSS class to assign to the spinner 
        zIndex: 2e9, 
        // The z-index (defaults to 2000000000) 
        top: 'auto', 
        // Top position relative to parent in px 
        left: 'auto' // Left position relative to parent in px 
    }; 
    var target = document.createElement("div"); 
    document.body.appendChild(target); 
    var spinner = new Spinner(opts).spin(target); 
    var overlay = null;
    if (duration == 0) {
    	overlay = iosOverlay({ 
            text: msg, 
            spinner: spinner 
        }); 
    } else {
	    overlay = iosOverlay({ 
	        text: msg, 
	        duration: duration, 
	        spinner: spinner 
	    });
    }
    return overlay; 
};

function updateSuccessToast(overlay, msg) {
	overlay.update({
        text: msg, 
        icon: "/plugins/iOS-Overlay/img/check.png" 
    });
	window.setTimeout(function() { 
        overlay.hide(); 
    }, 2e3); 
	return true;
}

function updateErrorToast(overlay, msg) {
    overlay.update({ 
        text: msg, 
        icon: "/plugins/iOS-Overlay/img/cross.png" 
    });
    window.setTimeout(function() { 
        overlay.hide(); 
    }, 2e3); 
    return true;
}

function successToast(msg) { 
    iosOverlay({
        text: msg, 
        duration: 2e3, 
        icon: "/plugins/iOS-Overlay/img/check.png" 
    }); 
    return true; 
};

function errorToast(msg) { 
    iosOverlay({ 
        text: msg, 
        duration: 2e3, 
        icon: "/plugins/iOS-Overlay/img/cross.png" 
    }); 
    return true; 
};

function loadingToSucessToast(e) { 
    var opts = { 
        lines: 13, 
        // The number of lines to draw 
        length: 11, 
        // The length of each line 
        width: 5, 
        // The line thickness 
        radius: 17, 
        // The radius of the inner circle 
        corners: 1, 
        // Corner roundness (0..1) 
        rotate: 0, 
        // The rotation offset 
        color: '#FFF', 
        // #rgb or #rrggbb 
        speed: 1, 
        // Rounds per second 
        trail: 60, 
        // Afterglow percentage 
        shadow: false, 
        // Whether to render a shadow 
        hwaccel: false, 
        // Whether to use hardware acceleration 
        className: 'spinner', 
        // The CSS class to assign to the spinner 
        zIndex: 2e9, 
        // The z-index (defaults to 2000000000) 
        top: 'auto', 
        // Top position relative to parent in px 
        left: 'auto' // Left position relative to parent in px 
    }; 
    var target = document.createElement("div"); 
    document.body.appendChild(target); 
    var spinner = new Spinner(opts).spin(target); 
    var overlay = iosOverlay({ 
        text: "加载中", 
        spinner: spinner 
    }); 
 
    window.setTimeout(function() { 
        overlay.update({ 
            icon: "/plugins/iOS-Overlay/img/check.png", 
            text: "操作成功" 
        }); 
    }, 
    3e3); 
 
    window.setTimeout(function() { 
        overlay.hide(); 
    }, 
    5e3); 
 
    return false; 
};