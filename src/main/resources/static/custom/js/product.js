if (typeof jQuery === 'undefined') {
throw new Error('Custom JS requires jQuery')
}

+function($) {
	'use strict';
	$("#createProductGroup").on("click", function() {
		$(".singleProductInfo").hide();
		$(".groupProductInfo").show();
	});
	$("#createProductSingle").on("click", function() {
		$(".singleProductInfo").show();
		$(".groupProductInfo").hide();
	});
	
}(jQuery);

function incrProductNum(event) {
	var btn = $(event.target);
	var numInput = btn.closest("div").children("input");
	numInput.val(parseInt(numInput.val()) + 1);
}

function descProductNum(event) {
	var btn = $(event.target);
	var numInput = btn.closest("div").children("input");
	var num = parseInt(numInput.val()) - 1;
	if (num < 1) {
		num = 1;
	}
	numInput.val(num);
}

function deleteProductItem(event) {
	var btn = $(event.target);
	btn.closest("li").remove();
}

function create_editable_product_item(product) {
	var item = $("<li class='item'></li>")
	item.html('<div class="product-img"> \
					<img src="/dist/img/default-50x50.gif" /> \
				</div> \
				<div class="product-info"> \
					<div class="hide"> \
						<p name="id"></p> \
					</div> \
					<div class="hide"> \
						<p name="type"></p> \
					</div> \
					<div class="product-title"> \
						<a href="#" name="name"></a> \
						<span class="label label-info" name="identifier"></span> \
						<a><span name="deletebtn" class="fa fa-trash-o pull-right"></span></a> \
					</div> \
					<span name="introduction" class="product-description"></span> \
					<div style="display:inline;"> \
						<span>￥</span> \
						<span name="price" class="label label-warning"></span> \
					</div> \
					<div class="btn-group pull-right"> \
						<button name="minusnumber" type="button" class="btn btn-default btn-xs number-minus"><i class="fa fa-minus"></i></button> \
						<input name="number" class="btn btn-default btn-xs number" style="width:34px;" value="0"> \
						<button name="plusnumber" type="button" class="btn btn-default btn-xs number-plus"><i class="fa fa-plus"></i></button> \
					</div> \
				</div>');
	item.find("[name='id']").text(product.id);
	item.find("[name='name']").text(product.name);
	item.find("[name='identifier']").text(product.identifier);
	item.find("[name='introduction']").text(product.introduction);
	item.find("[name='price']").text(product.price);
	item.find("[name='plusnumber']").on("click", incrProductNum);
	item.find("[name='minusnumber']").on("click", descProductNum);
	item.find("[name='deletebtn']’").on("click", deleteProductItem);
	if (product.type == 2) {
		item.find("[name='type']").text("group");
	} else {
		item.find("[name='type']").text("single");
	}
	return item;
}