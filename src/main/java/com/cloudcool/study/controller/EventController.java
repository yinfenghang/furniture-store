package com.cloudcool.study.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.entity.Schedule;
import com.cloudcool.study.service.ScheduleService;
import com.cloudcool.study.service.ServiceResult;

@Controller
public class EventController extends CRMBaseController {
	@Autowired
	private ScheduleService scheduleService;

	@GetMapping("/calendar")
	public String calendar() {
		return "/calendar";
	}
	
	@PostMapping("/schedule/create")
	public @ResponseBody AjaxResponse createSchedule(@RequestBody Event event) {
		String check = Event.isValid(event);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		ServiceResult<Schedule> result;
		try {
			result = scheduleService.createSchedule(event.getTitle(), event.getContent(), 
						event.getStartTime(), event.getEndTime(), event.getUsers(), event.isNotify());
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, "创建的日程失败，请重试");
			}
		} catch (RuntimeException ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}
		
		List<Schedule> schedules = result.getData();
		if (schedules == null || schedules.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "创建日程失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("schedules", schedules);
	}
	
	@RequestMapping("calendar_detail")
	public String eventDetail() {
		return "calendar_detail";
	}
	
	@RequestMapping("event_todo")
	public String eventTodo() {
		return "event_todo";
	}
	
	public static class Event {
		private String title;
		private Date startTime;
		private Date endTime;
		private List<String> users = new ArrayList<>();
		private String content;
		private boolean notify;
		
		private static String isValid(Event event) {
			if (event == null) {
				return "未解析到有效数据";
			}
			
			if (event.getTitle() == null || event.getTitle().isEmpty()) {
				return "事件标题不能为空";
			}
			
			if (event.getStartTime() == null || event.getEndTime() == null) {
				return "起始时间和结束时间不能为空";
			}
			
			if (event.getUsers() == null || event.getUsers().size() <= 0) {
				return "至少要有一个参与人";
			}
			
			if (event.getContent() == null || event.getContent().isEmpty()) {
				return "事件内容不能为空";
			}
		
			return null;
		}
		
		public Event () {}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public Date getStartTime() {
			return startTime;
		}
		public void setStartTime(Date startTime) {
			this.startTime = startTime;
		}
		public Date getEndTime() {
			return endTime;
		}
		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}
		public List<String> getUsers() {
			return users;
		}
		public void setUsers(List<String> users) {
			this.users = users;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public boolean isNotify() {
			return notify;
		}
		public void setNotify(boolean notify) {
			this.notify = notify;
		}
		@Override
		public String toString() {
			return "Event [title=" + title + ", startTime=" + startTime + ", endTime=" + endTime + ", users=" + users
					+ ", content=" + content + ", notify=" + notify + "]";
		}
	}
}
