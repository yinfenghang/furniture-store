package com.cloudcool.study.controller;
import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.Product.Size;
import com.cloudcool.study.entity.ProductBundle;
import com.cloudcool.study.entity.ProductBundle.ProductItem;
import com.cloudcool.study.entity.ProductSingle;
import com.cloudcool.study.entity.TenantAgent;
import com.cloudcool.study.service.ProductService;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.service.TenantService;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Utils;

/*
 * URL分配列表
 * GET /product_create 获取创建产品页面
 * POST /product/create 请求创建产品
 * GET /product_list 请求产品列表页面
 * GET /product/list/{start}/{size} ajax请求产品列表
 * GET /product/search/{content} 查询商品
 * GET /product_detail/{identifier} 查看指定商品的商品详情
 * GET /product/delete/{identifier} 删除商品
 * GET /product/update/{identifier} 获取更新商品信息页面
 * POST /product/update 更新商品信息
 */
@Controller
public class ProductController extends CRMBaseController {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private ProductService productService;
	@Autowired
	private TenantService tenantService;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        /* 注册字符串、时间转换函数 */
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        /* 注册字符串、尺寸转换函数 */
        binder.registerCustomEditor(Size.class, new CustomSizeEditor());
    }
	
	@PostMapping("/product/create")
	public @ResponseBody AjaxResponse createProduct(@RequestBody ProductDetail details) {
		String check = ProductDetail.isValid(details, false);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		Product product = details.toProduct();
		if (product == null) {
			return new AjaxResponse(ReturnState.ERROR, "解析数据失败，请刷新后重试");
		}

		String username = UserLoginDao.getSelfUsername();
		product.setCreater(username);
		
		ServiceResult<Product> result = productService.createProduct(product);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		product = result.getData(0);
		if (product == null) {
			return new AjaxResponse(ReturnState.ERROR, "创建商品失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("identifier", product.getIdentifier());
	}
	
	@GetMapping("product_list")
	public String queryProduct() {
		return "product_list";
	}
	
	@GetMapping("/product/list/{start}/{size}")
	public @ResponseBody AjaxResponse getProductListPageable(@PathVariable("start") int start, @PathVariable("size") int size) {
		if (start < 0 || size <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "请求参数错误, start = " + start + ", size = " + size);
		}
		String tenantId = userLoginDao.findTenantId();
		
		/* get pageable product list */
		ServiceResult<Product> result = productService.getProductsPageable(tenantId, start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Product> products = result.getData();
		if (products == null || products.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到有效商品信息");
		}
		
		products = Utils.filterViewFields(products, result.getViewLimit("product"));
		
		/* count products */
		int count = 0;
		ServiceResult<Product> countResult = productService.countProduct(tenantId);
		if (!countResult.isFailed()) {
			count = countResult.getResultNum();
		}
		return AjaxResponse.getInstanceByResult(true).addAttribute("products", products).addAttribute("count", count);
	}
	
	@GetMapping("/product/search/{content}")
	public @ResponseBody AjaxResponse searchProduct(@PathVariable("content") String content) {
		if (content == null || content.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "请输入要搜索商品的编码或名称");
		}
		
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Product> result = productService.searchByIdentifierOrName(tenantId, content);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Product> products = result.getData();
		if (products == null || products.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未搜索到相关商品，试试其他关键字吧");
		}
		
		products = Utils.filterViewFields(products, result.getViewLimit("product"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("products", products);
	}
	
	@GetMapping("/product_detail/{identifier}")
	public String productDetail(Model model, @PathVariable("identifier") String identifier) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Product> result = productService.getProductByIdentifier(tenantId, identifier);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "product_detail";
		}
		
		Product product = result.getData(0);
		if (product == null) {
			model.addAttribute("error", "未查询到指定商品");
			return "product_detail";
		}
		
		Map<String, Boolean> limits = result.getViewLimit("product");
		if (product.isSingle()) {
			product = Utils.filterViewFields((ProductSingle)product, limits);
			model.addAttribute("product", (ProductSingle)product);
		} else {
			product = Utils.filterViewFields((ProductBundle)product, limits);
			model.addAttribute("product", (ProductBundle)product);
		}
		model.addAttribute("limit", limits);
		return "product_detail";
	}
	
	@GetMapping("/product/delete/{identifier}")
	public @ResponseBody AjaxResponse deleteProduct(@PathVariable("identifier") String identifier) {
		if (identifier == null || identifier.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "未指定要删除的商品");
		}
		
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Product> result = productService.deleteProduct(tenantId, identifier);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		if (result.getResultNum() == 0) {
			return new AjaxResponse(ReturnState.ERROR, "删除商品失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@RequestMapping("/product_create")
	public String createProduct(Model model) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<TenantAgent> result = tenantService.getAllAgent(tenantId);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "product_create";
		}
		
		TenantAgent agent = result.getData(0);
		if (agent == null) {
			model.addAttribute("error", "未查询到有效代理品牌、品类");
			return "product_create";
		}
		
		model.addAttribute("agents", agent.getAgent());
		model.addAttribute("categories", agent.getCategories());
		return "product_create";
	}
	
	@GetMapping("/product/update/{identifier}")
	public String getUpdateProductView(Model model, @PathVariable("identifier") String identifier) {
		if (identifier == null || identifier.isEmpty()) {
			model.addAttribute("未指定需要更新的商品编码");
			return "product_update";
		}
		
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Product> result = productService.getProductByIdentifier(tenantId, identifier);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "product_update";
		}
		
		Product product = result.getData(0);
		if (product == null) {
			model.addAttribute("未查询到指定商品信息");
			return "product_update";
		}
		
		Map<String, Boolean> limits = result.getEditLimit("product");
		product = Utils.filterViewFields(product, limits);
		model.addAttribute("product", product);
		model.addAttribute("limit", limits);
		return "product_update";
	}
	
	@PostMapping("/product/update")
	public @ResponseBody AjaxResponse updateProduct(@RequestBody ProductDetail details) {
		String check = ProductDetail.isValid(details, true);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		Product product = details.toProduct();
		if (product == null) {
			return new AjaxResponse(ReturnState.ERROR, "解析数据失败，请刷新后重试");
		}

		ServiceResult<Product> result = productService.updateProduct(product.getTenantId(), product);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		product = result.getData(0);
		if (product == null) {
			return new AjaxResponse(ReturnState.ERROR, "创建商品失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("identifier", product.getIdentifier());
	}
	
	public static class CustomSizeEditor extends PropertyEditorSupport {

		@Override
		public void setAsText(String text) throws IllegalArgumentException {
			String measurements[] = text.split("x");
			try {
				if (measurements.length == 3) {
					/* length, width, heigh */
					double length = Double.valueOf(measurements[0].trim());
					double width = Double.valueOf(measurements[1].trim());
					double heigh = Double.valueOf(measurements[2].trim());
					setValue(new Size(heigh, width, length));
				} else if (measurements.length == 2) {
					double length = Double.valueOf(measurements[0].trim());
					double width = Double.valueOf(measurements[1].trim());
					setValue(new Size(0.0, width, length));
				} else {
					setValue(new Size(0.0, 0.0, 0.0));
				}
			} catch (Exception ex) {
				throw new IllegalArgumentException("Size参数错误");
			}
		}
	}
	
	public static class ProductDetail {
		private String type;
		private String identifier;
		private String name;
		private String introduction;
		private String category;
		private String brand;
		private String material;
		private String color;
		private Size size;
		private double price;
		private double minPrice;
		private int inventory;
		private String remark;
		private int version;
		private String creater;
		private String tenantId;
		private List<Item> products = new ArrayList<>();
		
		public Product toProduct() {
			if (type.equals("group")) {
				ProductBundle bundle = new ProductBundle();
				bundle.setType(Constant.PRODUCT_COMBINATION);
				bundle.setIdentifier(identifier);
				bundle.setName(name);
				bundle.setIntroduction(introduction);
				bundle.setPrice(price);
				bundle.setMinPrice(minPrice);
				bundle.setInventory(inventory);
				bundle.setRemark(remark);
				bundle.setValid(true);
				bundle.setVersion(version);
				bundle.setCreater(creater);
				bundle.setTenantId(tenantId);
				for (Item item : products) {
					if(item.getType().equals("group")) {
						ProductBundle product = new ProductBundle();
						product.setId(item.getId());
						ProductItem productItem = new ProductItem();
						productItem.setItem(product);
						productItem.setNumber(item.getNumber());
						bundle.getItems().add(productItem);
					} else if (item.getType().equals("single")) {
						ProductSingle product = new ProductSingle();
						product.setId(item.getId());
						ProductItem productItem = new ProductItem();
						productItem.setItem(product);
						productItem.setNumber(item.getNumber());
						bundle.getItems().add(productItem);
					}
				}
				return bundle;
		} else if (type.equals("single")) {
				ProductSingle single = new ProductSingle();
				single.setType(Constant.PRODUCT_SINGLE);
				single.setIdentifier(identifier);
				single.setName(name);
				single.setIntroduction(introduction);
				single.setPrice(price);
				single.setMinPrice(minPrice);
				single.setInventory(inventory);
				single.setRemark(remark);
				single.setValid(true);
				single.setCategory(category);
				single.setBrand(brand);
				single.setMaterial(material);
				single.setColor(color);
				single.setSize(size);
				single.setVersion(version);
				single.setCreater(creater);
				single.setTenantId(tenantId);
				return single;
			}
			
			return null;
		}
		
		public static String isValid(ProductDetail detail, boolean isUpdate) {
			if (detail == null) {
				return "参数错误，未解析到有效数据";
			}
			
			if (detail.getType() == null || (!detail.getType().equals("group") && !detail.getType().equals("single"))) {
				return "商品类型错误";
			}
			
			if (detail.getName() == null || detail.getName().isEmpty()) {
				return "商品名不能为空";
			}
			
			if (detail.getIdentifier() == null || detail.getIdentifier().isEmpty()) {
				return "商品编码不能为空";
			}
			
			if (detail.getType().equals("single") && (detail.getCategory() == null || detail.getCategory().isEmpty())) {
				return "商品品类不能为空";
			}
			
			if (detail.getType().equals("single") && (detail.getBrand() == null || detail.getBrand().isEmpty())) {
				return "商品品牌不能为空";
			}
			
			if (detail.getPrice() < 0.0) {
				return "商品价格设置错误，不能 < 0元";
			}
			
			if (detail.getMinPrice() < 0.0) {
				detail.setMinPrice(detail.getPrice());
			}
			
			if (detail.getInventory() < 0) {
				detail.setInventory(0);
			}
			
			if (detail.getType().equals("group") &&  detail.getProducts() != null && detail.getProducts().size() <= 0) {
				return "组合商品中必须包含一种单品";
			}
			
			if (detail.getType().equals("group") &&  detail.getProducts() != null && detail.getProducts().size() > 0) {
				List<Item> items = detail.getProducts();
				for (Item item : items) {
					String check = Item.isValid(item);
					if (check != null) {
						return check;
					}
				}
			}
			
			if (isUpdate) {
				if (detail.getVersion() < 0) {
					return "商品版本号错误";
				}
				
				if (detail.getCreater() == null || detail.getCreater().isEmpty()) {
					return "商品创建人为空，请刷新页面后重试";
				}
				
				if (detail.getTenantId() == null || detail.getTenantId().isEmpty()) {
					return "商品租户为空，请刷新页面后重试";
				}
			}
			
			return null;
		}
		
		public ProductDetail() {
			
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getIdentifier() {
			return identifier;
		}

		public void setIdentifier(String identifier) {
			this.identifier = identifier;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getIntroduction() {
			return introduction;
		}

		public void setIntroduction(String introduction) {
			this.introduction = introduction;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String getBrand() {
			return brand;
		}

		public void setBrand(String brand) {
			this.brand = brand;
		}

		public String getMaterial() {
			return material;
		}

		public void setMaterial(String material) {
			this.material = material;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public Size getSize() {
			return size;
		}

		public void setSize(Size size) {
			this.size = size;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}

		public double getMinPrice() {
			return minPrice;
		}

		public void setMinPrice(double minPrice) {
			this.minPrice = minPrice;
		}

		public int getInventory() {
			return inventory;
		}

		public void setInventory(int inventory) {
			this.inventory = inventory;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public List<Item> getProducts() {
			return products;
		}

		public void setProducts(List<Item> products) {
			this.products = products;
		}

		public int getVersion() {
			return version;
		}

		public void setVersion(int version) {
			this.version = version;
		}

		public String getCreater() {
			return creater;
		}

		public void setCreater(String creater) {
			this.creater = creater;
		}

		public String getTenantId() {
			return tenantId;
		}

		public void setTenantId(String tenantId) {
			this.tenantId = tenantId;
		}

		@Override
		public String toString() {
			return "ProductDetail [type=" + type + ", identifier=" + identifier + ", name=" + name + ", introduction="
					+ introduction + ", category=" + category + ", brand=" + brand + ", material=" + material
					+ ", color=" + color + ", size=" + size + ", price=" + price + ", minPrice=" + minPrice
					+ ", inventory=" + inventory + ", remark=" + remark + ", version=" + version + ", products="
					+ products + "]";
		}
	}
	
	public static class Item {
		private String id;
		private String type;
		private String name;
		private String identification;
		private String introduction;
		private double price;
		private int number;
		
		public static String isValid(Item item) {
			if (item == null) {
				return "参数错误，未解析到有效单品数据";
			}
			
			if (item.getId() == null || item.getId().isEmpty()) {
				return "单品信息错误，请删除后重新添加";
			}
			
			if (item.getType() == null || (item.getType().equals("group") && item.getType().equals("single")) ) {
				return "单品类型信息错误，请删除后重新添加";
			}
			return null;
		}
		
		public Item() {}
		public Item(String id, String type, String name, String identification, String introduction, double price,
				int number) {
			super();
			this.id = id;
			this.type = type;
			this.name = name;
			this.identification = identification;
			this.introduction = introduction;
			this.price = price;
			this.number = number;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getIdentification() {
			return identification;
		}
		public void setIdentification(String identification) {
			this.identification = identification;
		}
		public String getIntroduction() {
			return introduction;
		}
		public void setIntroduction(String introduction) {
			this.introduction = introduction;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public int getNumber() {
			return number;
		}
		public void setNumber(int number) {
			this.number = number;
		}
		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		@Override
		public String toString() {
			return "ProductItem [id=" + id + ", type=" + type + ", name=" + name + ", identification=" + identification
					+ ", introduction=" + introduction + ", price=" + price + ", number=" + number + "]";
		}
	}
}
