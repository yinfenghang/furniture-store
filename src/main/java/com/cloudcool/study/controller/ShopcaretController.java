package com.cloudcool.study.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.Shopcaret;
import com.cloudcool.study.service.ProductService;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.service.ShopcaretService;

/* URL分配
 * POST /shopcaret/addproduct/{id}/{identifier} 向购物车添加商品
 * POST /shopcaret/deleteproduct/{id}/{identifier} 从购物车中删除商品

 */

@Controller
public class ShopcaretController {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private ShopcaretService shopcaretService;
	@Autowired
	private ProductService productService;
	
	@PostMapping("/shopcaret/addproduct")
	public @ResponseBody AjaxResponse addProductToShopcaret(String product_id, String product_identfier, int number) {
		if ((product_id == null || product_id.isEmpty()) && (product_identfier == null || product_identfier.isEmpty())) {
				return new AjaxResponse(ReturnState.ERROR, "参数错误, 商品ID和商品编码为空");
		}
		if (number <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "参数错误, 商品数量：" + number);
		}

		String tenantId = userLoginDao.findTenantId();
		Product product = null;
		if (product_id != null && !product_id.isEmpty()) {
			product = new Product();
			product.setId(product_id);
		} else {
			ServiceResult<Product> productResult = productService.getProductByIdentifier(tenantId, product_identfier);
			if (productResult.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, "未查询到指定商品, 商品编号：" + product_identfier);
			}
			
			product = productResult.getData(0);
			if (product == null) {
				return new AjaxResponse(ReturnState.ERROR, "未查询到指定商品, 商品编号：" + product_identfier);
			}
		}
		
		String username = UserLoginDao.getSelfUsername();
		ServiceResult<Shopcaret> result = shopcaretService.addProduct(username, product, number);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		if (result.getData(0) == null) {
			return new AjaxResponse(ReturnState.ERROR, "添加商品失败，请重试");
		}
		return AjaxResponse.getInstanceByResult(true);
	}
}
