package com.cloudcool.study.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.entity.Role;
import com.cloudcool.study.entity.SmsRegisterCodeRecord;
import com.cloudcool.study.entity.Tenant;
import com.cloudcool.study.entity.Tenant.Contactor;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.RegisterService;
import com.cloudcool.study.service.RoleService;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.service.TenantService;
import com.cloudcool.study.service.UserService;
import com.cloudcool.study.util.Config;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.PhoneValidator;

@Controller
public class RegisterController {
	@Autowired
	private TenantService tenantService;
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private RegisterService registerService;
	
	@GetMapping(value="register")
	public String register() {
		return "register";
	}
	
	@PostMapping(value="register")
	public ModelAndView register(RegisterBean bean) {
		ModelAndView mav = new ModelAndView("register");
		if (Config.debug)
			System.out.println(bean);

		/* verify register code */
		ServiceResult<SmsRegisterCodeRecord> codeResult = registerService.isRegsiterCodeRight(bean.getContactorPhone(), bean.getRegisterCode());
		if (codeResult.isFailed()) {
			mav.addObject("error", codeResult.getErrorMsg());
		}
		
		/* check */
		if (userService.isUserExist(bean.getUsername())) {
			mav.addObject("error", "用户名已存在，换一个试试");
			return mav;
		}
		
		/* create tenant */
		Contactor contactor = new Contactor();
		contactor.setName(bean.getContactorName());
		contactor.setPhone(bean.getContactorPhone());
		contactor.setEmail(bean.getContactorEmail());
		if (bean.getContactorSex().equals("male")) {
			contactor.setSex(Constant.SEX_MAN);
		} else {
			contactor.setSex(Constant.SEX_WOMAN);
		}
		Tenant tenant = new Tenant();
		tenant.setName(bean.getCorpName());
		tenant.setAddress(bean.getCorpAddress());
		tenant.setPhone(bean.getCorpPhone());
		tenant.setScope(bean.getCorpScope());
		tenant.setContactor(contactor);
		tenant.setValid(true);
		tenant = tenantService.createTenant(tenant);
		if (tenant == null) {
			mav.addObject("error", "注册失败，请重试");
			return mav;
		}

		/* create user */
		User user = new User();
		user.setUsername(bean.getUsername());
		user.setPassword(bean.getPassword());
		user.setName(bean.getContactorName());
		user.setPhone(bean.getContactorPhone());
		user.setAddress(bean.getCorpAddress());
		user.setPosition("管理员");
		if (bean.getContactorSex().equals("male")) {
			user.setSex(Constant.SEX_MAN);
		} else {
			user.setSex(Constant.SEX_WOMAN);
		}
		user.setValid(true);
		user.getRoles().addAll(Constant.getTenantAdmin());
		user.setTenantId(tenant.getId());
		user = userService.createUserByRegister(user);
		if (user == null) {
			tenantService.deleteTenant(tenant.getId());
			mav.addObject("error", "注册失败，请重试");
			return mav;
		}

		mav.setViewName("redirect:/login");
		return mav;
	}
	
	@PostMapping("/register/sendCode/{phone}")
	public @ResponseBody AjaxResponse sendRegisterCode(@PathVariable("phone") String phone) {
		if (phone == null || !PhoneValidator.isPhone(phone)) {
			return new AjaxResponse(ReturnState.ERROR, "电话号码错误");
		}
		
		ServiceResult<SmsRegisterCodeRecord> result = registerService.getRegisterCode(phone);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		String code = result.getData(0).getRegisterCode();
		return AjaxResponse.getInstanceByResult(true).addAttribute("code", code);
	}
	
	public static class RegisterBean {
		private String username;
		private String password;
		private String corpName;
		private String corpAddress;
		private String corpScope;
		private String corpPhone;
		private String contactorName;
		private String contactorPhone;
		private String contactorEmail;
		private String contactorSex;
		private String registerCode;

		public RegisterBean() {}
		
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getCorpName() {
			return corpName;
		}

		public void setCorpName(String corpName) {
			this.corpName = corpName;
		}

		public String getCorpAddress() {
			return corpAddress;
		}

		public void setCorpAddress(String corpAddress) {
			this.corpAddress = corpAddress;
		}

		public String getCorpScope() {
			return corpScope;
		}

		public void setCorpScope(String corpScope) {
			this.corpScope = corpScope;
		}

		public String getCorpPhone() {
			return corpPhone;
		}

		public void setCorpPhone(String corpPhone) {
			this.corpPhone = corpPhone;
		}

		public String getContactorName() {
			return contactorName;
		}

		public void setContactorName(String contactorName) {
			this.contactorName = contactorName;
		}

		public String getContactorPhone() {
			return contactorPhone;
		}

		public void setContactorPhone(String contactorPhone) {
			this.contactorPhone = contactorPhone;
		}

		public String getContactorEmail() {
			return contactorEmail;
		}

		public void setContactorEmail(String contactorEmail) {
			this.contactorEmail = contactorEmail;
		}

		public String getContactorSex() {
			return contactorSex;
		}

		public void setContactorSex(String contactorSex) {
			this.contactorSex = contactorSex;
		}

		public String getRegisterCode() {
			return registerCode;
		}

		public void setRegisterCode(String registerCode) {
			this.registerCode = registerCode;
		}

		@Override
		public String toString() {
			return "RegisterBean [username=" + username + ", password=" + password + ", corpName=" + corpName
					+ ", corpAddress=" + corpAddress + ", corpScope=" + corpScope + ", corpPhone=" + corpPhone
					+ ", contactorName=" + contactorName + ", contactorPhone=" + contactorPhone + ", contactorEmail="
					+ contactorEmail + ", contactorSex=" + contactorSex + ", registerCode=" + registerCode + "]";
		}
		
	}
}
