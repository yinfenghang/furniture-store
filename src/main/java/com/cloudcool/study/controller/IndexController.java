package com.cloudcool.study.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.service.PrivilegeService;
import com.cloudcool.study.service.RoleService;

@Controller
public class IndexController extends CRMBaseController {
	@Autowired
	private RoleService roleService;
	@Autowired
	private PrivilegeService privilegeService;
	
	@RequestMapping(value = {"/", "/index"})
	public String index(Model model) {
		if (!UserLoginDao.isUserLogin()) {
			model.addAttribute("loginError", true);
			return "login";
		}
		return "index";
	}
	
	@GetMapping("/admin_privilege")
	public String adminPrivilege(Model model) {
		model.addAttribute("roles", roleService.findAll());
		model.addAttribute("fields", privilegeService.getAllDataResources());
		return "admin_privilege";
	}
}
