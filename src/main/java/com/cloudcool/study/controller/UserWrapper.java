package com.cloudcool.study.controller;

import java.util.List;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.util.Constant;

/* only user for thymeleaf template */
public class UserWrapper {
	private User user;
	
	public UserWrapper(User user) {
		this.user = user;
	}
	
	public UserWrapper(User user, List<String> privileges) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isSystemAdmin() {
		List<String> roleIds = user.getRoles();
		for (String role : roleIds) {
			if (role.equals(Constant.ROLE_SYSTEM_ADMIN)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isTenantAdmin() {
		List<String> roleIds = user.getRoles();
		for (String role : roleIds) {
			if (role.equals(Constant.ROLE_TENANT_ADMIN)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasRight(String resource, String operation) {
		List<String> authorities = user.getGrantedAuthorities();
		for (String authority : authorities) {
			//System.out.println("authority: " + authority + ", resource: " + resource + ", operation: " + operation);
			if (authority.equals(resource + "-" + operation)) {
				return true;
			}
		}
		return false;
	}
	 
	public boolean hasRights(String resource, List<String> operations) {
		for (String operation : operations) {
			if (hasRight(resource, operation)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return "UserWrapper [user=" + user + "]";
	}
}
