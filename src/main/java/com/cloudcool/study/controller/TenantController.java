package com.cloudcool.study.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.AccountRecord;
import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SubscribePackage;
import com.cloudcool.study.entity.Tenant;
import com.cloudcool.study.entity.Tenant.CashoutAccount;
import com.cloudcool.study.entity.Tenant.Contactor;
import com.cloudcool.study.entity.Tenant.DatePack;
import com.cloudcool.study.entity.Tenant.NumPack;
import com.cloudcool.study.entity.TenantAgent;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.FunctionPackageService;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.service.SubscribePackageService;
import com.cloudcool.study.service.TenantService;
import com.cloudcool.study.service.UserService;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Utils;

/*
 * URL分配：
 * GET /tenant_info 获取当前用户所属租户信息
 * POST /tenant/company 修改tenant基本信息
 * POST /tenant/contactor 修改租户联系人信息
 * GET /tenant_setting 获取租户代理信息
 * POST /tenant/agent/add 添加租户代理品牌
 * POST /tenant/category/add 添加租户代理品类
 * GET /tenant_placeorder 获取可订购功能包列表
 * POST /tenant/placeorder 订购功能包
 * GET /tenant_waterbill 获取资金流水界面
 * GET /tenant/waterbill/{start}/{size} 分页获取资金流水记录
 * GET /tenant_recharge 获取充值页面
 * POST /tenant/cashout/account 修改取款账户信息
 * GET /tenant/cashout/record/{start}/{size} 查询提款记录
 */
@Controller
public class TenantController extends CRMBaseController {
	
	@Autowired
	private TenantService tenantService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private SubscribePackageService packageService;
	@Autowired
	private FunctionPackageService functionService;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

	@RequestMapping("/tenant_info")
	public String getTenantInfo(Model model) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Tenant> result = tenantService.read(tenantId);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "tenant_info";
		}
		
		Tenant tenant = result.getData(0);
		if (tenant == null) {
			model.addAttribute("error", "查询租户信息失败，请重试");
			return "tenant_info";
		}
		
		ServiceResult<User> countResult = userService.countAll();
		if (countResult.isFailed()) {
			model.addAttribute("count", 0);
		} else {
			model.addAttribute("count", countResult.getResultNum() - 1);
		}

		model.addAttribute("tenant", Utils.filterViewFields(tenant, result.getViewLimit("tenant")));
		model.addAttribute("limit", result.getViewLimit());
		return "tenant_info";
	}
	
	@PostMapping("/tenant/company")
	public @ResponseBody AjaxResponse changeCompany(Tenant tenant) {
		String tenantId = userLoginDao.findTenantId();
		tenant.setId(tenantId);
		ServiceResult<Tenant> result = tenantService.update(tenant);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		tenant = result.getData(0);
		if (tenant == null) {
			return new AjaxResponse(ReturnState.ERROR, "更新公司信息失败，请重试");
		}
		
		tenant = Utils.filterViewFields(tenant, result.getViewLimit("tenant"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("tenant", tenant);
	}
	
	@PostMapping("/tenant/contactor")
	public @ResponseBody AjaxResponse changeContactor(Contactor contactor, @RequestParam("version") int version) {
		String check = Contactor.isValid(contactor);
		if (check != null) {
			return new AjaxResponse(check);
		}
		
		Tenant tenant = new Tenant();
		String tenantId = userLoginDao.findTenantId();
		tenant.setId(tenantId);
		tenant.setVersion(version);
		tenant.setContactor(contactor);
		
		ServiceResult<Tenant> result = tenantService.update(tenant);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		tenant = result.getData(0);
		if (tenant == null) {
			return new AjaxResponse(ReturnState.ERROR, "更新联系人失败，请重试");
		}
		
		tenant = Utils.filterViewFields(tenant, result.getViewLimit("tenant"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("tenant", tenant);
	}
	
	@RequestMapping("tenant_report")
	public String getTenantReport() {
		return "tenant_report";
	}
	
	@RequestMapping("/tenant_placeorder")
	public String getTenantPlaceorder(Model model) {
		String tenantId = userLoginDao.findTenantId();
		/* get tenant info */
		ServiceResult<Tenant> tenantResult = tenantService.read(tenantId);
		if (tenantResult.isFailed()) {
			model.addAttribute("error", tenantResult.getErrorMsg());
			return "tenant_placeholder";
		}
		Tenant tenant = tenantResult.getData(0);
		if (tenant == null) {
			model.addAttribute("error", "未查询到您所属租户");
			return "tenant_placeholder";
		}

		
		/* get function packages */
		ServiceResult<FunctionPackage> functionResult = functionService.readAll();
		if (functionResult.isFailed()) {
			model.addAttribute("error", functionResult.getErrorMsg());
			return "tenant_placeholder";
		}
		List<FunctionPackage> functionPacks = functionResult.getData();
		if (functionPacks == null || functionPacks.size() <= 0) {
			model.addAttribute("未查询到有效功能包，请拨打客户电话咨询");
			return "tenant_placeholder";
		}
		
		tenant = Utils.filterViewFields(tenant, tenantResult.getViewLimit("tenant"));
		functionPacks = Utils.filterViewFields(functionPacks, functionResult.getViewLimit("functionPackage"));
		model.addAttribute("tenant", new TenantWrapper(tenant));
		model.addAttribute("functions", functionPacks);

		/* get subscribe packages */
		ServiceResult<SubscribePackage> packResult = packageService.readAll();
		if (!packResult.isFailed()) {
			List<SubscribePackage> packs = packResult.getData();
			if (packs != null && packs.size() > 0) {
				packs = Utils.filterViewFields(packs, packResult.getViewLimit("subscribePackage"));
				model.addAttribute("subscribes", packs);
			}
		}
		
		return "tenant_placeorder";
	}
	
	@PostMapping("/tenant/placeorder")
	public @ResponseBody AjaxResponse placeOrder(@RequestParam("name") String name, @RequestParam("type") int type, @RequestParam("count") int count) {
		if (!Constant.isPackTypeValid(type)) {
			return new AjaxResponse(ReturnState.ERROR, "功能包类型无效");
		}
		
		if (count <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "订购数量必须大于0");
		}
		
		/* TODO: need to cashout */
	
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Tenant> result = tenantService.placeorder(tenantId, type, name, count);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		Tenant tenant = result.getData(0);
		if (tenant == null) {
			/* TODO: need to record the failure event */
			return new AjaxResponse(ReturnState.ERROR, "订购失败，请重试");
		}
		
		tenant = Utils.filterViewFields(tenant, result.getViewLimit("tenant"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("pack", new PlaceOrderResult(tenant, name));
	}
	
	@GetMapping("/tenant_recharge")
	public String getTenantRecharge(Model model) {
		/* need to get account, and recharge recrod */
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Tenant> result = tenantService.read(tenantId);
		if (result.isFailed()) {
			model.addAttribute("error", "获取租户数据失败，请刷新页面重试");
			return "tenant_recharge";
		}

		Tenant tenant = result.getData(0);
		if (tenant == null) {
			model.addAttribute("error", "未查询到租户，请刷新页面重试");
			return "tenant_recharge";
		}
		
		tenant = Utils.filterViewFields(tenant, result.getViewLimit("tenant"));
		model.addAttribute("tenant", new TenantWrapper(tenant));
		return "tenant_recharge";
	}
	
	@GetMapping("/tenant/recharge/{start}/{size}")
	public @ResponseBody AjaxResponse getTenantRechargeRecord(@PathVariable("start") int start, @PathVariable("size") int size) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<AccountRecord> result = tenantService.readRechargeRecordPageable(tenantId, start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, "查询充值记录失败，请重试");
		}
		
		List<AccountRecord> records = result.getData();
		if (records == null) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到充值记录");
		}
		records = Utils.filterViewFields(records, result.getViewLimit("accountRecord"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("records", records);
	}
	
	@RequestMapping("/tenant_waterbill")
	public String getTenantWaterBill(Model model) {
		return "tenant_waterbill";
	}
	
	@GetMapping("/tenant/waterbill/{start}/{size}")
	public @ResponseBody AjaxResponse getWaterBillPageable(@PathVariable("start")int start, @PathVariable("size")int size) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<AccountRecord> result = tenantService.readWaterbillPageable(tenantId, start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<AccountRecord> records = result.getData();
		if (records == null || records.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到有效数据");
		}
		
		int count = -1;
		ServiceResult<AccountRecord> countResult = tenantService.readWaterbillCount(tenantId);
		if (!countResult.isFailed()) {
			count = countResult.getResultNum();
		}
		
		records = Utils.filterViewFields(records, result.getViewLimit("accountRecord"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("records", records).addAttribute("count", count);
	}
	
	@RequestMapping("tenant_export")
	public String getTenantExport() {
		return "tenant_export";
	}
	
	@RequestMapping("tenant_withdraw")
	public String getTenantWithdraw() {
		return "tenant_withdraw";
	}
	
	@RequestMapping("/tenant_setting")
	public String getTenantSetting(Model model) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<TenantAgent> result = tenantService.getAllAgent(tenantId);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "tenant_setting";
		}
		TenantAgent agent = result.getData(0);
		if (agent == null) {
			model.addAttribute("error", "未查询到代理数据");
			return "tenant_setting";
		}
		
		model.addAttribute("agent", agent);
		return "tenant_setting";
	}
	
	@PostMapping("/tenant/agent/add")
 	public @ResponseBody AjaxResponse tenantAddAgent(@RequestParam("agent") String agent) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<TenantAgent> result = tenantService.addAgent(tenantId, agent);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@PostMapping("/tenant/agent/remove")
 	public @ResponseBody AjaxResponse tenantRemoveAgent(@RequestParam("agent") String agent) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<TenantAgent> result = tenantService.removeAgent(tenantId, agent);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@PostMapping("/tenant/category/add")
 	public @ResponseBody AjaxResponse tenantAddCategory(@RequestParam("category") String category) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<TenantAgent> result = tenantService.addCategory(tenantId, category);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@PostMapping("/tenant/category/remove")
 	public @ResponseBody AjaxResponse tenantRemoveCategory(@RequestParam("category") String category) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<TenantAgent> result = tenantService.removeCategory(tenantId, category);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@RequestMapping("tenant_drawout")
	public String getTenantDrawout(Model model) {
		/* need to get account, and recharge recrod */
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<Tenant> result = tenantService.read(tenantId);
		if (result.isFailed()) {
			model.addAttribute("error", "获取租户数据失败，请刷新页面重试");
			return "tenant_recharge";
		}

		Tenant tenant = result.getData(0);
		if (tenant == null) {
			model.addAttribute("error", "未查询到租户，请刷新页面重试");
			return "tenant_recharge";
		}
		
		tenant = Utils.filterViewFields(tenant, result.getViewLimit("tenant"));
		model.addAttribute("tenant", tenant);
		return "tenant_drawout";
	}
	
	@GetMapping("/tenant/cashout/record/{start}/{size}")
	public @ResponseBody AjaxResponse getTenantCashoutRecord(@PathVariable("start") int start, @PathVariable("size") int size) {
		String tenantId = userLoginDao.findTenantId();
		ServiceResult<AccountRecord> result = tenantService.readCashoutRecordPageable(tenantId, start, size);
		if (result.isFailed()) {
			return new AjaxResponse("查询提现记录失败，请刷新重试");
		}
		
		List<AccountRecord> records = result.getData();
		if (records == null) {
			return new AjaxResponse("未查询到提下记录");
		}
		
		records = Utils.filterViewFields(records, result.getViewLimit("accountRecord"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("records", records);
	}
	
	@PostMapping("/tenant/cashout/account")
	public @ResponseBody AjaxResponse updateTenantCashoutAccount(@RequestParam("version") int version, CashoutAccount account) {
		if (version < 0) {
			return new AjaxResponse("租户信息版本号错误，请刷新页面后重试");
		}
		String tenantId = userLoginDao.findTenantId();
		Tenant tenant = new Tenant();
		tenant.setId(tenantId);
		tenant.setCashoutAccount(account);
		tenant.setVersion(version);
		ServiceResult<Tenant> result = tenantService.changeCashoutAccount(tenant);
		if (result.isFailed()) {
			return new AjaxResponse(result.getErrorMsg());
		}
		
		tenant = result.getData(0);
		if (tenant == null) {
			return new AjaxResponse("未查询到您所属租户信息，请重新登录");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("account", tenant.getCashoutAccount()).addAttribute("version", tenant.getVersion());
	}
	
	/* test */
	@RequestMapping(value="tenant", method={RequestMethod.POST})
	public @ResponseBody Map<String, Object> save(@RequestBody Tenant tenant) {
		System.out.println(tenant.toString());
		Map<String, Object> response = new LinkedHashMap<String, Object>();
		if (Tenant.isValid(tenant, null) == null) {
			response.put("message", "data invalid");
			response.put("tenant", tenant.toString());
		} else {
			response.put("message", "insert success");
			response.put("tenant", tenantService.createTenant(tenant));
		}
		
		return response;
	}
	
	public static class TenantWrapper {
		private Tenant tenant;
		
		public TenantWrapper(Tenant tenant) {
			this.tenant = tenant;
		}
		
		public double getAmount() {
			return tenant.getAmount();
		}
		
		public boolean subscribed(String packName) {
			List<DatePack> packs = tenant.getFeaturePacks();
			for (DatePack pack : packs) {
				if (pack.getName().equals(packName))
					return true;
			}
			List<NumPack> numPacks = tenant.getDataPacks();
			for (NumPack pack : numPacks) {
				if (pack.getName().equals(packName))
					return true;
			}
			return false;
		}
		
		public boolean isDateType(String packName) {
			List<DatePack> packs = tenant.getFeaturePacks();
			for (DatePack pack : packs) {
				if (pack.getName().equals(packName))
					return true;
			}
			return false;
		}
		
		public String getEndTimeStr(String packName) {
			List<DatePack> packs = tenant.getFeaturePacks();
			for (DatePack pack : packs) {
				if (pack.getName().equals(packName)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					return dateFormat.format(pack.getEndTime());
				}
			}
			
			return "NA";
		}
		
		public Date getEndTime(String packName) {
			List<DatePack> packs = tenant.getFeaturePacks();
			for (DatePack pack : packs) {
				if (pack.getName().equals(packName)) {
					return pack.getEndTime();
				}
			}
			
			return null;
		}
		
		public int getRemainder(String packName) {
			List<NumPack> numPacks = tenant.getDataPacks();
			for (NumPack pack : numPacks) {
				if (pack.getName().equals(packName))
					return pack.getRemainder();
			}
			
			return 0;
		}
		
		public String getStatus(String packName) {
			if (!subscribed(packName)) {
				return "未订购";
			}
			
			if (isDateType(packName)) {
				Date endTime = getEndTime(packName);
				if (endTime.before(new Date())) {
					return "已超期失效，请重新订购";
				}
				return "正常运行";
			} else {
				int left = getRemainder(packName);
				if (left <= 0) {
					return "配额已用完，停止运行";
				} else {
					return "余额为" + left;
				}
			}
		}
	}
	
	public static class PlaceOrderResult {
		private String name;
		private int type;
		private String status;
		private String endTime;
		private int remainder;
		
		public PlaceOrderResult(Tenant tenant, String name) {
			TenantWrapper wrapper = new TenantWrapper(tenant);
			this.name = name;
			this.type = wrapper.isDateType(name) ? Constant.PACK_DATE : Constant.PACK_NUM;
			this.status = wrapper.getStatus(name);
			if (this.type == Constant.PACK_DATE) {
				Date date = wrapper.getEndTime(name);
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				endTime = format.format(date);
			} else {
				remainder = wrapper.getRemainder(name);
			}
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		public int getRemainder() {
			return remainder;
		}

		public void setRemainder(int remainder) {
			this.remainder = remainder;
		}

		@Override
		public String toString() {
			return "PlaceOrderResult [name=" + name + ", type=" + type + ", status=" + status + ", endTime=" + endTime
					+ "]";
		}
	}
}
