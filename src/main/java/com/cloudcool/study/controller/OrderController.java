package com.cloudcool.study.controller;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.entity.Order;
import com.cloudcool.study.entity.Order.Consignee;
import com.cloudcool.study.entity.Order.Introductor;
import com.cloudcool.study.entity.Order.Participant;
import com.cloudcool.study.entity.OrderItem;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.CustomerService;
import com.cloudcool.study.service.OrderService;
import com.cloudcool.study.service.ProductService;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.service.SmsService;
import com.cloudcool.study.service.UserService;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.DefaultIdGenerator;
import com.cloudcool.study.util.PhoneValidator;
import com.cloudcool.study.util.QRCodeUtils;
import com.cloudcool.study.util.Utils;

/*
 * URL分配
 * GET /order_create 获取创建用户视图
 * GET /order/searchcustomer/{content} 通过order搜索客户信息，绕过权限校验机制
 * POST /order/create 创建订单
 * GET /order/verify 获取待验证订单
 * GET /order/cashin 获取待收余款订单
 * GET /order/storeout 获取待出库订单
 * GET /order/delivery 获取待配送订单
 * GET /order/install 获取待安装订单
 * GET /order/list/uncomplete 获取未完成订单
 * GET /order/list/complete 获取已完成订单
 * GET /order/search/{type}/{content} 搜索特定状态的订单
 * POST /order/operate 订单操作
 */

@Controller
public class OrderController extends CRMBaseController {
	@Autowired
	private CustomerService customerService;
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private OrderService orderService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	@Autowired
	private SmsService smsService;

	@GetMapping("/order_create")
	public String createOrder() {
		return "order_create";
	}
	
	@GetMapping("/order/searchcustomer/{content}")
	public @ResponseBody AjaxResponse searchCustomer(@PathVariable("content") String content) {
		if (content == null || content.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "请输入客户姓名或电话后重试");
		}
		
		ServiceResult<Customer> result = customerService.searchCustomersByOrder(content);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Customer> customers = result.getData();
		if (customers == null) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到指定客户");
		}
		
		customers = Utils.filterViewFields(customers, result.getViewLimit("customer"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("customers", customers);
	}
	
	@GetMapping("/order/searchuser/{role}/{content}")
	public @ResponseBody AjaxResponse searchUser(@PathVariable("role") String role, @PathVariable("content") String content) {
		if (role == null || role.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "用户权限错误，请刷新页面后重试");
		}
		
		if (!role.equals("delivery") && !role.equals("install")) {
			return new AjaxResponse(ReturnState.ERROR, "用户只能是配送员或安装工");
		}
		
		if (content == null || content.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "搜索内容不能为空");
		}
		
		if (role.equals("delivery")) {
			role = Constant.ROLE_TENANT_DELIVERY;
		}
		if (role.equals("install")) {
			role = Constant.ROLE_TENANT_INSTALLER;
		}
		
		ServiceResult<User> result = userService.searchByRoleAndContent(role, content);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<User> users = result.getData();
		if (users == null || users.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未搜索到指定员工，请检查员工名称或电话是否正确");
		}
		
		users = Utils.filterViewFields(users, result.getViewLimit("user"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("users", users);
	}
	
	@PostMapping("/order/create")
	public @ResponseBody AjaxResponse createOrder(@RequestBody Order order) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		String check = null;
		
		// 检查客户信息
		Customer customer = order.getCustomer();
		if (customer == null) {
			return new AjaxResponse(ReturnState.ERROR, "客户信息不能为空");
		}
		if (customer.getId() == null || customer.getId().isEmpty()) {
			customer.setCreater(username);
			customer.setTenantId(tenantId);
			check = Customer.isValid(customer, null);
			if (check != null) {
				return new AjaxResponse(ReturnState.ERROR, check);
			}
		}
		
		// 检查收货人信息
		Consignee consignee = order.getConsignee();
		check = Consignee.isValid(consignee);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		// 检查带单人信息
		Introductor introductor = order.getIntroductor();
		if (introductor != null) {
			if (introductor.getName() == null || introductor.getName().isEmpty() ||
					introductor.getPhone() == null || introductor.getPhone().isEmpty()) {
				order.setIntroductor(null);
			} else {
				if (!PhoneValidator.isPhone(introductor.getPhone())) {
					return new AjaxResponse(ReturnState.ERROR, "带单人电话不能为空");
				}
			}
		}
		
		// 设定导购信息
		User user = userLoginDao.findUser();
		Participant guide = new Participant(user.getUsername(), user.getName(), user.getPhone());
		order.setGuider(guide);
		
		// 检查订单信息
		order.setOrderNo(DefaultIdGenerator.generate());
		order.setState(Constant.ORDER_VERIFY);
		if (order.getTotal() < 0.0) {
			return new AjaxResponse(ReturnState.ERROR, "订单金额不能小于0");
		}
		if (order.getPayment() < 0.0) {
			return new AjaxResponse(ReturnState.ERROR, "定金金额不能小于0");
		}
		
		List<OrderItem> items = order.getItems();
		for (int i = 0; i < items.size(); i++) {
			OrderItem item = items.get(i);
			if (item.getIdentifier() == null) {
				return new AjaxResponse(ReturnState.ERROR, "商品列表中有无效商品，请删除后重试");
			}
			ServiceResult<Product> result = productService.getProductByIdentifier(tenantId, item.getIdentifier());
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, "未查找到商品(" + item.getIdentifier() + ")，请确认此商品是否已失效");
			}
			Product product = result.getData(0);
			if (product == null) {
				return new AjaxResponse(ReturnState.ERROR, "未查找到商品(" + item.getIdentifier() + ")，请确认此商品是否已失效");
			}
			item.setProduct(product);
		}

		try {
			ServiceResult<Order> result = orderService.checkAndCreateOrder(order);
			order = result.getData(0);
			return AjaxResponse.getInstanceByResult(true).addAttribute("orderno", order.getOrderNo());
		} catch (Exception ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}
	}
	
	@GetMapping("/order_list/{type}")
	public String getTypeOrderlist(Model model, @PathVariable("type") String type) {
		if (type.equals("verify")) {
			model.addAttribute("title", "待审核订单");
			model.addAttribute("type", "verify");
		}
		
		if (type.equals("cashin")) {
			model.addAttribute("title", "待收款订单");
			model.addAttribute("type", "cashin");
		}
		
		if (type.equals("storeout")) {
			model.addAttribute("title", "待出库订单");
			model.addAttribute("type", "storeout");
		}
		
		if (type.equals("delivery")) {
			model.addAttribute("title", "待派送订单");
			model.addAttribute("type", "delivery");
		}
		
		if (type.equals("install")) {
			model.addAttribute("title", "待安装订单");
			model.addAttribute("type", "install");
		}
		
		return "order_list";
	}

	@GetMapping("/order/list/{type}/{start}/{size}")
	public @ResponseBody AjaxResponse getOrderList(@PathVariable("type") String type, @PathVariable("start") int start, @PathVariable("size") int size) {
		int state = Constant.getOrderStateInt(type);
		if (state == Constant.ORDER_INVALID) {
			new AjaxResponse(ReturnState.ERROR, "订单类型错误，请重试");
		}
		
		ServiceResult<Order> result;
		result = orderService.getOrdersByStatePageable(state, start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Order> orders = result.getData();
		if (orders == null || orders.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到订单");
		}
		
		int count = 0;
		ServiceResult<Order> countResult = orderService.countOrdersByState(state);
		if (!countResult.isFailed()) {
			count = countResult.getResultNum();
		}
		
		orders = Utils.filterViewFields(orders, result.getViewLimit("order"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("orders", orders).addAttribute("count", count);
	}
	
	@GetMapping("/order/search/{type}/{content}")
	public @ResponseBody AjaxResponse searchOrderByTypeAndContent(@PathVariable("type") String type, @PathVariable("content") String content) {
		return AjaxResponse.getInstanceByResult(false);
	}
	
	@GetMapping("/order_verify/{orderNo}") 
	public String getOrderEdit(Model model, @PathVariable("orderNo") String orderNo) {
		if (orderNo == null || orderNo.isEmpty() || orderNo.equals("null")) {
			model.addAttribute("error", "指定的订单号无效，请重试");
			return "order_verify";
		}
		
		int state = Constant.ORDER_VERIFY;
		ServiceResult<Order> result = orderService.getOrderByStateAndOrderNo(state, orderNo);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "order_verify";
		}
		
		Order order = result.getData(0);
		if (order == null) {
			model.addAttribute("error", "未查找到指定的订单，请确认订单号和订单状态是否正确");
			return "order_verify";
		}
		
		order = Utils.filterViewFields(order, result.getEditLimit("order"));
		model.addAttribute("order", order);
		Map<String, Boolean> limit = result.getEditLimit("order");
		model.addAttribute("limit", limit);
		
		return "order_verify";
	}
	
	@PostMapping("/order/verify/{delivery}/{installer}")
	public @ResponseBody AjaxResponse verifyOrder(@RequestBody Order info, @PathVariable("delivery") Boolean delivery_notify, @PathVariable("installer") boolean installer_notify) {
		if (info == null) {
			return new AjaxResponse(ReturnState.ERROR, "未解析到订单数据");
		}
		if (info.getOrderNo() == null || info.getOrderNo().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "订单号不能为空");
		}
		
		if (info.getVersion() == null) {
			return new AjaxResponse(ReturnState.ERROR, "订单版本号不能为空");
		}
		
		User user = userLoginDao.findUser();
		Participant approver = new Participant(user.getUsername(), user.getName(), user.getPhone());
		info.setApprover(approver);
		String check = Participant.isValid(info.getDeliveryer());
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		check =  Participant.isValid(info.getInstaller());
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		info.setItems(null);
		
		try {
			ServiceResult<Order> result = orderService.update(Constant.ORDER_CASHIN, info);
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
			}
			info = result.getData(0);
			if (info == null) {
				return new AjaxResponse(ReturnState.ERROR, "更新订单失败，请刷新页面后重试");
			}
		}  catch (RuntimeException ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}
		
		/* send notify */
		if (delivery_notify) {
			smsService.sendDeliveryNotify(info.getDeliveryer().getParticipantPhone(), info.getOrderNo(), info.getOrderNo());
		}
		if (installer_notify) {
			smsService.sendInstallNottify(info.getDeliveryer().getParticipantPhone(), info.getOrderNo(), info.getOrderNo());
		}
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@GetMapping("/order_cashin/{orderNo}") 
	public String getOrderCashin(Model model, @PathVariable("orderNo") String orderNo) {
		if (orderNo == null || orderNo.isEmpty() || orderNo.equals("null")) {
			model.addAttribute("error", "指定的订单号无效，请重试");
			return "order_cashin";
		}
		
		int state = Constant.ORDER_CASHIN;
		ServiceResult<Order> result = orderService.getSelfCreateOrderByState(state, orderNo);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "order_cashin";
		}
		
		Order order = result.getData(0);
		if (order == null) {
			model.addAttribute("error", "未查找到指定的订单，请确认订单号和订单状态是否正确");
			return "order_cashin";
		}
		
		order = Utils.filterViewFields(order, result.getEditLimit("order"));
		model.addAttribute("order", order);
		Map<String, Boolean> limit = result.getEditLimit("order");
		model.addAttribute("limit", limit);
		
		return "order_cashin";
	}
	
	@PostMapping("/order/cashin")
	public @ResponseBody AjaxResponse orderCashin(@RequestBody Order info) {
		if (info == null) {
			return new AjaxResponse(ReturnState.ERROR, "未解析到订单数据");
		}
		if (info.getOrderNo() == null || info.getOrderNo().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "订单号不能为空");
		}
		if (info.getVersion() == null) {
			return new AjaxResponse(ReturnState.ERROR, "订单版本号不能为空"); 
		}
		info.setItems(null);
		
		try {
			ServiceResult<Order> result = orderService.update(Constant.ORDER_STOREOUT, info);
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
			}
			info = result.getData(0);
			if (info == null) {
				return new AjaxResponse(ReturnState.ERROR, "更新订单失败，请刷新页面后重试");
			}
		}  catch (RuntimeException ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}

		return AjaxResponse.getInstanceByResult(true);
	}
	
	@GetMapping("/order_storeout/{orderNo}") 
	public String getOrderStoreout(Model model, @PathVariable("orderNo") String orderNo) {
		if (orderNo == null || orderNo.isEmpty() || orderNo.equals("null")) {
			model.addAttribute("error", "指定的订单号无效，请重试");
			return "order_storeout";
		}
		
		int state = Constant.ORDER_STOREOUT;
		ServiceResult<Order> result = orderService.getOrderByStateAndOrderNo(state, orderNo);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "order_storeout";
		}
		
		Order order = result.getData(0);
		if (order == null) {
			model.addAttribute("error", "未查找到指定的订单，请确认订单号和订单状态是否正确");
			return "order_storeout";
		}
		
		order = Utils.filterViewFields(order, result.getEditLimit("order"));
		model.addAttribute("order", order);
		Map<String, Boolean> limit = result.getEditLimit("order");
		model.addAttribute("limit", limit);
		
		return "order_storeout";
	}
	
	@PostMapping("/order/storeout")
	public @ResponseBody AjaxResponse orderStoreout(@RequestBody Order info) {
		if (info == null) {
			return new AjaxResponse(ReturnState.ERROR, "未解析到订单数据");
		}
		if (info.getOrderNo() == null || info.getOrderNo().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "订单号不能为空");
		}
		if (info.getVersion() == null) {
			return new AjaxResponse(ReturnState.ERROR, "订单版本号不能为空"); 
		}
		info.setItems(null);
		
		try {
			ServiceResult<Order> result = orderService.orderStoreout(Constant.ORDER_DELIVERY, info);
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
			}
			info = result.getData(0);
			if (info == null) {
				return new AjaxResponse(ReturnState.ERROR, "订单出库失败，请重试"); 
			}
		} catch (RuntimeException ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage()); 
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@GetMapping("/order_delivery/{orderNo}") 
	public String getOrderDelivery(Model model, @PathVariable("orderNo") String orderNo) {
		if (orderNo == null || orderNo.isEmpty() || orderNo.equals("null")) {
			model.addAttribute("error", "指定的订单号无效，请重试");
			return "order_delivery";
		}
		
		int state = Constant.ORDER_DELIVERY;
		ServiceResult<Order> result = orderService.getOrderByStateAndOrderNo(state, orderNo);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "order_delivery";
		}
		
		Order order = result.getData(0);
		if (order == null) {
			model.addAttribute("error", "未查找到指定的订单，请确认订单号和订单状态是否正确");
			return "order_delivery";
		}
		
		order = Utils.filterViewFields(order, result.getEditLimit("order"));
		model.addAttribute("order", order);
		Map<String, Boolean> limit = result.getEditLimit("order");
		model.addAttribute("limit", limit);
		
		return "order_delivery";
	}
	
	@PostMapping("/order/delivery")
	public @ResponseBody AjaxResponse orderDelivery(@RequestBody Order info) {
		if (info == null) {
			return new AjaxResponse(ReturnState.ERROR, "未解析到订单数据");
		}
		if (info.getOrderNo() == null || info.getOrderNo().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "订单号不能为空");
		}
		
		if (info.getVersion() == null) {
			return new AjaxResponse(ReturnState.ERROR, "订单版本号不能为空");
		}
		
		try {
			ServiceResult<Order> result = orderService.update(Constant.ORDER_INSTALL, info);
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
			}
			info = result.getData(0);
			if (info == null) {
				return new AjaxResponse(ReturnState.ERROR, "更新订单失败，请刷新页面后重试");
			}
		}  catch (RuntimeException ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@GetMapping("/order_install/{orderNo}") 
	public String getOrderInstall(Model model, @PathVariable("orderNo") String orderNo) {
		if (orderNo == null || orderNo.isEmpty() || orderNo.equals("null")) {
			model.addAttribute("error", "指定的订单号无效，请重试");
			return "order_install";
		}
		
		int state = Constant.ORDER_INSTALL;
		ServiceResult<Order> result = orderService.getOrderByStateAndOrderNo(state, orderNo);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "order_install";
		}
		
		Order order = result.getData(0);
		if (order == null) {
			model.addAttribute("error", "未查找到指定的订单，请确认订单号和订单状态是否正确");
			return "order_install";
		}
		
		order = Utils.filterViewFields(order, result.getEditLimit("order"));
		model.addAttribute("order", order);
		Map<String, Boolean> limit = result.getEditLimit("order");
		model.addAttribute("limit", limit);
		
		return "order_install";
	}
	
	@PostMapping("/order/install")
	public @ResponseBody AjaxResponse orderInstall(@RequestBody Order info) {
		if (info == null) {
			return new AjaxResponse(ReturnState.ERROR, "未解析到订单数据");
		}
		if (info.getOrderNo() == null || info.getOrderNo().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "订单号不能为空");
		}
		if (info.getVersion() == null) {
			return new AjaxResponse(ReturnState.ERROR, "订单版本号不能为空");
		}
		
		try {
			ServiceResult<Order> result = orderService.update(Constant.ORDER_FINISH, info);
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
			}
			info = result.getData(0);
			if (info == null) {
				return new AjaxResponse(ReturnState.ERROR, "更新订单失败，请刷新页面后重试");
			}
		}  catch (RuntimeException ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@RequestMapping("/order_uncomplete")
	public String uncompleteOrder() {
		return "order_uncomplete";
	}
	
	@GetMapping("/order/uncomplete/{start}/{size}")
	public @ResponseBody AjaxResponse getOrderUncompleteList(@PathVariable("start") int start, @PathVariable("size") int size) {
		if (start < 0 || size <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "参数错误");
		}
		
		ServiceResult<Order> result = orderService.getUncompleteOrdersPageable(start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Order> orders = result.getData();
		if (orders == null || orders.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到指定订单");
		}
		
		int count = 0;
		ServiceResult<Order> countResult = orderService.countUncompleteOrdersPageable();
		if (countResult.isFailed()) {
			count = 0;
		} else {
			count = countResult.getResultNum();
		}
		
		orders = Utils.filterViewFields(orders, result.getViewLimit("order"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("orders", orders).addAttribute("count", count);
	}
	
	@RequestMapping("/order_complete")
	public String completeOrder() {
		return "order_complete";
	}
	
	@GetMapping("/order/complete/{start}/{size}")
	public @ResponseBody AjaxResponse getOrderCompleteList(@PathVariable("start") int start, @PathVariable("size") int size) {
		if (start < 0 || size <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "参数错误");
		}
		
		ServiceResult<Order> result = orderService.getCompleteOrdersPageable(start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Order> orders = result.getData();
		if (orders == null || orders.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到指定订单");
		}
		
		int count = 0;
		ServiceResult<Order> countResult = orderService.countCompleteOrdersPageable();
		if (countResult.isFailed()) {
			count = 0;
		} else {
			count = countResult.getResultNum();
		}
		
		orders = Utils.filterViewFields(orders, result.getViewLimit("order"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("orders", orders).addAttribute("count", count);
	}
	
	@RequestMapping("/order_detail/{orderNo}")
	public String orderdetail(Model model, @PathVariable("orderNo") String orderNo) {
		if (orderNo == null || orderNo.isEmpty()) {
			model.addAttribute("error", "订单Id不能为空");
			return "order_detail";
		}
		
		ServiceResult<Order> result = orderService.getSelfCreateOrderByOrderNo(orderNo);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "order_detail";
		}
		
		Order order = result.getData(0);
		if (order == null) {
			model.addAttribute("error", "未查询到订单");
			return "order_detail";
		}
		
		model.addAttribute("order", order);
		if (order.getState() == Constant.ORDER_VERIFY) {
			return "order_update";
		} else {
			return "order_detail";
		}
	}
	
	@PostMapping("/order/update")
	public @ResponseBody AjaxResponse updateOrder(@RequestBody Order order) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		String check = null;
		
		if (order.getOrderNo() == null || order.getOrderNo().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "订单号不能为空，请刷新重试");
		}
		
		if (order.getState() != Constant.ORDER_VERIFY) {
			return new AjaxResponse(ReturnState.ERROR, "订单不在待审核状态，只有待审核状态才允许修改");
		}
		
		// 检查客户信息
		Customer customer = order.getCustomer();
		if (customer == null) {
			return new AjaxResponse(ReturnState.ERROR, "客户信息不能为空");
		}
		if (customer.getId() == null || customer.getId().isEmpty()) {
			customer.setCreater(username);
			customer.setTenantId(tenantId);
			check = Customer.isValid(customer, null);
			if (check != null) {
				return new AjaxResponse(ReturnState.ERROR, check);
			}
		}
		
		// 检查收货人信息
		Consignee consignee = order.getConsignee();
		check = Consignee.isValid(consignee);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		// 检查带单人信息
		Introductor introductor = order.getIntroductor();
		if (introductor != null) {
			if (introductor.getName() == null || introductor.getName().isEmpty() ||
					introductor.getPhone() == null || introductor.getPhone().isEmpty()) {
				order.setIntroductor(null);
			} else {
				if (!PhoneValidator.isPhone(introductor.getPhone())) {
					return new AjaxResponse(ReturnState.ERROR, "带单人电话不能为空");
				}
			}
		}
		
		// 检查订单信息
		if (order.getTotal() < 0.0) {
			return new AjaxResponse(ReturnState.ERROR, "订单金额不能小于0");
		}
		order.setPayment(null);
		List<OrderItem> items = order.getItems();
		for (int i = 0; i < items.size(); i++) {
			OrderItem item = items.get(i);
			if (item.getIdentifier() == null) {
				return new AjaxResponse(ReturnState.ERROR, "商品列表中有无效商品，请删除后重试");
			}
			ServiceResult<Product> result = productService.getProductByIdentifier(tenantId, item.getIdentifier());
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, "未查找到商品(" + item.getIdentifier() + ")，请确认此商品是否已失效");
			}
			Product product = result.getData(0);
			if (product == null) {
				return new AjaxResponse(ReturnState.ERROR, "未查找到商品(" + item.getIdentifier() + ")，请确认此商品是否已失效");
			}
			item.setProduct(product);
		}

		try {
			ServiceResult<Order> result = orderService.checkAndCreateOrder(order);
			if (result.isFailed()) {
				return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
			}
			order = result.getData(0);
			return AjaxResponse.getInstanceByResult(true).addAttribute("orderno", order.getOrderNo());
		} catch (Exception ex) {
			return new AjaxResponse(ReturnState.ERROR, ex.getMessage());
		}
	}
	
	@RequestMapping("order_detail_anonymous")
	public String order_detail_anonymous() {
		return "order_detail_anonymous";
	}
	
	@GetMapping("/order_qrcode/{payment}")
	public @ResponseBody String createQrcode(@PathVariable("payment")int payment) throws IOException {
		return QRCodeUtils.createQRCode("" + payment, 200, 200);
	}
}
