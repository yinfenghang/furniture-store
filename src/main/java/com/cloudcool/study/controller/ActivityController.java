package com.cloudcool.study.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ActivityController extends CRMBaseController {
	@RequestMapping("activity_create")
	public String activityCreate() {
		return "activity_create";
	}
	
	@RequestMapping("activity_list")
	public String activityList() {
		return "activity_list";
	}
	
	@RequestMapping("activity_detail_user")
	public String activityListUser() {
		return "activity_detail_user";
	}
	
	@RequestMapping("activity_detail_product")
	public String activityListProduct() {
		return "activity_detail_product";
	}
}
