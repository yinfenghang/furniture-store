package com.cloudcool.study.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.FilterableResult;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.service.UserService;
import com.cloudcool.study.util.Utils;

/*
 * URL分配：
 * GET /user_info/{username} 查询用户信息（显示）
 * GET /user_edit/{username} 查询用户信息（编辑）
 * POST /user_edit/{username} 修改用户信息
 * GET /user_create 获取用户输入页面
 * POST（ajax） /user_create 创建用户
 * GET /user_list 获取用户列表页面
 * GET /user_list/{start}/{size} 获取部分用户列表
 * GET/POST /user_delete/{username} 删除用户
 * GET /user_search/{content} 搜索用户
 */


@Controller
public class UserInfoController extends CRMBaseController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserLoginDao userLoginDao;
	
	private List<User> filterRole(List<User> users) {
		for (User user : users) {
			List<String> roleStrs = user.getRoles();
			List<String> roles = new ArrayList<>();
			for (String roleStr : roleStrs) {
				roles.add(user.getRoleStr(roleStr));
			}
			user.setRoles(roles);
		}
		return users;
	}
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
	@GetMapping("/user_info/{username}")
	public String getUserInfoView(Model model, @PathVariable("username")@Nullable String username) {
		ServiceResult<User> result = null;
		
		if (username == null || username.isEmpty()) {
			result = userService.read(UserLoginDao.getSelfUsername());
		} else {
			result = userService.read(username);
		}
		/* 查询用户失败，向用户显示失败请提示失败原因 */
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "user_info";
		}
		
		User user = result.getData(0);
		if (user == null) {
			model.addAttribute("error", "查询用户失败，请重试");
			return "user_info";
		}
		
		/* 查询用户成功，显示查询用户信息 */
		model.addAttribute("userInfo", Utils.filterViewFields(user, result.getViewLimit("user")));
		model.addAttribute("limit", result.getViewLimit());
		return "user_info";
	}
	
	@GetMapping(path= {"/user_edit/{username}", "/user_edit/", "/user_edit"})
	public String getUserInfoForEdit(Model model, @PathVariable("username")@Nullable String username) {
		ServiceResult<User> result = null;
		
		if (username == null || username.isEmpty()) {
			result = userService.read(UserLoginDao.getSelfUsername());
		} else {
			result = userService.read(username);
		}
		
		/* 查询用户失败，向用户显示失败请提示失败原因 */
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "user_edit";
		}

		User user = result.getData(0);
		if (user == null) {
			model.addAttribute("error", "查询用户失败，请重试");
			return "user_edit";
		}
		
		/* 查询用户成功，显示查询用户信息 */
		model.addAttribute("userInfo", Utils.filterEditFields(user, result.getEditLimit("user")));
		model.addAttribute("limit", result.getEditLimit());
		return "user_edit";
	}
	
	@PostMapping("/user_edit/{username}")
	public @ResponseBody AjaxResponse postUserInfo(Model model, User user, @PathVariable("username")String username) {
	//System.out.println("user：" + user.toString());
		user.setUsername(username);
		ServiceResult<User> result = userService.update(user);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		user = result.getData(0);
		if (user == null) {
			return new AjaxResponse(ReturnState.ERROR, "更新用户信息失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@GetMapping("/user_create")
	public String userCreate() {
		return "user_create";
	}
		
	@PostMapping("/user_create")
	public @ResponseBody AjaxResponse userCreate(User user) {
		String tenantId = userLoginDao.findTenantId();
		user.setTenantId(tenantId);
		if (user.getUsername() == null || user.getUsername().isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "用户名不能为空");
		}
		if (userService.isUserExist(user.getUsername())) {
			return new AjaxResponse(ReturnState.ERROR, "用户名已存在，换个用户名试试吧");
		}
		
		/* 初始化密码都是123456 */
		user.setPassword("123456");
		ServiceResult<User> result = userService.createUser(user);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		user = result.getData(0);
		if (user == null) {
			return new AjaxResponse(ReturnState.ERROR,  "创建用户失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("username", user.getUsername());
	}
	
	@GetMapping("/user_list/{start}/{size}")
	public @ResponseBody AjaxResponse getUsers(@PathVariable("start") int start, @PathVariable("size") int size) {
		if (start < 0 || size <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "参数无效: start " + start + ", size " + size);
		}
		
		ServiceResult<User> result = userService.readAllPaging(start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		List<User> users = filterRole(result.getData());
		users = Utils.filterViewFields(users, result.getViewLimit("user"));
		
		ServiceResult<User> countResult = userService.countAll();
		if (countResult.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, countResult.getErrorMsg());
		}
		int count = countResult.getResultNum() - 1;
		return AjaxResponse.getInstanceByResult(true).addAttribute("users", users).addAttribute("sum", count);
	}
	
	@GetMapping("/user_list")
	public String userList(Model model) {
		return "user_list";
	}
	
	@RequestMapping("/user_delete/{username}")
	public @ResponseBody AjaxResponse userDelete(@PathVariable("username") String username) {
		if (username == null || username.isEmpty()) {
			return AjaxResponse.getInstanceByResult(false);
		}
		
		ServiceResult<User> result = userService.delete(username);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		return AjaxResponse.getInstanceByResult(true);
	}
	
	@GetMapping("/user_search/{content}")
	public @ResponseBody AjaxResponse userSearch(@PathVariable("content") String content) {
		if (content == null || content.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "参数错误");
		}
		
		ServiceResult<User> result = userService.searchUser(content);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<User> users = filterRole(result.getData());
		users = Utils.filterViewFields(users, result.getViewLimit("user"));
		return AjaxResponse.getInstanceByResult(true).addAttribute("users", users);
	}
}
