package com.cloudcool.study.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.service.AfterSaleService;

/*
 * URL分配
 * GET /aftersale_create/ 获取aftersale订单创建页面
 * POST /aftersale/create 创建aftersale订单
 */

@Controller
public class AfterSaleController extends CRMBaseController {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private AfterSaleService afterSaleService;
	
//	@GetMapping("/aftersale_create")
//	public String createAfterSale(Model model) {
//		return "aftersale_create";
//	}
}
