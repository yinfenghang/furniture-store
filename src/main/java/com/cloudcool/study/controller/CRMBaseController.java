package com.cloudcool.study.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Notify;
import com.cloudcool.study.entity.Shopcaret;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.Task;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.NotifyService;
import com.cloudcool.study.service.PrivilegeService;
import com.cloudcool.study.service.ShopcaretService;
import com.cloudcool.study.service.TaskService;
import com.cloudcool.study.service.UserService;
import com.cloudcool.study.util.Config;
import com.cloudcool.study.util.Constant;

public class CRMBaseController {
	protected static final int TYPE_VIEW = 0;
	protected static final int TYPE_EDIT = 1;
	protected static final int TYPE_UPDATE = 2;
	
	@Autowired
	private UserLoginDao userLoginDao;
	
	@Autowired
	private NotifyService notifyService;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private ShopcaretService shopcaretService;
	
	@ModelAttribute
	public void initModel(Model model) {
		try {
			UserWrapper userWrapper = new UserWrapper(userLoginDao.findUser());
			model.addAttribute("userWrapper", userWrapper);
			
			List<Shopcaret> carets = shopcaretService.fetchShopcarets(userWrapper.getUser().getUsername());
			model.addAttribute("carets", carets);
			
			List<Notify> notifies = notifyService.getUnreadNotify(userWrapper.getUser().getUsername());
			List<NotifyItem> items = new ArrayList<>();
			for (Notify notify : notifies) {
				String label = Constant.getNotifyLable(notify.getType());
				String content = notify.getTitle();
				items.add(new NotifyItem(label, content));
			}
			if (false) {
				items.add(new NotifyItem("fa-users text-aqua", "sldkf"));
				items.add(new NotifyItem("fa-warning text-yellow", "sldkf2"));
				items.add(new NotifyItem("fa-users text-aqua", "sldkf"));
				items.add(new NotifyItem("fa-warning text-yellow", "sldkf2"));
				items.add(new NotifyItem("fa-users text-aqua", "sldkf"));
				items.add(new NotifyItem("fa-warning text-yellow", "sldkf2"));
			}
			model.addAttribute("notifications", items);
			
			List<Task> tasks = taskService.getUncompleteTask(userWrapper.getUser().getId());
			List<TaskItem> taskItems = new ArrayList<>();
			for (Task task : tasks) {
				taskItems.add(new TaskItem(task.getTitle(), task.getProgress()));
			}
			
			if (false) {
				taskItems.add(new TaskItem("fa-user ", 32));
				taskItems.add(new TaskItem("lsdfk", 53));
			}
			model.addAttribute("tasks", taskItems);
			
		//	System.out.println("yinfenghang:" + items.toString() + "; " + tasks.toString() + "; " + userWrapper.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static class NotifyItem {
		private String label;
		private String content;
		public NotifyItem() {}
		public NotifyItem(String label, String content) {
			super();
			this.label = label;
			this.content = content;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		@Override
		public String toString() {
			return "NotifyItem [label=" + label + ", content=" + content + "]";
		}
	}
	
	public static class TaskItem {
		private String title;
		private int progress;
		public TaskItem(String title, int progress) {
			super();
			this.title = title;
			this.progress = progress;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public int getProgress() {
			return progress;
		}
		public void setProgress(int progress) {
			this.progress = progress;
		}
		@Override
		public String toString() {
			return "TaskItem [title=" + title + ", progress=" + progress + "]";
		}
	}
}
