package com.cloudcool.study.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.service.PrivilegeService;
import com.cloudcool.study.service.RoleService;


@Controller
public class LoginController {	
	
	@RequestMapping("index_anonymous")
	public String indexAnonymous() {
		return "index_anonymous";
	}
	
	@GetMapping(value = "login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("login");
		return mav;
	}

	@GetMapping(value = "login-error")
	public ModelAndView loginError() {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("loginError", true);
		return mav;
	}
	
	@GetMapping(value = "login_anonymous")
	public String loginAnony(Model model, String error) {
		if (error != null) {
			model.addAttribute("error", "username or password error");
		}
		return "login_anonymous";
	}
	
	@PostMapping(value = "login_anonymous")
	public String loginAnony1(Model model) {
		return "index_anonymous";
	}
}
