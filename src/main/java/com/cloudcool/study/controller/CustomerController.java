package com.cloudcool.study.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.CustomerDao;
import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.service.CustomerService;
import com.cloudcool.study.service.ServiceResult;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.PhoneValidator;
import com.cloudcool.study.util.Utils;

/*
 * URL分配
 * GET /customer_create 获取创建客户视图
 * POST /customer/create 创建客户
 * GET /customer_detail/{customerid} 查看客户详情
 * POST /customer/update/ 更新客户信息
 * GET /customer_update/{customerid} 获取修改客户信息视图
 * GET /customer_list 获取客户列表视图
 * GET /customer/list/{start}/{size} 获取客户列表
 * POST /customer/delete 删除客户
 * 
 */
@Controller
public class CustomerController extends CRMBaseController {
	@Autowired
	private CustomerService customerService;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
	@GetMapping("/customer_create")
	public String createCustomer() {
		return "customer_create";
	}
	
	@PostMapping("/customer/create")
	public @ResponseBody AjaxResponse createCustomer(@RequestBody CustomerInfo info) {
		String check = CustomerInfo.isValid(info, false);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		Customer customer = info.toCustomer();
		ServiceResult<Customer> result = customerService.createCustomer(customer);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}		
		
		customer = result.getData(0);
		if (customer == null) {
			return new AjaxResponse(ReturnState.ERROR, "新增客户失败");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("customerid", customer.getId());
	}
	
	@GetMapping("/customer_detail/{customerid}")
	public String customerDetail(Model model, @PathVariable("customerid") String customerid) {
		if (customerid == null || customerid.isEmpty()) {
			return "参数错误，客户ID为空";
		}
		
		ServiceResult<Customer> result = customerService.getCustomerById(customerid);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "customer_detail";
		}
		
		Customer customer = result.getData(0);
		if (customer == null) {
			model.addAttribute("error", "未查询到客户，请检查客户ID是否正确");
			return "customer_detail";
		}
		
		Map<String, Boolean> limit = result.getViewLimit("customer");
		customer = Utils.filterViewFields(customer, limit);
		model.addAttribute("customer", customer);
		model.addAttribute("limit", limit);
		return "customer_detail";
	}
	
	@GetMapping("/customer_update/{customerid}")
	public String customerUpdate(Model model, @PathVariable("customerid") String customerid) {
		if (customerid == null || customerid.isEmpty()) {
			return "参数错误，客户ID为空";
		}
		
		ServiceResult<Customer> result = customerService.getCustomerById(customerid);
		if (result.isFailed()) {
			model.addAttribute("error", result.getErrorMsg());
			return "customer_update";
		}
		
		Customer customer = result.getData(0);
		if (customer == null) {
			model.addAttribute("error", "未查询到客户，请检查客户ID是否正确");
			return "customer_update";
		}
		
		Map<String, Boolean> limit = result.getEditLimit("customer");
		customer = Utils.filterViewFields(customer, limit);
		model.addAttribute("customer", customer);
		model.addAttribute("limit", limit);
		return "customer_update";
	}
	
	@PostMapping("/customer/update")
	public @ResponseBody AjaxResponse updateCustomer(@RequestBody CustomerInfo info) {
		String check = CustomerInfo.isValid(info, true);
		if (check != null) {
			return new AjaxResponse(ReturnState.ERROR, check);
		}
		
		Customer customer = info.toCustomer();
		ServiceResult<Customer> result = customerService.updateCustomer(customer);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		customer = result.getData(0);
		if (customer == null) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到指定客户");
		}
		
		return AjaxResponse.getInstanceByResult(true).addAttribute("customerid", customer.getId());
	}
	
	@GetMapping("/customer_list")
	public String queryCustomer() {
		return "customer_list";
	}
	
	@GetMapping("/customer/list/{start}/{size}")
	public @ResponseBody AjaxResponse getCustomerListPageable(@PathVariable("start") int start, @PathVariable("size") int size) {
		if (start < 0 || size <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "参数错误，start=" + start + "， size=" + size);
		}
		
		ServiceResult<Customer> result = customerService.getCustomersPageable(start, size);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		List<Customer> customers = result.getData();
		if (customers == null || customers.size() <= 0) {
			return new AjaxResponse(ReturnState.ERROR, "未查询到更多客户信息");
		}
		
		int count = 0;
		ServiceResult<Customer> countResult = customerService.countCustomers();
		if (!countResult.isFailed()) {
			count = countResult.getResultNum();
		}
		
		Map<String, Boolean> limit = result.getViewLimit("customer");
		customers = Utils.filterViewFields(customers, limit);
		return AjaxResponse.getInstanceByResult(true).addAttribute("customers", customers).addAttribute("count", count);
	}
	
	@PostMapping("/customer/delete")
	public @ResponseBody AjaxResponse deleteCustomer(String customer_id) {
		if (customer_id == null || customer_id.isEmpty()) {
			return new AjaxResponse(ReturnState.ERROR, "客户ID不能为空");
		}
		
		ServiceResult<Customer> result = customerService.deleteCustomer(customer_id);
		if (result.isFailed()) {
			return new AjaxResponse(ReturnState.ERROR, result.getErrorMsg());
		}
		
		if (result.getResultNum() == 0) {
			return new AjaxResponse(ReturnState.ERROR, "删除客户(" + customer_id+ ")失败，请重试");
		}
		
		return AjaxResponse.getInstanceByResult(true);
	}
	
	public static class CustomerInfo {
		private String id;
		private String name;
		private String address;
		private String phone;
		private Date birthday;
		private String sex;
		private String remark;
		
		public static String isValid(CustomerInfo info, boolean isUpdate) {
			if (info == null) {
				return "参数解析错误";
			}
			
			if (isUpdate && (info.getId() == null || info.getId().isEmpty())) {
				return "客户ID不能为空";
			}
			
			if (info.getName() == null || info.getName().isEmpty()) {
				return "客户姓名不能为空";
			}
			
			if (info.getPhone() == null) {
				return "客户电话能为空";
			}
			
			if (!PhoneValidator.isPhone(info.getPhone())) {
				return "客户电话格式为空，请输入11位手机号码";
			}
			
			return null;
		}
		
		public Customer toCustomer() {
			Customer customer = new Customer();
			customer.setId(id);
			customer.setName(name);
			customer.setPhone(phone);
			customer.setAddress(address);
			customer.setBirthday(birthday);
			if (sex.equals("male")) {
				customer.setSex(Constant.SEX_MAN);
			} else {
				customer.setSex(Constant.SEX_WOMAN);
			}
			customer.setRemark(remark);
			customer.setValid(true);
			return customer;
		}
		
		public CustomerInfo() {}
		public CustomerInfo(String id, String name, String address, String phone, Date birthday, String sex, String remark) {
			super();
			this.id = id;
			this.name = name;
			this.address = address;
			this.phone = phone;
			this.birthday = birthday;
			this.sex = sex;
			this.remark = remark;
		}
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public Date getBirthday() {
			return birthday;
		}
		public void setBirthday(Date birthday) {
			this.birthday = birthday;
		}
		public String getSex() {
			return sex;
		}
		public void setSex(String sex) {
			this.sex = sex;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		@Override
		public String toString() {
			return "CustomerInfo [name=" + name + ", address=" + address + ", phone=" + phone + ", birthday=" + birthday
					+ ", sex=" + sex + ", remark=" + remark + "]";
		}
		
	}
}
