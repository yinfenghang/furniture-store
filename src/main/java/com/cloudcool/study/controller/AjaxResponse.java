package com.cloudcool.study.controller;

import java.util.HashMap;
import java.util.Map;

import com.cloudcool.study.util.Constant;

public class AjaxResponse {
	private ReturnState returnState;
	private String returnMsg;
	private Map<String, Object> returnData = new HashMap<>();
	
	public AjaxResponse() {
		this(ReturnState.OK, "");
	}
	
	public AjaxResponse(String returnMsg) {
		this(ReturnState.OK, returnMsg);
	}
	
	public AjaxResponse(ReturnState returnState, String returnMsg) {
		this.returnState = returnState;
		this.returnMsg = returnMsg;
	}
	
	public AjaxResponse(String attributeName, Object attributeValue) {
		this();
		addAttribute(attributeName, attributeValue);
	}
	
	public ReturnState getReturnState() {
		return returnState;
	}

	public void setReturnState(ReturnState returnState) {
		this.returnState = returnState;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public Map<String, Object> getReturnData() {
		return returnData;
	}

	public void setReturnData(Map<String, Object> returnData) {
		this.returnData = returnData;
	}

	public AjaxResponse addAttribute(String attributeName, Object attributeValue) {
		getReturnData().put(attributeName, attributeValue);
		return this;
	}
	
	public static AjaxResponse getInstanceByResult(boolean result) {
		if (result) {
			return new AjaxResponse(Constant.OPERATE_SUCCESS);
		} else {
			return new AjaxResponse(ReturnState.ERROR, Constant.OPERATE_ERROR);
		}
	}
	
	public enum ReturnState {
		OK,
		WARNING,
		ERROR
	};
}
