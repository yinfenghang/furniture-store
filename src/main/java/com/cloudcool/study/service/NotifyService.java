package com.cloudcool.study.service;

import java.util.List;
import com.cloudcool.study.entity.Notify;

public interface NotifyService {
	public List<Notify> getUnreadNotify(String user);
	public Notify createNotify(Notify notify);
}
