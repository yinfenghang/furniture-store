package com.cloudcool.study.service;

import com.cloudcool.study.entity.SubscribePackage;

public interface SubscribePackageService {
	public ServiceResult<SubscribePackage> readAll();
	public ServiceResult<SubscribePackage> create(SubscribePackage pack);
	public ServiceResult<SubscribePackage> update(SubscribePackage pack);
	public ServiceResult<SubscribePackage> delete(SubscribePackage pack);
	public ServiceResult<SubscribePackage> findById(String subscribePackId);
}
