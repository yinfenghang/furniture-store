package com.cloudcool.study.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.aliyuncs.exceptions.ClientException;

public interface SmsService {
	public boolean sendRegisterCode(String phone, String code, String outId);
	public boolean sendEventNotify(String phone, Date startTime, String title, String outId);
	public boolean sendDeliveryNotify(String phone, String orderNo, String outId);
	public boolean sendInstallNottify(String phone, String orderNo, String outId);
	public void startSmsReportPuller() throws ClientException, ParseException;
	public String getErrorMsgByOutId(String id);	
}
