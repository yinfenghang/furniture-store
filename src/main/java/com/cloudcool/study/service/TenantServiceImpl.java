package com.cloudcool.study.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.TenantPermissionEvaluation;
import com.cloudcool.study.dao.AccountRecordDao;
import com.cloudcool.study.dao.TenantAgentDao;
import com.cloudcool.study.dao.TenantDao;
import com.cloudcool.study.dao.UpdateOptimisticLockException;
import com.cloudcool.study.dao.UpdateParamInvalidException;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.AccountRecord;
import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SubscribePackage;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.Tenant;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.entity.Tenant.CashoutAccount;
import com.cloudcool.study.entity.Tenant.DatePack;
import com.cloudcool.study.entity.Tenant.NumPack;
import com.cloudcool.study.entity.TenantAgent;

@Service
public class TenantServiceImpl implements TenantService{
	@Autowired
	private TenantDao tenantDao;
	@Autowired
	private AccountRecordDao recordDao;
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private TenantAgentDao agentDao;
	@Autowired
	private FunctionPackageService packService;
	@Autowired
	private SubscribePackageService subscribePackService;
	
	@Autowired
	private TenantPermissionEvaluation tenantPermissionEvaluation;
	
	@Override
	public boolean deleteTenant(String tenantId) {
		return tenantDao.delete(tenantId);
	}

	@Nullable
	@Override
	@PreAuthorize("isAnonymous()")
	public Tenant createTenant(Tenant tenant) {
		if (null == Tenant.isValid(tenant, null)) {
			return null;
		}
		
		return tenantDao.create(tenant);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasReqDeleteTenantPermission(#tenantId)")
	public ServiceResult<Tenant> deleteReq(String tenantId) {
		boolean result = tenantDao.invalid(tenantId);
		if (result) {
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("删除失败，请重试");
		}
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasReqDeleteTenantPermission(#tenant.getId())")
	public ServiceResult<Tenant> deleteReq(Tenant tenant) {
		return deleteReq(tenant.getId());
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasReadTenantPermission(#tenantId)")
	public ServiceResult<Tenant> read(String tenantId) {
		Tenant tenant = tenantDao.findById(tenantId);
		if (tenant == null) {
			return new ServiceResult<>("查询租户信息失败，请重试");
		}
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(tenant, "tenant", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasUpdateTenantPermission(#tenant.getId())")
	public ServiceResult<Tenant> update(Tenant tenant) {
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions();
		try {
			tenant = tenantDao.update(tenant, limits);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return new ServiceResult<>(e.getMessage());
		}
		
		if (tenant == null) {
			return new ServiceResult<>("更新租户信息失败，请重试");
		}
		return new ServiceResult<>(tenant, "tenant", limits); 
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasRechargePermission(#tenantId)")
	public ServiceResult<Tenant> recharge(String tenantId, double account) {
		Tenant tenant = tenantDao.addAccount(tenantId, account);
		if (tenant == null) {
			return new ServiceResult<>("充值失败，请重试");
		}
		User user = userLoginDao.findUser();
		recordDao.addRechargeRecord(tenantId, account, user.getName() + "充值");
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions();		
		return new ServiceResult<>(tenant, "tenant", limits); 
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasRechargePermission(#tenantId)")
	public ServiceResult<AccountRecord> readRechargeRecordPageable(String tenantId, int start, int size) {
		List<AccountRecord> records = recordDao.queryRechargeRecordPageable(tenantId, start, size);
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("accountRecord");
		return new ServiceResult<>(records, "accountRecords", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSubscribePackagePermission(#tenantId)")
	public ServiceResult<Tenant> subscribe(String tenantId, String subscribePackId, int number) {
		ServiceResult<SubscribePackage> packResult = subscribePackService.findById(subscribePackId);
		if (packResult.isFailed()) {
			return new ServiceResult<>(packResult.getErrorMsg());
		}
		
		SubscribePackage pack = packResult.getData(0);
		if (pack == null) {
			return new ServiceResult<>("未查找到功能订阅包");
		}
		Tenant tenant = tenantDao.findById(tenantId);
		if (tenant == null) {
			return new ServiceResult<>("获取租户信息失败，请重试");
		}
		
		List<FunctionPackage> functionPackages = pack.getFunctions();
		for (FunctionPackage functionPackage : functionPackages) {
			boolean found = false;
			if (functionPackage.getType() == Constant.PACK_DATE) {
				List<DatePack> datePacks = tenant.getFeaturePacks();
				int days = (int) (pack.getUnit() * number);
				for (DatePack datePack : datePacks) {
					if (datePack.getName().equals(functionPackage.getName())) {
						datePack.setEndTime(new Date(datePack.getEndTime().getTime() + days * 24 * 60 * 60 * 1000L));
						found = true;
						break;
					}
				}
				if (!found) {
					DatePack datePack = new DatePack();
					datePack.setName(functionPackage.getName());
					datePack.setNickName(functionPackage.getNickName());
					datePack.setStartTime(new Date());
					datePack.setEndTime(new Date(datePack.getStartTime().getTime() + days * 24 * 60 * 60 * 1000L));
					datePacks.add(datePack);
				}
			} else {
				List<NumPack> numPacks = tenant.getDataPacks();
				int sum = (int) (pack.getUnit() * number);
				for (NumPack numPack : numPacks) {
					if (numPack.getName().equals(functionPackage.getName())) {
						
						numPack.setRemainder(numPack.getRemainder() + sum);
						found = true;
						break;
					}
				}
				if (!found) {
					NumPack numPack = new NumPack();
					numPack.setName(functionPackage.getName());
					numPack.setNickName(functionPackage.getNickName());
					numPack.setRemainder(sum);
					numPacks.add(numPack);
				}
			}
		}
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("tenant");
		try {
			tenant = tenantDao.update(tenant, limits);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return new ServiceResult<>(e.getMessage());
		}
		if (tenant == null) {
			return new ServiceResult<>("更新租户信息失败");
		}
		
		recordDao.addSubsribeRecord(tenantId, pack.getPrice() * number, 
				"订购包:" + pack.getName() + ", " + pack.getNickname() + ", 单价: " + pack.getPrice() + ", 单位：" + pack.getUnit() + ", 数量:" + number);
		
		return new ServiceResult<>(tenant, "tenant", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSubscribePackagePermission(#tenantId)")
	public ServiceResult<Tenant> placeorder(String tenantId, int type, String name, int count) {
		ServiceResult<FunctionPackage> result = packService.findByNameAndType(name, type);
		if (result.isFailed()) {
			return new ServiceResult<>(result.getErrorMsg());
		}
		
		FunctionPackage pack = result.getData(0);
		if (pack == null) {
			return new ServiceResult<>("未查找到对应功能包，请拨打客服电话咨询");
		}
		
		Tenant tenant = tenantDao.findById(tenantId);
		if (tenant == null) {
			return new ServiceResult<>("未查找到您所属租户，请刷新页面后重试");
		}
		
		if (type == Constant.PACK_DATE) {
			/* find the datepack in tenant */
			List<DatePack> featurePacks = tenant.getFeaturePacks();
			for (DatePack featurePack : featurePacks) {
				if (featurePack.getName().equals(name)) {
					int days = (int) (pack.getUnit() * count + 1);
					if (featurePack.getEndTime().before(new Date())) {
						featurePack.setStartTime(new Date());
						featurePack.setEndTime(new Date(featurePack.getStartTime().getTime() + days * 24 * 60 * 60 * 1000L));
					} else {
						featurePack.setEndTime(new Date(featurePack.getEndTime().getTime() + days * 24 * 60 * 60 * 1000L));
					}
					break;
				}
			}
		} else {
			List<NumPack> dataPacks = tenant.getDataPacks();
			for (NumPack dataPack : dataPacks) {
				if (dataPack.getName().equals(name)) {
					int num = (int) (pack.getUnit() * count);
					dataPack.setRemainder(dataPack.getRemainder() + num);
					break;
				}
			}
		}
		
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions();
		try {
			tenant = tenantDao.update(tenant, limits);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException ex) {
			return new ServiceResult<>(ex.getMessage());
		}
		
		if (tenant == null) {
			return new ServiceResult<>("更新租户订购信息失败，请拨打客户电话处理");
		}
		
		recordDao.addSubsribeRecord(tenantId, pack.getPrice() * count, 
				"功能包:" + pack.getName() + ", " + pack.getNickName() + ", 单价: " + pack.getPrice() + ", 单位：" + pack.getUnit() + ", 数量:" + count);
		
		return new ServiceResult<>(tenant, "tenant", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSubscribePackagePermission(#tenantId)")
	public ServiceResult<AccountRecord> readSubscribeRecord(String tenandId) {
		List<AccountRecord> records = recordDao.querySubscribeRecord(tenandId);
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("accountRecord");
		return new ServiceResult<>(records, "accountRecords", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasReadWaterbillPermission(#tenantId)")
	public ServiceResult<AccountRecord> readWaterbill(String tenantId) {
		List<AccountRecord> records = recordDao.queryWaterbill(tenantId);
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("accountRecord");
		return new ServiceResult<>(records, "accountRecords", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasReadWaterbillPermission(#tenantId)")
	public ServiceResult<AccountRecord> readWaterbillPageable(String tenantId, int start, int size) {
		List<AccountRecord> records = recordDao.queryWaterbillPageable(tenantId, start, size);
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("accountRecord");
		return new ServiceResult<>(records, "accountRecord", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasReadWaterbillPermission(#tenantId)")
	public ServiceResult<AccountRecord> readWaterbillCount(String tenantId) {
		int count = recordDao.countWaterbill(tenantId);
		return new ServiceResult<>(count);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasCashoutPermission(#tenantId)")
	public ServiceResult<Tenant> cashout(String tenantId, double amount) {
		Tenant tenant = tenantDao.addAccount(tenantId, amount);
		if (tenant == null) {
			return new ServiceResult<>("更新租户信息失败");
		}
		
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("tenant");
		return new ServiceResult<>(tenant, "tenant", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasCashoutPermission(#tenantId)")
	public ServiceResult<AccountRecord> readCashoutRecordPageable(String tenantId, int start, int size) {
		List<AccountRecord> records = recordDao.queryCashoutRecordPageable(tenantId, start, size);
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("accountRecord");
		return new ServiceResult<>(records, "accountRecords", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasCashoutAccountPermission(#tenant.getId())")
	public ServiceResult<Tenant> changeCashoutAccount(Tenant tenant) {
		if (tenant.getCashoutAccount() == null) {
			return new ServiceResult<>("提款账户信息不能为空，请重试");
		}
		
		String check = CashoutAccount.isValid(tenant.getCashoutAccount());
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		List<SysDataResource> limits = tenantPermissionEvaluation.getFieldPermissions("tenant");
		try {
			tenant = tenantDao.update(tenant, limits);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return new ServiceResult<>(e.getMessage());
		}
		
		if (tenant == null) {
			return new ServiceResult<>("更新账户失败，请重试");
		}

		return new ServiceResult<>(tenant, "tenant", limits);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasExportDataPermission(#tenantId)")
	public ServiceResult<Tenant> exportData(String tenantId) {
		return new ServiceResult<>("上线中，敬请期待");
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSetAgentPermisison(#tenantId)")
	public ServiceResult<TenantAgent> getAllAgent(String tenantId) {
		TenantAgent agent = agentDao.findByTenantId(tenantId);
		if (agent == null) {
			/* there is no tenant agent data, forget to create? */
			agent = new TenantAgent();
			agent.setTenantId(tenantId);
			agent = agentDao.create(agent);
		}
		return new ServiceResult<>(agent);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSetAgentPermisison(#tenantId)")
	public ServiceResult<TenantAgent> addAgent(String tenantId, String agent) {
		TenantAgent tenantAgent = agentDao.addAgent(tenantId, agent);
		if (tenantAgent == null) {
			return new ServiceResult<>("添加代理失败，请重试");
		}
		
		return new ServiceResult<>(tenantAgent);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSetAgentPermisison(#tenantId)")
	public ServiceResult<TenantAgent> addCategory(String tenantId, String category) {
		TenantAgent tenantAgent = agentDao.addcategory(tenantId, category);
		if (tenantAgent == null) {
			return new ServiceResult<>("添加代理失败，请重试");
		}
		
		return new ServiceResult<>(tenantAgent);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSetAgentPermisison(#tenantId)")
	public ServiceResult<TenantAgent> removeAgent(String tenantId, String agent) {
		TenantAgent tenantAgent = agentDao.removeAgent(tenantId, agent);
		if (tenantAgent == null) {
			return new ServiceResult<>("添加代理失败，请重试");
		}
		
		return new ServiceResult<>(tenantAgent);
	}
	
	@Override
	@PreAuthorize("@tenantPermissionEvaluation.hasSetAgentPermisison(#tenantId)")
	public ServiceResult<TenantAgent> removeCategory(String tenantId, String category) {
		TenantAgent tenantAgent = agentDao.removeCategory(tenantId, category);
		if (tenantAgent == null) {
			return new ServiceResult<>("添加代理失败，请重试");
		}
		
		return new ServiceResult<>(tenantAgent);
	}
}
