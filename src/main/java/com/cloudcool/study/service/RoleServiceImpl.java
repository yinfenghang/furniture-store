package com.cloudcool.study.service;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cloudcool.study.dao.RoleDao;
import com.cloudcool.study.dao.UserDao;
import com.cloudcool.study.entity.Privilege;
import com.cloudcool.study.entity.Role;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private UserDao userDao;
	
	public Role createRole(Role role) {
		return roleDao.create(role);
	}
	
	public Role getRole(String roleStr) {
		return roleDao.findByName(roleStr);
	}

	public Role getRoleTenantAdmin() {
		return getRole(Constant.ROLE_TENANT_ADMIN);
	}
	
	public Role getRoleSystemAdmin() {
		return getRole(Constant.ROLE_SYSTEM_ADMIN);
	}

	public Role findById(String id) {
		return roleDao.findById(id);
	}

	public List<Role> findByUser(String username) {
		User user = userDao.findByUsername(username);
		if (user == null)
			return new ArrayList<>();
		
		List<Role> roles = new ArrayList<>();
		List<String> roleStrs = user.getRoles();
		for (String roleStr : roleStrs) {
			Role role = roleDao.findByName(roleStr);
			if (role == null)
				continue;
			
			roles.add(role);
		}
		
		return roles;
	}

	@Override
	public List<Role> findAll() {
		return roleDao.findAll();
	}
}
