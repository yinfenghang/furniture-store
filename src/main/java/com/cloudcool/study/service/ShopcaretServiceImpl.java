package com.cloudcool.study.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.OrderPermissionEvaluation;
import com.cloudcool.study.dao.ShopcaretDao;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.Shopcaret;
import com.cloudcool.study.entity.SysDataResource;

@Service
public class ShopcaretServiceImpl implements ShopcaretService{
	@Autowired
	private ShopcaretDao shopcaretDao;
	@Autowired
	private OrderPermissionEvaluation orderPermissionEvaluation;
	
	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasAddProductPermission(#creater)")
	public ServiceResult<Shopcaret> addProduct(String creater, Product product, int number) {
		Shopcaret caret = new Shopcaret( product, number, creater);
		caret = shopcaretDao.create(caret);
		if (caret == null) {
			return new ServiceResult<>("添加商品失败，请重试");
		}
		
		List<SysDataResource> limits = orderPermissionEvaluation.getFieldPermissions("shopcaret");
		return new ServiceResult<>(caret, "shopcaret", limits);
	}
	
	public List<Shopcaret> fetchShopcarets(String user) {
		return shopcaretDao.findByCreater(user);
	}
	
	public void deleteShopcaret(Shopcaret shopcaret) {
		if (!shopcaretDao.delete(shopcaret)) {
			System.out.println("delete shopcaret: " + shopcaret + " failed");
		}
	}
}
