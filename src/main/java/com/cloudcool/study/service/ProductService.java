package com.cloudcool.study.service;

import com.cloudcool.study.entity.Product;

public interface ProductService {
	/* create product */
	public ServiceResult<Product> createProduct(Product product);
	
	/* read product list */
	public ServiceResult<Product> getProductsPageable(String tenantId, int start, int size);
	public ServiceResult<Product> getProductByIdentifier(String tenantId, String identifier);
	public ServiceResult<Product> countProduct(String tenantId);

	/* search */
	public ServiceResult<Product> searchByIdentifierOrName(String tenantId, String content);

	/* delete product */
	public ServiceResult<Product> deleteProduct(String tenantId, String identifier);

	/* update */
	public ServiceResult<Product> updateProduct(String tenantId, Product product);
	
	/* storeout & storein */
	public ServiceResult<Product> storeout(String tenantId, Product product, int number);
	public ServiceResult<Product> storein(String tenantId, Product product, int number);
}
