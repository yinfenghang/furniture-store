package com.cloudcool.study.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.ProductPermissionEvaluation;
import com.cloudcool.study.dao.ProductDao;
import com.cloudcool.study.dao.UpdateOptimisticLockException;
import com.cloudcool.study.dao.UpdateParamInvalidException;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.ProductBundle;
import com.cloudcool.study.entity.ProductBundle.ProductItem;
import com.cloudcool.study.entity.ProductSingle;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;
	@Autowired
	private ProductPermissionEvaluation productPermissionEvaluation;
	@Autowired
	private UserLoginDao userLoginDao;

	@Override
	@PreAuthorize("@productPermissionEvaluation.hasCreateProductPermission()")
	public ServiceResult<Product> createProduct(Product product) {
		String tenantId = userLoginDao.findTenantId();
		product.setTenantId(tenantId);
		
		String check = null;
		if (product instanceof ProductSingle) {
			check = ProductSingle.isValid((ProductSingle)product);
		} else {
			check = ProductBundle.isValid((ProductBundle)product);
		}
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		boolean exist = productDao.isIdentifierExist(product.getIdentifier(), product.getTenantId());
		if (exist) {
			return new ServiceResult<>("商品编号已存在，请重新设定");
		}
		
		product = productDao.create(product);
		if (product == null) {
			return new ServiceResult<>("创建商品失败");
		}
		
		List<SysDataResource> limits = productPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(product, "product", limits);
	}
	
	@Override
	@PreAuthorize("@productPermissionEvaluation.hasReadProductPermission(#tenantId)")
	public ServiceResult<Product> getProductByIdentifier(String tenantId, String identifier) {
		Product product = productDao.findByIdentifierAndTenantId(identifier, tenantId);
		if (product == null) {
			return new ServiceResult<>("未查询到指定商品信息");
		}
		
		List<SysDataResource> limits = productPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(product, "product", limits);
	}

	@Override
	@PreAuthorize("@productPermissionEvaluation.hasReadProductPermission(#tenantId)")
	public ServiceResult<Product> getProductsPageable(String tenantId, int start, int size) {
		List<Product> products = productDao.findProductPageable(tenantId, start, size);
		if (products == null || products.size() <= 0) {
			return new ServiceResult<>("未查询更多商品");
		}
		
		List<SysDataResource> limits = productPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(products, "product", limits);
	}

	@Override
	@PreAuthorize("@productPermissionEvaluation.hasReadProductPermission(#tenantId)")
	public ServiceResult<Product> countProduct(String tenantId) {
		int count = productDao.countProduct(tenantId);
		return new ServiceResult<>(count);
	}

	@Override
	@PreAuthorize("@productPermissionEvaluation.hasReadProductPermission(#tenantId)")
	public ServiceResult<Product> searchByIdentifierOrName(String tenantId, String content) {
		List<Product> products = productDao.searchByIdentifierOrName(tenantId, content);
		if (products == null) {
			return new ServiceResult<>("未搜索到相关商品");
		}
		
		List<SysDataResource> limits = productPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(products, "product", limits);
	}

	@Override
	@PreAuthorize("@productPermissionEvaluation.hasDeleteProductPermission(#tenantId)")
	public ServiceResult<Product> deleteProduct(String tenantId, String identifier) {
		boolean result = productDao.invalid(tenantId, identifier);
		if (result) {
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("删除失败");
		}
	}
	
	@Override
	@PreAuthorize("@productPermissionEvaluation.hasUpdateProductPermission(#tenantId, #product.getTenantId())")
	public ServiceResult<Product> updateProduct(String tenantId, Product product) {
		List<SysDataResource> dataResources = productPermissionEvaluation.getFieldPermissions("product");
		try {
			 product = productDao.update(product, dataResources);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return new ServiceResult<>(e.getMessage());
		}
		List<SysDataResource> limits = productPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(product, "product", limits);
	}
	
	private ServiceResult<Product> _storeout(String tenantId, String identifier, int number) {
		Product product = productDao.storeoutByIdentifier(tenantId, identifier, number);
		if (product == null) {
			return new ServiceResult<>("商品" + identifier + "出库失败，请确认商品是否失效");
		}
		return new ServiceResult<>(product);
	}

	@Override
	public ServiceResult<Product> storeout(String tenantId, Product product, int number) {
		if (product instanceof ProductSingle) {
			return _storeout(tenantId, product.getIdentifier(), number);
		} else if (product instanceof ProductBundle) {
			List<Product> products = new ArrayList<>();
			ProductBundle bundle = (ProductBundle)product;
			for (ProductItem item : bundle.getItems()) {
				ServiceResult<Product> pdt = _storeout(tenantId, item.getItem().getIdentifier(), item.getNumber() * number);
				if (pdt.isFailed()) {
					return new ServiceResult<>(pdt.getErrorMsg());
				}
				products.add(pdt.getData(0));
			}
			return new ServiceResult<>(products);
		} else {
			return new ServiceResult<>("商品类型错误");
		}
	}

	@Override
	public ServiceResult<Product> storein(String tenantId, Product product, int number) {
		if (product instanceof ProductSingle) {
			return new ServiceResult<>("入库的商品类型必须为单品");
		}
		
		String identifier = product.getIdentifier();
		product = productDao.storeinByIdentifier(tenantId, product.getIdentifier(), number);
		if (product == null) {
			return new ServiceResult<>("商品" + identifier + "入库失败，请重试");
		}
		
		return new ServiceResult<>(product);
	}
}
