package com.cloudcool.study.service;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import com.cloudcool.study.entity.SysDataResource;

public class ServiceResult<T> {
	public static final int RET_OK = 0;
	public static final int RET_FAIL = 1;
	public static final int LIMIT_VIEW = 0;
	public static final int LIMIT_EDIT = 1;
	public static final int LIMIT_UPDATE = 2;
	
	private int status;
	private String errorMsg;
	private FilterableResult<T> resultData;
	/* 0为false，非0为true */
	private int resultNum = 0;
	
	public ServiceResult(T item) {
		super();
		this.status = RET_OK;
		List<T> items = new ArrayList<>();
		items.add(item);
		this.resultData = new FilterableResult<>(items, null);
	}
	
	public ServiceResult(List<T> items) {
		super();
		this.status = RET_OK;
		this.resultData = new FilterableResult<>(items, null);
	}
	
	public ServiceResult(T item, String resource, List<SysDataResource> dataResources) {
		super();
		this.status = RET_OK;
		Map<String, List<SysDataResource>> limits = new HashMap<>();
		limits.put(resource, dataResources);
		List<T> items = new ArrayList<>();
		items.add(item);
		this.resultData = new FilterableResult<>(items,limits);
	}
	
	public ServiceResult(List<T> items, String resource, List<SysDataResource> dataResources) {
		super();
		this.status = RET_OK;
		Map<String, List<SysDataResource>> limits = new HashMap<>();
		limits.put(resource, dataResources);
		this.resultData = new FilterableResult<>(items,limits);
	}
	
	public ServiceResult(List<T> items, Map<String, List<SysDataResource>> limits) {
		super();
		this.status = RET_OK;
		this.resultData = new FilterableResult<>(items, limits);
	}
	
	public ServiceResult(FilterableResult<T> result) {
		super();
		this.status = RET_OK;
		this.resultData = result;
	}
	
	public ServiceResult(int resultNum) {
		super();
		this.status = RET_OK;
		this.resultNum = resultNum;
	}
	
	public ServiceResult(String msg) {
		super();
		this.status = RET_FAIL;
		this.errorMsg = msg;
	}
	
	public boolean isFailed() {
		if (status == RET_OK)
			return false;
		return true;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public FilterableResult<T> getResultData() {
		return resultData;
	}
	public void setResultData(FilterableResult<T> result) {
		this.resultData = result;
	}
	
	public T getData(int index) {
		List<T> items = this.resultData.getItems();
		if (items == null || items.size() <= index) {
			return null;
		}
		
		return items.get(index);
	}
	
	public List<T> getData() {
		return this.resultData.getItems();
	}
	
	public Map<String, List<SysDataResource>> getRawLimit() {
		return this.resultData.getLimit();
	}
	
	public List<SysDataResource> getRawLimit(String resource) {
		return this.resultData.getLimit(resource);
	}
	
	public Map<String, Map<String, Boolean>> getViewLimit() {
		return getLimit(LIMIT_VIEW);
	}
	
	public Map<String, Map<String, Boolean>> getEditLimit() {
		return getLimit(LIMIT_EDIT);
	}
	
	public Map<String, Map<String, Boolean>> getUpdateLimit() {
		return getLimit(LIMIT_UPDATE);
	}
	
	public Map<String, Boolean> getViewLimit(String resource) {
		return getLimit(resource, LIMIT_VIEW);
	}
	
	public Map<String, Boolean> getEditLimit(String resource) {
		return getLimit(resource, LIMIT_EDIT);
	}
	
	public Map<String, Boolean> getUpdateLimit(String resource) {
		return getLimit(resource, LIMIT_UPDATE);
	}
	
	public Map<String, Map<String, Boolean>> getLimit(int type) {
		Map<String, Map<String, Boolean>> limits = new HashMap<>();
		Map<String, List<SysDataResource>> rawLimit = getRawLimit();
		for (String key : rawLimit.keySet()) {
			Map<String, Boolean> limit = getLimit(key, type);
			limits.put(key, limit);
		}
		return limits;
	}
	
	public Map<String, Boolean> getLimit(String resource, int type) {
		List<SysDataResource> dataResources = getRawLimit(resource);
		Map<String, Boolean> limit = new HashMap<>();
		if (dataResources == null) {
			return limit;
		}
		
		for (SysDataResource dataResource : dataResources) {
			switch (type) {
			case LIMIT_VIEW:
				limit.put(dataResource.getDataCode(), dataResource.isAccessible());
				break;
			case LIMIT_EDIT:
				limit.put(dataResource.getDataCode(), dataResource.isEditable());
				break;
			case LIMIT_UPDATE:
				limit.put(dataResource.getDataCode(), dataResource.isUpdateable());
				break;
			default:
				break;
			}
		}
		return limit;
	}

	public int getResultNum() {
		return resultNum;
	}

	public void setResultNum(int resultNum) {
		this.resultNum = resultNum;
	}

	@Override
	public String toString() {
		return "ServiceResult [status=" + status + ", errorMsg=" + errorMsg + ", resultData=" + resultData
				+ ", resultNum=" + resultNum + "]";
	}
}
