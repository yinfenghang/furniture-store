package com.cloudcool.study.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudcool.study.dao.PrivilegeDao;
import com.cloudcool.study.entity.Privilege;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.util.Note;

@Service
public class PrivilegeServiceImpl implements PrivilegeService{
	@Autowired
	private PrivilegeDao privilegeDao;

	public Privilege createPrivilege(Privilege privilege) {
		return privilegeDao.create(privilege);
	}
	
	private final String classPrefix = "com.cloudcool.study.entity.";
	public Map<String, List<SysDataResource>> getAllDataResources() {
		Map<String, List<SysDataResource>> mapDataResources = new HashMap<>();
		Set<String> collections = privilegeDao.getAllCollections();
		for (String collection : collections) {
			List<SysDataResource> dataResources = new ArrayList<>();
			char[] chars = collection.toCharArray();
			chars[0] = (char)(chars[0] - 'a' + 'A');
			try {
				Class<?> clazz = Class.forName(classPrefix + new String(chars));
				Field[] fields = clazz.getDeclaredFields();
				for (Field field :fields) {
					Note note = field.getDeclaredAnnotation(Note.class);
					SysDataResource dataResource = new SysDataResource();
					dataResource.setResourceId(collection);
					dataResource.setDataCode(field.getName());
					dataResource.setAccessible(false);
					dataResource.setUpdateable(false);
					dataResource.setEditable(false);
					dataResources.add(dataResource);
				}
			} catch (ClassNotFoundException e) {
				System.out.println("Cannot find class " + classPrefix + new String(chars));
			}
			mapDataResources.put(collection, dataResources);
		}
		
		return mapDataResources;
 	}
}
