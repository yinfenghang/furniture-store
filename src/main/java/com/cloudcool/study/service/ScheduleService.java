package com.cloudcool.study.service;

import java.util.Date;
import java.util.List;

import com.cloudcool.study.entity.Schedule;

public interface ScheduleService {

	public ServiceResult<Schedule> createSchedule(String title, String content, 
			Date startTime, Date endTime, List<String> users, boolean notify) throws RuntimeException;

}
