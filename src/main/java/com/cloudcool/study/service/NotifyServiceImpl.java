package com.cloudcool.study.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudcool.study.dao.NotifyDao;
import com.cloudcool.study.entity.Notify;

@Service
public class NotifyServiceImpl implements NotifyService {
	@Autowired
	private NotifyDao notifyDao;
	
	public Notify createNotify(Notify notify) {
		return notifyDao.create(notify);
	}

	public List<Notify> getUnreadNotify(String user) {
		return notifyDao.findByReceiver(user);
	}
}
