package com.cloudcool.study.service;

import java.util.List;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.Shopcaret;

public interface ShopcaretService {
	public ServiceResult<Shopcaret> addProduct(String creater, Product product, int number);
	public List<Shopcaret>  fetchShopcarets(String creater);
	public void deleteShopcaret(Shopcaret shopcaret);
}
