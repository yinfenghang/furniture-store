package com.cloudcool.study.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Privilege;
import com.cloudcool.study.entity.Role;
import com.cloudcool.study.entity.User;

@Service
public class CRMUserDetailService implements UserDetailsService {

	@Autowired
	private UserLoginDao userLoginDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		//System.out.println("loading user " + username);
			
		User user = userLoginDao.findUserByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username: %s", username));
		}
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		List<String> grantedAuthorities = user.getGrantedAuthorities();
		for (String authority : grantedAuthorities) {
			authorities.add(new SimpleGrantedAuthority(authority));
		}

		//System.out.println("user granted: " + authorities.toString());
		return  new org.springframework.security.core.userdetails.User(user.getUsername(),
				user.getPassword(), 
				user.isValid(), 
				user.isValid(), 
				user.isValid(),
				user.isValid(), 
				authorities);
	}

}
