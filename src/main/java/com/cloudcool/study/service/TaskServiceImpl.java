package com.cloudcool.study.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudcool.study.dao.TaskDao;
import com.cloudcool.study.entity.Task;
import com.cloudcool.study.util.Constant;

@Service
public class TaskServiceImpl implements TaskService {
	@Autowired
	private TaskDao taskDao;
	
	public Task createTask(Task task) {
		return taskDao.create(task);
	}
	
	public List<Task> getUncompleteTask(String owner) {
		return taskDao.findByOwnerAndStatus(owner, Constant.TASK_STATUS_UNCOMPLETE);
	}

	public Task findById(String id) {
		return taskDao.findById(id);
	}

	public Task updateProgress(Task task) {
		return task;
	}
}
