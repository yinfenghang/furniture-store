package com.cloudcool.study.service;

import java.util.List;
import com.cloudcool.study.entity.Role;

public interface RoleService {
	/* create */
	public Role createRole(Role role);
	/* read */
	public Role getRole(String roleStr);
	public Role findById(String id);
	public List<Role> findByUser(String username);
	public List<Role> findAll();

	/* tools */
	public Role getRoleTenantAdmin();
	public Role getRoleSystemAdmin();
}
