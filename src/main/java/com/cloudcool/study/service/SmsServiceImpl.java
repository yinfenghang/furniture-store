package com.cloudcool.study.service;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alicom.mns.tools.DefaultAlicomMessagePuller;
import com.alicom.mns.tools.MessageListener;
import com.aliyun.mns.model.Message;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.cloudcool.study.dao.SmsRecordDao;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SmsRecord;
import com.cloudcool.study.util.Constant;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;

@Service
public class SmsServiceImpl implements SmsService {
	private final String regionId = "cn-hangzhou";
	private final String accessKeyId = "LTAIeryRdHRrUvyf";
	private final String accessSecret = "AUOje5e4uW4okTYsXbCgenrYkrDOtC";
	private final String providerDomain = "dysmsapi.aliyuncs.com";
	private final String sdkVersion = "2017-05-25";
	/* 短信签名 */
	private final String signNameReg = "云库门店";
	private final String signNameNotify = "云库科技";
	/* 短信模板 */
	private final String templateRegister = "SMS_169897861";
	private final String templateDelivery = "SMS_169897862";
	private final String templateInstaller = "SMS_169897869";
	private final String templateEventNotify = "SMS_170347263";
	/* 短信回执 */
	private final String smsReportQueueId = "Alicom-Queue-1049204660040122-SmsReport";
	
	@Autowired
	private SmsRecordDao smsDao;
	@Autowired
	private UserLoginDao userLoginDao;
	
	private Gson gson = new Gson();
	
	private String getTemplateName(String code) {
		switch (code) {
		case templateRegister:
			return "register";
		case templateDelivery:
			return "delivery";
		case templateInstaller:
			return "installer";
		case templateEventNotify:
			return "eventNotify";
		default:
			return "unknown";
		}
	}
	
	/* 创建ACSClient */
	private IAcsClient createClient() {
		DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessSecret);
		return new DefaultAcsClient(profile);
	}
	/* 创建请求对象，并初始化公共部分 */
	private CommonRequest createRequest(String phone, String signName, String templateCode, String contentParam, String outId) {
		CommonRequest request = new CommonRequest();
		request.setMethod(MethodType.POST);
		request.setDomain(providerDomain);
		request.setVersion(sdkVersion);
		request.setAction("SendSms");
		request.putQueryParameter("RegionId", regionId);
		
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", contentParam);
        request.putQueryParameter("OutId", outId);
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", signName);
		
		return request;
	}
	
	private SmsCommonResponse _sendSms(IAcsClient client, CommonRequest request) {
		try {
			CommonResponse response = client.getCommonResponse(request);
			return new SmsCommonResponse(response);
		} catch (ServerException e) {
		    e.printStackTrace();
		    return new SmsCommonResponse(e.getMessage());
		} catch (ClientException e) {
		    e.printStackTrace();
		    return new SmsCommonResponse(e.getMessage());
		}
	}
	
	/* 本地记录创建 */
	private SmsRecord createRecord(int type, String phone, 
			String signName, String templateCode, String contentParam, String outId, String remark) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		
		SmsRecord record = new SmsRecord();
		record.setPhone(phone);
		record.setSignName(signName);
		record.setTemplateCode(templateCode);
		record.setTemplateName(getTemplateName(templateCode));
		record.setContentParam(contentParam);
		record.setRemark(remark);
		record.setOutId(outId);
		record.setSender(username);
		record.setTenantId(tenantId);
		record.setStatus(Constant.SMS_CREATE);

		return smsDao.create(record);
	}
	
	private SmsRecord updateWithResponse(SmsRecord smsRecord, SmsCommonResponse response) {
		String tenantId = userLoginDao.findTenantId();
		SmsRecord record = new SmsRecord();
		record.setId(smsRecord.getId());
		
		if (response.isSuccess()) {
			String status = response.getCode();
			if (status.equals("OK")) {
				record.setStatus(Constant.SMS_SEND_SUCCESS);
			} else {
				record.setStatus(Constant.SMS_SEND_FAILED);
			}
			record.setBizId(response.getBizId());
			record.setRequestId(response.getRequestId());
			record.setErrorCode(response.getCode());
			record.setErrorMsg(response.getMessage());
		} else {
			record.setStatus(Constant.SMS_SEND_FAILED);
			record.setErrorCode(response.getCode());
			record.setErrorMsg(response.getMessage());
		}

		return smsDao.updateById(tenantId, record);
	}
	
	@Override
	public String getErrorMsgByOutId(String id) {
		SmsRecord record =  smsDao.findByOutId(id);
		if (record == null) {
			return null;
		} else {
			return record.getErrorMsg();
		}
	}
	
	@Override
	public boolean sendRegisterCode(String phone, String code, String outId) {
		IAcsClient client = createClient();
		
		/* prepare request parameters */
		StringWriter sw = new StringWriter();
		JsonWriter writer = new JsonWriter(sw);
		try {
			writer.beginObject()
						.name("code").value(code)
						.endObject();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String contentParam = sw.toString();
		/* create record */
		String remark = "手机:" + phone + "在" + (new Date()) + "发起注册请求";
		SmsRecord record = createRecord( Constant.SMS_TYPE_REGISTER, phone, signNameReg, templateRegister, contentParam, outId, remark);
		CommonRequest request = createRequest(phone, signNameReg, templateRegister, contentParam, record.getId());
		SmsCommonResponse response = _sendSms(client, request);
		updateWithResponse(record, response);
		return response.isSuccess();
	}

	@Override
	public boolean sendDeliveryNotify(String phone, String orderNo, String outId) {
		IAcsClient client = createClient();
		
		/* prepare request parameters */
		StringWriter sw = new StringWriter();
		JsonWriter writer = new JsonWriter(sw);
		try {
			writer.beginObject()
						.name("order").value(orderNo)
						.endObject();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String contentParam = sw.toString();
		/* create record */
		String remark = "向手机:" + phone + "发送订单" + orderNo + "配送通知";
		SmsRecord record = createRecord( Constant.SMS_TYPE_REGISTER, phone, signNameNotify, templateDelivery, contentParam, outId, remark);
		CommonRequest request = createRequest(phone, signNameNotify, templateDelivery, contentParam, record.getId());
		SmsCommonResponse response = _sendSms(client, request);
		updateWithResponse(record, response);
		return response.isSuccess();
	}

	@Override
	public boolean sendInstallNottify(String phone, String orderNo, String outId) {
		IAcsClient client = createClient();
		
		/* prepare request parameters */
		StringWriter sw = new StringWriter();
		JsonWriter writer = new JsonWriter(sw);
		try {
			writer.beginObject()
						.name("order").value(orderNo)
						.endObject();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String contentParam = sw.toString();
		/* create record */
		String remark = "向手机:" + phone + "发送订单" + orderNo + "配送通知";
		SmsRecord record = createRecord( Constant.SMS_TYPE_REGISTER, phone, signNameNotify, templateInstaller, contentParam, outId, remark);
		CommonRequest request = createRequest(phone, signNameNotify, templateInstaller, contentParam, record.getId());
		SmsCommonResponse response = _sendSms(client, request);
		updateWithResponse(record, response);
		return response.isSuccess();
	}
	
	@Override
	public boolean sendEventNotify(String phone, Date startTime, String title, String outId) {
		IAcsClient client = createClient();
		
		/* prepare request parameters */
		StringWriter sw = new StringWriter();
		JsonWriter writer = new JsonWriter(sw);
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		try {
			writer.beginObject()
						.name("startTime").value(format.format(startTime))
						.name("title").value(title)
						.endObject();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String contentParam = sw.toString();
		/* create record */
		String remark = "向手机:" + phone + "发送日程通知，日程标题" + title + "，日程起始时间" + format.format(startTime) + "配送通知";
		SmsRecord record = createRecord( Constant.SMS_TYPE_REGISTER, phone, signNameNotify, templateEventNotify, contentParam, outId, remark);
		CommonRequest request = createRequest(phone, signNameNotify, templateEventNotify, contentParam, record.getId());
		SmsCommonResponse response = _sendSms(client, request);
		updateWithResponse(record, response);
		return response.isSuccess();
	}

	public static class SmsMessageListener implements MessageListener {
		private Gson gson=new Gson();
		@Autowired
		private SmsRecordDao smsDao;
		@Autowired
		private UserLoginDao userLoginDao;

		@Override
		public boolean dealMessage(Message message) {
			String tenantId = userLoginDao.findTenantId();
			System.out.println("接受到短信发送回执：");
			System.out.println("message handle: " + message.getReceiptHandle());
			System.out.println("message body: " + message.getMessageBodyAsString());
			System.out.println("message id: " + message.getMessageId());
			System.out.println("message dequeue count:" + message.getDequeueCount());
			//以短信回执消息处理为例
			try{
				Type contenType = new TypeToken<HashMap<String, Object>>() {}.getType();
			    Map<String,Object> contentMap=gson.fromJson(message.getMessageBodyAsString(), contenType);
				Boolean success=(Boolean)contentMap.get("success");
				String outId=(String)contentMap.get("out_id");
				String sendTime=(String)contentMap.get("send_time");
				String reportTime=(String)contentMap.get("report_time");
				String errCode=(String)contentMap.get("err_code");
				String errMsg=(String)contentMap.get("err_msg");
				SmsRecord record = new SmsRecord();
				record.setId(outId);
				if (success) {
					record.setStatus(Constant.SMS_RECEIVE_SUCCESS);
				} else {
					record.setStatus(Constant.SMS_RECEIVE_FAILED);
				}
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				record.setSendTime(format.parse(sendTime));
				record.setReportTime(format.parse(reportTime));
				record.setErrorCode(errCode);
				record.setErrorMsg(errMsg);
				smsDao.updateById(tenantId, record);
				//TODO 这里开始写业务代码
			}catch(com.google.gson.JsonSyntaxException | ParseException e){
			   e.printStackTrace();
			}
			Boolean dealResult=true;
			return dealResult;//返回true，则工具类自动删除已拉取的消息。
		}
	}
	
	@Override
	public void startSmsReportPuller() throws ClientException, ParseException {
		DefaultAlicomMessagePuller puller=new DefaultAlicomMessagePuller();

		//设置异步线程池大小及任务队列的大小，还有无数据线程休眠时间
		puller.setConsumeMinThreadSize(6);
		puller.setConsumeMaxThreadSize(16);
		puller.setThreadQueueSize(200);
		puller.setPullMsgThreadSize(1);
		//和服务端联调问题时开启,平时无需开启，消耗性能
		puller.openDebugLog(false);

		/*
		* TODO 将messageType和queueName替换成您需要的消息类型名称和对应的队列名称
		*云通信产品下所有的回执消息类型:
		*1:短信回执：SmsReport，
		*2:短息上行：SmsUp
		*3:语音呼叫：VoiceReport
		*4:流量直冲：FlowReport
		*/
		//puller.startReceiveMsg(accessKeyId, accessSecret, "SmsReport", smsReportQueueId, new SmsMessageListener());
	}
}
