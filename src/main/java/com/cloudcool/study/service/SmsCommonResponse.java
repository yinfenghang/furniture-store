package com.cloudcool.study.service;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.aliyuncs.CommonResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class SmsCommonResponse {
	private final int TYPE_RESPONSE = 0;
	private final int TYPE_EXCEPTION = 1;
	private final String IN_EXC_MSG = "isv.INTERNAL_EXCEPTION";
	private int type;
	private String code;
	private String message;
	private CommonResponse response;
	private Map<String, Object> contentMap;
	private Gson gson = new Gson();
	
	public SmsCommonResponse(CommonResponse response) {
		this.type = TYPE_RESPONSE;
		this.response = response;
		Type contenType = new TypeToken<HashMap<String, Object>>() {}.getType();
		this.contentMap = gson.fromJson(response.getData(), contenType);
	}
	
	public SmsCommonResponse(String message) {
		this.type = TYPE_EXCEPTION;
		this.code = IN_EXC_MSG;
		this.message = message;
	}
	
	public Map<String, Object> getContentMap() {
		if (type == TYPE_RESPONSE) {
			return contentMap;
		} else {
			return new HashMap<>();
		}
	}
	
	public String getBizId() {
		if (type == TYPE_RESPONSE) {
			return (String)contentMap.get("BizId");
		} else {
			return "";
		}
	}
	
	public String getRequestId() {
		if (type == TYPE_RESPONSE) {
			return (String)contentMap.get("RequestId");
		} else {
			return "";
		}
	}
	
	public String getCode() {
		if (type == TYPE_RESPONSE) {
			return (String)contentMap.get("Code");
		} else {
			return code;
		}
	}
	
	public String getMessage() {
		if (type == TYPE_RESPONSE) { 
			return (String)contentMap.get("Message");
		} else {
			return message;
		}
	}
	
	public boolean isSuccess() {
		if (type == TYPE_RESPONSE) {
			return response.getHttpResponse().isSuccess();
		} else {
			return false;
		}
	}
}
