package com.cloudcool.study.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.SubscribePackagePermissionEvaluation;
import com.cloudcool.study.dao.SubscribePackageDao;
import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SubscribePackage;
import com.cloudcool.study.entity.SysDataResource;

@Service
public class SubscribePackageServiceImpl implements SubscribePackageService {
	@Autowired
	private SubscribePackageDao packageDao;
	@Autowired
	private SubscribePackagePermissionEvaluation subscribePackagePermissionEvaluation;
	
	@Override
	@PreAuthorize("@subscribePackagePermissionEvaluation.hasReadSubscribePackagePermission()")
	public ServiceResult<SubscribePackage> findById(String subscribePackId) {
		if (subscribePackId == null) {
			return new ServiceResult<>("订阅包ID不能为空");
		}
		
		SubscribePackage pack = packageDao.findById(subscribePackId);
		if (pack == null) {
			return new ServiceResult<>("未查询到订阅包，请刷新页面后重试");
		}
		
		List<SysDataResource> limits = subscribePackagePermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(pack, "subscribePackage", limits);
	}
	
	@Override
	@PreAuthorize("@subscribePackagePermissionEvaluation.hasReadSubscribePackagePermission()")
	public ServiceResult<SubscribePackage> readAll() {
		List<SubscribePackage> packResult = packageDao.findAll();
		if (packResult == null) {
			return new ServiceResult<>("查询功能订购包失败");
		}
		
		List<SysDataResource> limits = subscribePackagePermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(packResult, "subscribePackage", limits);
	}

	@Override
	@PreAuthorize("@subscribePackagePermissionEvaluation.hasCreateSubscribePackagePermission()")
	public ServiceResult<SubscribePackage> create(SubscribePackage pack) {
		String check = SubscribePackage.isValid(pack, null);
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		pack = packageDao.create(pack);
		if (pack == null) {
			return new ServiceResult<>("创建功能包失败");
		}

		List<SysDataResource> limits = subscribePackagePermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(pack, "subscribePackage", limits);
	}

	@Override
	@PreAuthorize("@subscribePackagePermissionEvaluation.hasUpdateSubscribePackagePermission()")
	public ServiceResult<SubscribePackage> update(SubscribePackage pack) {
		List<SysDataResource> limits = subscribePackagePermissionEvaluation.getFieldPermissions();
		
		if (pack.getName() == null && pack.getId() == null) {
			return new ServiceResult<>("功能包Id和功能包名不能同时为空");
		}
		
		String check = SubscribePackage.isValid(pack, limits);
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		pack = packageDao.update(pack, limits);
		if (pack == null) {
			return new ServiceResult<>("更新功能包失败，请重试");
		}
		return new ServiceResult<>(pack, "subscribePackage", limits);
	}

	@Override
	@PreAuthorize("@subscribePackagePermissionEvaluation.hasDeleteSubscribePackagePermission()")
	public ServiceResult<SubscribePackage> delete(SubscribePackage pack) {
		if (pack.getId() == null && pack.getName() == null) {
			return new ServiceResult<>("功能包Id和功能包名不能同时为空");
		}
		
		boolean result = packageDao.delete(pack);
		if (result) {
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("删除功能包失败，请重试");
		}
	}
}
