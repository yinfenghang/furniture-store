package com.cloudcool.study.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.UserPermissionEvaluation;
import com.cloudcool.study.dao.UpdateOptimisticLockException;
import com.cloudcool.study.dao.UpdateParamInvalidException;
import com.cloudcool.study.dao.UserDao;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.util.Constant;

@Service
public class UserServiceImpl implements UserService  {
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private UserPermissionEvaluation userPermissionEvaluation;

	@Override
	@PreAuthorize("@userPermissionEvaluation.hasCreateUserPermission()")
	public ServiceResult<User> createUser(User user) {
		/* 只能创建本租户的用户 */
		String tenantId = userLoginDao.findTenantId();
		user.setTenantId(tenantId);
		
		String check = User.isValid(user, null);
		/* invalid inputs */
		if (check != null) {
			return new ServiceResult<User>(check);
		}

		user = userDao.create(user);
		if (user == null) {
			return new ServiceResult<User>("创建用户失败");
		}
		List<SysDataResource> dataResource = userPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<User>(user, "user", dataResource);
	}
	
	@Nullable
	@PreAuthorize("isAnonymous()")
	public User createUserByRegister(User user) {
		String check = User.isValid(user, null);
		if (check != null) {
			System.out.println("用户校验失败：" + check);
			return null;
		}
		if (user.getTenantId() == null || user.getTenantId().isEmpty()) {
			System.out.println("未指定有效租户");
			return null;
		}
		
		return userDao.create(user);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasDeleteUserPermission(#user.username)")
	public ServiceResult<User> delete(User user) {
		if (user == null || user.getId() == null || user.getId().isEmpty())
			return new ServiceResult<>("未指定需要删除的用户");
		
		boolean result = userDao.invalid(user);
		if (result) {
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("删除失败，请重试");
		}
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasDeleteUserPermission(#username)")
	public ServiceResult<User> delete(String username) {
		if (username== null || username.isEmpty())
			return new ServiceResult<>("未指定需要删除的用户");
		
		boolean result = userDao.invalid(username);
		if (result) {
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("删除失败，请重试");
		}
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasReadUserPermission(#username)")
	public ServiceResult<User> read(String username) {
		if (username== null || username.isEmpty())
			return new ServiceResult<>("未指定需要查询的用户");
		
		User user = userDao.findByUsername(username);
		List<SysDataResource> limits = userPermissionEvaluation.getFieldPermissions(username);
		return new ServiceResult<>(user, "user", limits);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasReadUserPermission()")
	public ServiceResult<User> readAll() {
		String tenantId = userLoginDao.findTenantId();
		List<User> users = userDao.findAll(tenantId);
		for (User user : users) {
			if (UserLoginDao.isSelf(user.getUsername())) {
				users.remove(user);
				break;
			}
		}
		List<SysDataResource> limits = userPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(users, "user", limits);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasReadUserPermission()")
	public ServiceResult<User> searchUser(String content) {
		String tenantId = userLoginDao.findTenantId();
		List<User> users = userDao.search(tenantId, content);
		for (User user : users) {
			if (UserLoginDao.isSelf(user.getUsername())) {
				users.remove(user);
				break;
			}
		}
		List<SysDataResource> limits = userPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(users, "user", limits);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasReadUserPermission()")
	public ServiceResult<User> searchByRoleAndContent(String role, String content) {
		String tenantId = userLoginDao.findTenantId();
		List<User> users = userDao.search(tenantId, role, content);
		List<SysDataResource> limits = userPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(users, "user", limits);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasReadUserPermission()")
	public ServiceResult<User> readAllPaging(int start, int size) {
		String tenantId = userLoginDao.findTenantId();
		List<User> users = userDao.findAll(tenantId, start, size);
		for (User user : users) {
			if (UserLoginDao.isSelf(user.getUsername())) {
				users.remove(user);
				break;
			}
		}
		List<SysDataResource> limits = userPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(users, "user", limits);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasReadUserPermission()")
	public ServiceResult<User> countAll() {
		String tenantId = userLoginDao.findTenantId();
		int count = userDao.count(tenantId);
		return new ServiceResult<>(count);
	}
	
	@Override
	@PreAuthorize("@userPermissionEvaluation.hasUpdateUserPermission(#user.username)")
	public ServiceResult<User> update(User user) {
		List<SysDataResource> dataResources = userPermissionEvaluation.getFieldPermissions(user.getUsername());
		String check = User.isValid(user, dataResources);
		/* invalid inputs */
		if (check != null) {
			return new ServiceResult<User>(check);
		}
		
		try {
			user = userDao.update(user, dataResources);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return new ServiceResult<>(e.getMessage());
		}
		List<SysDataResource> limits = userPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(user, "user", limits);
	}
	
	public boolean isUserExist(String username) {
		if (userDao.countByUsername(username) > 0) {
			return true;
		}
		return false;
	}

	public boolean isSystemAdmin(User user) {
		List<String> roles = user.getRoles();
		for (String role : roles) {
			if (role.equals(Constant.ROLE_SYSTEM_ADMIN)) {
				return true;
			}
		}
		return false;
	}

	public boolean isTenantAdmin(User user) {
		List<String> roles = user.getRoles();
		for (String role :roles) {
			if (role.equals(Constant.ROLE_TENANT_ADMIN)) {
				return true;
			}
		}
		return false;
	}
}
