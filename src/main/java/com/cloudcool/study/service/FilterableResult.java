package com.cloudcool.study.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cloudcool.study.entity.SysDataResource;

public class FilterableResult<T> {
	private List<T> items = new ArrayList<>();
	private Map<String, List<SysDataResource>> limit = new HashMap<>();
	
	public FilterableResult() {
		super();
	}
	public FilterableResult(List<T> items, Map<String, List<SysDataResource>> limit) {
		super();
		this.items = items;
		this.limit = limit;
	}
	public List<T> getItems() {
		return items;
	}
	public T getItemAt(int index) {
		if (items == null || items.size() <= index)
			return null;
		
		return items.get(index);
	}
	public void setItems(List<T> items) {
		this.items = items;
	}
	
	public Map<String, List<SysDataResource>> getLimit() {
		return limit;
	}
	public void setLimit(Map<String, List<SysDataResource>> limit) {
		this.limit = limit;
	}
	public List<SysDataResource> getLimit(String resource) {
		return limit.get(resource);
	}
	@Override
	public String toString() {
		return "FilterableResult [items=" + items + ", limit=" + limit + "]";
	}	
}
