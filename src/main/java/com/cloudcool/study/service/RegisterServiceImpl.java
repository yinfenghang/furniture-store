package com.cloudcool.study.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudcool.study.dao.SmsRegsiterCodeRecordDao;
import com.cloudcool.study.entity.SmsRecord;
import com.cloudcool.study.entity.SmsRegisterCodeRecord;
import com.cloudcool.study.util.PhoneValidator;
import com.cloudcool.study.util.RegisterCodeGenerator;

@Service
public class RegisterServiceImpl implements RegisterService {
	@Autowired
	private SmsRegsiterCodeRecordDao recordDao;
	@Autowired
	private SmsService smsService;
	
	@Override
	public ServiceResult<SmsRegisterCodeRecord> getRegisterCode(String phone) {
		if (phone == null || !PhoneValidator.isPhone(phone)) {
			return new ServiceResult<>("电话号码为空或格式不正确");
		}
		
		String code = RegisterCodeGenerator.generate();
		SmsRegisterCodeRecord record = new SmsRegisterCodeRecord(phone, code);
		record = recordDao.create(record);
		boolean sended = smsService.sendRegisterCode(phone, code, record.getId());
		if (sended) {
			return new ServiceResult<>(record);
		} else {
			String errorMsg = smsService.getErrorMsgByOutId(record.getId());
			if (errorMsg == null) {
				return new ServiceResult<>("发送失败");
			} else {
				return new ServiceResult<>(errorMsg);
			}
		}
	}
	
	@Override
	public ServiceResult<SmsRegisterCodeRecord> isRegsiterCodeRight(String phone, String code) {
		if (phone == null || !PhoneValidator.isPhone(phone)) {
			return new ServiceResult<>("电话号码为空或格式不正确");
		}
		
		if (code == null || code.length() != 6) {
			return new ServiceResult<>("验证码错误");
		}
		
		SmsRegisterCodeRecord record = recordDao.findByPhone(phone);
		if (record == null) {
			return new ServiceResult<>("验证码错误");
		}
		
		Date now = new Date();
		if ((now.getTime() - record.getLastModifiedTime().getTime()) > 5 * 60 * 1000) {
			recordDao.removeByPhone(phone);
			return new ServiceResult<>("验证码错误");
		}
		
		if (record.getRegisterCode().equals(code)) {
			recordDao.removeByPhone(phone);
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("验证码错误");
		}
	}
}
