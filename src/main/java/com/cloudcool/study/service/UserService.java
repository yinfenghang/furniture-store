package com.cloudcool.study.service;

import com.cloudcool.study.entity.User;

public interface UserService {
	/* create */
	public ServiceResult<User> createUser(User user);
	public User createUserByRegister(User user);
	/* delete */
	public ServiceResult<User> delete(User user);
	public ServiceResult<User> delete(String username);
	/* read */
	public ServiceResult<User> read(String username);
	public ServiceResult<User> readAll();
	public ServiceResult<User> countAll();
	public ServiceResult<User> readAllPaging(int start, int size);
	public ServiceResult<User> searchUser(String content);
	public ServiceResult<User> searchByRoleAndContent(String role, String content);
	/* update */
	public ServiceResult<User> update(User user);
	/* tools */
	public boolean isUserExist(String username);
	public boolean isSystemAdmin(User user);
	public boolean isTenantAdmin(User user);
}
