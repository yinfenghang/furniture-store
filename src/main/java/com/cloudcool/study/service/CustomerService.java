package com.cloudcool.study.service;

import org.springframework.stereotype.Service;

import com.cloudcool.study.entity.Customer;

public interface CustomerService {
	/* create */
	public ServiceResult<Customer> createCustomer(Customer customer);

	/* read */
	public ServiceResult<Customer> getCustomerById(String customerid);
	public ServiceResult<Customer> getCustomersPageable(int start, int size);
	public ServiceResult<Customer> countCustomers();
	public ServiceResult<Customer> searchCustomersByOrder(String conent);
	
	/* update */
	public ServiceResult<Customer> updateCustomer(Customer customer);

	/* delete */
	public ServiceResult<Customer> deleteCustomer(String customer_id);
}
