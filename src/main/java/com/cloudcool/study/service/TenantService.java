package com.cloudcool.study.service;

import com.cloudcool.study.entity.AccountRecord;
import com.cloudcool.study.entity.Tenant;
import com.cloudcool.study.entity.Tenant.CashoutAccount;
import com.cloudcool.study.entity.TenantAgent;

public interface TenantService {
	/* create */
	public Tenant createTenant(Tenant tenant);
	/* delete */
	public boolean deleteTenant(String tenantId);
	public ServiceResult<Tenant> deleteReq(String tenantId);
	public ServiceResult<Tenant> deleteReq(Tenant tenant);
	/* read */
	public ServiceResult<Tenant> read(String tenantId);
	/* update */
	public ServiceResult<Tenant> update(Tenant tenant);
	/* recharge */
	public ServiceResult<Tenant> recharge(String tenantId, double amount);
	public ServiceResult<AccountRecord> readRechargeRecordPageable(String tenantId, int start, int size);
	/* subscribe */
	public ServiceResult<Tenant> subscribe(String tenantId, String subscribePackId, int unit);
	public ServiceResult<AccountRecord> readSubscribeRecord(String tenandId);
	public ServiceResult<Tenant> placeorder(String tenantId, int type, String name, int count);
	/* waterbill */
	public ServiceResult<AccountRecord> readWaterbill(String tenantId);
	public ServiceResult<AccountRecord> readWaterbillCount(String tenantId);
	public ServiceResult<AccountRecord> readWaterbillPageable(String tenantId, int start, int size);
	/* cashout */
	public ServiceResult<Tenant> cashout(String tenantId, double amount);
	public ServiceResult<AccountRecord> readCashoutRecordPageable(String tenantId, int start, int size);
	public ServiceResult<Tenant> changeCashoutAccount(Tenant tenant);
	/* export */
	public ServiceResult<Tenant> exportData(String tenantId);
	/* add agent */
	public ServiceResult<TenantAgent> getAllAgent(String tenantId);
	public ServiceResult<TenantAgent> addAgent(String tenantId, String agent);
	public ServiceResult<TenantAgent> addCategory(String tenantId, String category);
	public ServiceResult<TenantAgent> removeAgent(String tenantId, String agent);
	public ServiceResult<TenantAgent> removeCategory(String tenantId, String category);
}
