package com.cloudcool.study.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.CustomerPermissionEvaluation;
import com.cloudcool.study.dao.CustomerDao;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.entity.SysDataResource;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private CustomerPermissionEvaluation customerPermissionEvaluation;

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasCreateCustomerPermission()")
	public ServiceResult<Customer> createCustomer(Customer customer) {
		String username = UserLoginDao.getSelfUsername();
		String tenantId = userLoginDao.findTenantId();
		customer.setCreater(username);
		customer.setTenantId(tenantId);
		
		String check = Customer.isValid(customer, null);
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		customer = customerDao.create(customer);
		if (customer == null) {
			return new ServiceResult<>("创建客户失败，请重试");
		}
		
		List<SysDataResource> limits = customerPermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(customer, "customer", limits);
	}

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasReadCustomerPermission(#customerid)")
	public ServiceResult<Customer> getCustomerById(String customerid) {
		String tenantId = userLoginDao.findTenantId();
		Customer customer = customerDao.findByIdAndTenantId(customerid, tenantId);
		if (customer == null) {
			return new ServiceResult<>("未查询到客户信息");
		}
		
		List<SysDataResource> limit = null;
		String username = UserLoginDao.getSelfUsername();
		if (username.equals(customer.getCreater())) {
			limit = customerPermissionEvaluation.getFieldPermissions();
		} else {
			limit = customerPermissionEvaluation.getFieldPermissions("customerOther");
		}

		return new ServiceResult<>(customer, "customer", limit);
	}

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasUpdateCustomerPermission(#customer.getId())")
	public ServiceResult<Customer> updateCustomer(Customer customer) {
		List<SysDataResource> limit = null;
		String username = UserLoginDao.getSelfUsername();
		if (username.equals(customer.getCreater())) {
			limit = customerPermissionEvaluation.getFieldPermissions();
		} else {
			limit = customerPermissionEvaluation.getFieldPermissions("customerOther");
		}
		
		String tenantId = userLoginDao.findTenantId();
		customer.setTenantId(tenantId);
		String check = Customer.isValid(customer, limit);
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		customer = customerDao.update(customer, limit);
		if (customer == null) {
			return new ServiceResult<>("未查询到客户信息");
		}

		return new ServiceResult<>(customer, "customer", limit);
	}

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasReadCustomerPermission()")
	public ServiceResult<Customer> getCustomersPageable(int start, int size) {
		String tenantId = userLoginDao.findTenantId();
		List<Customer> customers = null;
		List<SysDataResource> limit = null;
		if (customerPermissionEvaluation.hasPermission("customer-readOther")) {
			customers = customerDao.findCustomersPageable(tenantId, start, size);
			limit = customerPermissionEvaluation.getFieldPermissions("customerOther");
		} else {
			String username = UserLoginDao.getSelfUsername();
			customers = customerDao.findSelfCustomersPageable(username, tenantId, start, size);
			limit = customerPermissionEvaluation.getFieldPermissions("customer");
		}
		
		if (customers == null) {
			return new ServiceResult<>("未查询到有效客户信息");
		}
		
		return new ServiceResult<>(customers, "customer", limit);
	}

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasReadCustomerPermission()")
	public ServiceResult<Customer> countCustomers() {
		String tenantId = userLoginDao.findTenantId();
		int count = 0;
		if (customerPermissionEvaluation.hasPermission("customer-readOther")) {
			count = customerDao.countCustomers(tenantId);
		} else {
			String username = UserLoginDao.getSelfUsername();
			count = customerDao.countSelfCustomers(username, tenantId);
		}
		
		if (count < 0) {
			count = 0;
		}
		
		return new ServiceResult<>(count);
	}

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasDeleteCustomerPermission(#customer_id)")
	public ServiceResult<Customer> deleteCustomer(String customer_id) {
		String tenantId = userLoginDao.findTenantId();
		boolean result = customerDao.invalid(tenantId, customer_id);
		if (result) {
			return new ServiceResult<>(1);
		}
		
		return new ServiceResult<>(0);
	}

	@Override
	@PreAuthorize("@customerPermissionEvaluation.hasSearchCustomerByOrderPermission()")
	public ServiceResult<Customer> searchCustomersByOrder(String content) {
		String tenantId = userLoginDao.findTenantId();
		List<Customer> customers = customerDao.searchByNameOrPhone(tenantId, content);
		if (customers == null) {
			return new ServiceResult<>("未查找到指定客户，请修改后重试");
		}
		
		List<SysDataResource> limit = customerPermissionEvaluation.getFieldPermissions("customerOther");
		return new ServiceResult<>(customers, "customer", limit);
	}
}