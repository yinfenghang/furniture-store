package com.cloudcool.study.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cloudcool.study.Security.FunctionPackagePermissionEvaluation;
import com.cloudcool.study.dao.FunctionPackageDao;
import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SysDataResource;

@Service
public class FunctionPackageServiceImpl implements FunctionPackageService {

	@Autowired
	private FunctionPackageDao functionPackageDao;
	@Autowired
	private FunctionPackagePermissionEvaluation functionPackagePermissionEvaluation;

	@Override
	@PreAuthorize("@functionPackagePermissionEvaluation.hasCreateFunctionPackagePermission()")
	public ServiceResult<FunctionPackage> create(FunctionPackage function) {
		String check = FunctionPackage.isValid(function, null);
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		function = functionPackageDao.create(function);
		if (function == null) {
			return new ServiceResult<>("创建功能包失败");
		}

		List<SysDataResource> limits = functionPackagePermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(function, "functionPackage", limits);
	}

	@Override
	@PreAuthorize("@functionPackagePermissionEvaluation.hasReadFunctionPackagePermission()")
	public ServiceResult<FunctionPackage> readAll() {
		List<FunctionPackage> packs = functionPackageDao.findAll();
		if (packs == null) {
			return new ServiceResult<>(new ArrayList<>());
		}
		
		List<SysDataResource> limits = functionPackagePermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(packs, "functionPackage", limits);
	}
	
	@Override
	@PreAuthorize("@functionPackagePermissionEvaluation.hasUpdateFunctionPackagePermission()")
	public ServiceResult<FunctionPackage> update(FunctionPackage function) {
		List<SysDataResource> limits = functionPackagePermissionEvaluation.getFieldPermissions();
		
		if (function.getName() == null && function.getId() == null) {
			return new ServiceResult<>("功能包Id和功能包名不能同时为空");
		}
		
		String check = FunctionPackage.isValid(function, limits);
		if (check != null) {
			return new ServiceResult<>(check);
		}
		
		function = functionPackageDao.update(function, limits);
		if (function == null) {
			return new ServiceResult<>("更新功能包失败，请重试");
		}
		return new ServiceResult<>(function, "functionPackage", limits);
	}

	@Override
	@PreAuthorize("@functionPackagePermissionEvaluation.hasDeleteFunctionPackagePermission()")
	public ServiceResult<FunctionPackage> delete(FunctionPackage function) {
		if (function.getId() == null && function.getName() == null) {
			return new ServiceResult<>("功能包Id和功能包名不能同时为空");
		}
		
		boolean result = functionPackageDao.delete(function);
		if (result) {
			return new ServiceResult<>(1);
		} else {
			return new ServiceResult<>("删除功能包失败，请重试");
		}
	}
	
	@Override
	@PreAuthorize("@functionPackagePermissionEvaluation.hasReadFunctionPackagePermission()")
	public ServiceResult<FunctionPackage> findByNameAndType(String name, int type) {
		FunctionPackage pack = functionPackageDao.findByNameAndType(name, type);
		if (pack == null) {
			return new ServiceResult<>("无效功能包");
		}
		
		List<SysDataResource> limits = functionPackagePermissionEvaluation.getFieldPermissions();
		return new ServiceResult<>(pack, "functionPackage", limits);
	}
}
