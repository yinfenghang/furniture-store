package com.cloudcool.study.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cloudcool.study.Security.OrderPermissionEvaluation;
import com.cloudcool.study.controller.AjaxResponse;
import com.cloudcool.study.controller.AjaxResponse.ReturnState;
import com.cloudcool.study.dao.OrderDao;
import com.cloudcool.study.dao.UpdateOptimisticLockException;
import com.cloudcool.study.dao.UpdateParamInvalidException;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.entity.Order;
import com.cloudcool.study.entity.Order.Consignee;
import com.cloudcool.study.entity.OrderItem;
import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.util.Constant;

@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private OrderPermissionEvaluation orderPermissionEvaluation;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private ProductService productService;
	
	private String getOrderLimitStr(int state) {
		String stateStr;
		switch (state) {
		case Constant.ORDER_VERIFY:
			stateStr = "orderVerify";
			break;
		case Constant.ORDER_CASHIN:
			stateStr = "orderCashin";
			break;
		case Constant.ORDER_STOREOUT:
			stateStr = "orderStoreout";
			break;
		case Constant.ORDER_DELIVERY:
			stateStr = "orderDelivery";
			break;
		case Constant.ORDER_INSTALL:
			stateStr = "orderInstall";
			break;
		case Constant.ORDER_FINISH:
			stateStr = "orderFinish";
			break;
		default:
			return null;
		}
		
		return stateStr;
	}
	
	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasCreateOrderPermission()")
	public ServiceResult<Order> createOrder(Order order) {
		String tenantId = userLoginDao.findTenantId();
		order.setTenantId(tenantId);
		
		order = orderDao.create(order);
		if (order == null) {
			return new ServiceResult<>("创建订单失败");
		} else {
			return new ServiceResult<>(order);
		}
	}
	
	@Override
	@Transactional
	@PreAuthorize("@orderPermissionEvaluation.hasCreateOrderPermission()")
	public ServiceResult<Order> checkAndCreateOrder(Order order) throws RuntimeException {
		Customer customer = order.getCustomer();
		// 新建客户
		if (customer.getId() == null || customer.getId().isEmpty()) {
			ServiceResult<Customer> result = customerService.createCustomer(customer);
			if (result.isFailed()) {
				// TODO: 创建客户信息失败，记录
				return new ServiceResult<>("创建客户信息失败，请重试");
			} else {
				customer = result.getData(0);
				if (customer != null) {
					order.setCustomer(customer);
				} else {
					return new ServiceResult<>("创建客户信息失败，请重试");
				}
			}
		// 查询客户
		} else {
			ServiceResult<Customer> result = customerService.getCustomerById(customer.getId());
			if (result.isFailed()) {
				return new ServiceResult<>("未查询到指定客户信息");
			} else {
				customer = result.getData(0);
				order.setCustomer(customer);
			}
		}
		
		ServiceResult<Order> result = null;
		if (order.getId() != null && !order.getId().isEmpty()) {
			try {
				result = updateCreate(order);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RuntimeException(ex.getMessage());
			}
		} else {
			result = createOrder(order);
		}
		
		if (result.isFailed()) {
			throw new RuntimeException(result.getErrorMsg());
		}
		if (result.getData(0) == null) {
			throw new RuntimeException("创建订单失败，请重试");
		}
		
		return result;
	}

	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasOperateOrderPermission(#state)")
	public ServiceResult<Order> getOrdersByStatePageable(int state, int start, int size) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		
		List<Order> orders = null;
		if (state == Constant.ORDER_VERIFY || state == Constant.ORDER_STOREOUT) {
			orders = orderDao.findByTenantIdAndStatePageable(tenantId, state, start, size);
		} else if (state == Constant.ORDER_CASHIN){
			orders = orderDao.findByTenandIdAndGuiderAndStatePageable(tenantId, username, state, start, size);
		} else if (state == Constant.ORDER_DELIVERY) {
			orders = orderDao.findByTenantIdAndDeliveryAndStatePageable(tenantId, username, state, start, size);
		} else if (state == Constant.ORDER_INSTALL) {
			orders = orderDao.findByTenantIdAndInstallerAndStatePageable(tenantId, username, state, start, size);
		} else {
			return new ServiceResult<>("指定的订单状态有误");
		}

		if (orders == null || orders.size() <= 0) {
			return new ServiceResult<>("未查询到订单");
		}
		
		String stateStr = getOrderLimitStr(state);
		if (stateStr == null)  {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}
		
		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);
		return new ServiceResult<>(orders, "order", limit);
	}

	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasOperateOrderPermission(#state)")
	public ServiceResult<Order> countOrdersByState(int state) {
		String tenantId = userLoginDao.findTenantId();
		int count = orderDao.countOrdersByTenandIdAndState(tenantId, state);
		if (count < 0) {
			count = 0;
		}
		return new ServiceResult<>(count);
	}

	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasOperateOrderPermission(#state)")
	public ServiceResult<Order> getOrderByStateAndOrderNo(int state, String orderNo) {
		String tenantId = userLoginDao.findTenantId();
		Order order = orderDao.findByTenantIdAndStateAndOrderNo(tenantId, state, orderNo);
		if (order == null) {
			return new ServiceResult<>("未查找到指定的订单，请确定订单号和订单状态是否正确");
		}
		
		String stateStr = getOrderLimitStr(state);
		if (stateStr == null) {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}

		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);
		return new ServiceResult<>(order, "order", limit);
	}
	
	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasReadOrderPermission(#orderNo)")
	public ServiceResult<Order> getOrderByOrderNo(String orderNo) {
		String tenantId = userLoginDao.findTenantId();
		Order order = orderDao.findByTenantIdAndOrderNo(tenantId, orderNo);
		if (order == null) {
			return new ServiceResult<>("未查找到指定的订单，请确定订单号和订单状态是否正确");
		}
		
		String stateStr = getOrderLimitStr(order.getState());
		if (stateStr == null) {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}

		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);
		return new ServiceResult<>(order, "order", limit);
	}
	
	public ServiceResult<Order> updateCreate(Order order)  throws RuntimeException {
		String tenantId = userLoginDao.findTenantId();
		
		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions("order");
		int oldState = order.getState();
		try {
			order = orderDao.update(tenantId, order, oldState, limit);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		if (order == null) {
			throw new RuntimeException("更新订单信息失败，未找到对应订单");
		}
		
		return new ServiceResult<>(order);
	}

	@Override
	@Transactional
	@PreAuthorize("@orderPermissionEvaluation.hasOperateOrderPermission(#order.getState())")
	public ServiceResult<Order> update(int newSate, Order order)  throws RuntimeException {
		String tenantId = userLoginDao.findTenantId();
		
		String stateStr = getOrderLimitStr(order.getState());
		if (stateStr == null) {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}
		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);

		int oldState = order.getState();
		order.setState(newSate);
		try {
			order = orderDao.update(tenantId, order, oldState, limit);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		if (order == null) {
			throw new RuntimeException("更新订单信息失败，未找到对应订单");
		}
		
		return new ServiceResult<>(order);
	}
	
	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasOperateOrderPermission(#state)")
	public ServiceResult<Order> getSelfCreateOrderByState(int state, String orderNo) {
		String username = UserLoginDao.getSelfUsername();
		String tenandId = userLoginDao.findTenantId();
		Order order = orderDao.findByTenantIdAndGuiderAndState(tenandId, username, state);
		if (order == null) {
			return new ServiceResult<>("未查询到指定订单");
		}
		
		String stateStr = getOrderLimitStr(state);
		if (stateStr == null) {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}

		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);
		return new ServiceResult<>(order, "order", limit);
	}
	
	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasCreateOrderPermission()")
	public ServiceResult<Order> getSelfCreateOrderByOrderNo(String orderNo) {
		String username = UserLoginDao.getSelfUsername();
		String tenandId = userLoginDao.findTenantId();
		Order order = orderDao.findByTenantIdAndGuiderAndOrderNo(tenandId, username, orderNo);
		if (order == null) {
			return new ServiceResult<>("未查询到指定订单");
		}
		
		String stateStr = getOrderLimitStr(order.getState());
		if (stateStr == null) {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}

		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);
		return new ServiceResult<>(order, "order", limit);
	}

	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasReadOrderPermission()")
	public ServiceResult<Order> getUncompleteOrdersPageable(int start, int size) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		List<Order> orders;
		List<SysDataResource> limit;
		if (orderPermissionEvaluation.hasPermission("order-readOther")) {
			orders = orderDao.findByTenandIdAndExStatePageable(tenantId, Constant.ORDER_FINISH, start, size);
			limit = orderPermissionEvaluation.getFieldPermissions("order");
		} else if (orderPermissionEvaluation.hasPermission("order-read")) {
			orders = orderDao.findByTenandIdAndGuiderAndExStatePageable(tenantId, username, Constant.ORDER_FINISH, start, size);
			limit = orderPermissionEvaluation.getFieldPermissions("order");
		} else {
			return new ServiceResult<>("无查看订单权限");
		}
		
		if (orders == null || orders.size() <= 0) {
			return new ServiceResult<>("未查询到有效订单");
		}
		
		return new ServiceResult<>(orders, "order", limit);
	}
	
	@Override
	public ServiceResult<Order> countUncompleteOrdersPageable() {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		int count = 0;
		if (orderPermissionEvaluation.hasPermission("order-readOther")) {
			count = orderDao.countByTenandIdAndStatePageable(tenantId, Constant.ORDER_FINISH);
		} else if (orderPermissionEvaluation.hasPermission("order-read")) {
			count = orderDao.countByTenandIdAndGuiderAndStatePageable(tenantId, username, Constant.ORDER_FINISH);
		} else {
			return new ServiceResult<>("无查看订单权限");
		}
		
		return new ServiceResult<>(count);
	}
	
	@Override
	@PreAuthorize("@orderPermissionEvaluation.hasReadOrderPermission()")
	public ServiceResult<Order> getCompleteOrdersPageable(int start, int size) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		List<Order> orders;
		List<SysDataResource> limit;
		if (orderPermissionEvaluation.hasPermission("order-readOther")) {
			orders = orderDao.findByTenandIdAndStatePageable(tenantId, Constant.ORDER_FINISH, start, size);
			limit = orderPermissionEvaluation.getFieldPermissions("order");
		} else if (orderPermissionEvaluation.hasPermission("order-read")) {
			orders = orderDao.findByTenandIdAndGuiderAndStatePageable(tenantId, username, Constant.ORDER_FINISH, start, size);
			limit = orderPermissionEvaluation.getFieldPermissions("order");
		} else {
			return new ServiceResult<>("无查看订单权限");
		}
		
		if (orders == null || orders.size() <= 0) {
			return new ServiceResult<>("未查询到有效订单");
		}
		
		return new ServiceResult<>(orders, "order", limit);
	}
	
	@Override
	public ServiceResult<Order> countCompleteOrdersPageable() {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		int count = 0;
		if (orderPermissionEvaluation.hasPermission("order-readOther")) {
			count = orderDao.countByTenandIdAndStatePageable(tenantId, Constant.ORDER_FINISH);
		} else if (orderPermissionEvaluation.hasPermission("order-read")) {
			count = orderDao.countByTenandIdAndGuiderAndStatePageable(tenantId, username, Constant.ORDER_FINISH);
		} else {
			return new ServiceResult<>("无查看订单权限");
		}
		
		return new ServiceResult<>(count);
	}
	
	@Override
	@Transactional
	@PreAuthorize("@orderPermissionEvaluation.hasOperateOrderPermission(#order.getState())")
	public ServiceResult<Order> orderStoreout(int newState, Order order) throws RuntimeException {
		String tenantId = userLoginDao.findTenantId();
		
		String stateStr = getOrderLimitStr(order.getState());
		if (stateStr == null) {
			return new ServiceResult<>("指定的订单状态错误，请重试");
		}
		List<SysDataResource> limit = orderPermissionEvaluation.getFieldPermissions(stateStr);

		int oldState = order.getState();
		order.setState(newState);
		try {
			order = orderDao.update(tenantId, order, oldState, limit);
		} catch (UpdateParamInvalidException | UpdateOptimisticLockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		if (order == null) {
			throw new RuntimeException("更新订单信息失败，未找到对应订单");
		}
		
		List<OrderItem> orderItems = order.getItems();
		for (OrderItem orderItem : orderItems) {
			Product product = orderItem.getProduct();
			if (product == null)
				continue;
			
			int number = orderItem.getNumber();
			ServiceResult<Product> productResult = productService.storeout(tenantId, product, number);
			if (productResult.isFailed()) {
				throw new RuntimeException("商品出库失败：" + productResult.getErrorMsg());
			}
		}
		
		return new ServiceResult<>(order);
	}
}
