package com.cloudcool.study.service;

import com.cloudcool.study.entity.SmsRegisterCodeRecord;

public interface RegisterService {

	public ServiceResult<SmsRegisterCodeRecord>  getRegisterCode(String phone);
	public ServiceResult<SmsRegisterCodeRecord> isRegsiterCodeRight(String phone, String code);
}
