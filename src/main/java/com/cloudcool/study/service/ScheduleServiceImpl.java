package com.cloudcool.study.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cloudcool.study.dao.EventDao;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.dao.UserDao;
import com.cloudcool.study.entity.Schedule;

@Service
public class ScheduleServiceImpl implements ScheduleService {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private EventDao eventDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private SmsService smsService;

	@Override
	@Transactional
	public ServiceResult<Schedule> createSchedule(String title, String content, Date startTime, Date endTime,
			List<String> users, boolean notify) throws RuntimeException {
		if (users.size() <= 0) {
			return new ServiceResult<>("至少要有一位参与人");
		}
		
		String currUsername = UserLoginDao.getSelfUsername();
		String tenantId = userLoginDao.findTenantId();
		Set<String> usernames = new HashSet<>();
		for (int i = 0; i < users.size(); i++) {
			usernames.add(users.get(i));
		}
		
		Map<String, String> phones = new HashMap<>();
		List<Schedule> schedules = new ArrayList<>();
		for (String username : usernames) {
			Schedule schedule = new Schedule();
			schedule.setOwner(username);
			schedule.setStartTime(startTime);
			schedule.setEndTime(endTime);
			schedule.setTitle(title);
			schedule.setContent(content);
			schedule.setCreater(currUsername);
			schedule.setTenantId(tenantId);
			schedules.add(schedule);  
		}
		
		schedules = eventDao.create(schedules);
		if (schedules == null || schedules.size() <= 0) {
			throw new RuntimeException("创建日程失败，请重试");
		}
		
		if (notify) {
			// TODO: send sms here
			for (Schedule schedule : schedules) {
				String phone = userDao.getPhoneByUsername(schedule.getOwner());
				smsService.sendEventNotify(phone, startTime, title, schedule.getId());
			}
		}
		return new ServiceResult<>(schedules);
	}
	
}
