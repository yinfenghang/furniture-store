package com.cloudcool.study.service;

import java.util.List;

import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SysDataResource;

public interface FunctionPackageService {
	public ServiceResult<FunctionPackage> create(FunctionPackage function);
	public ServiceResult<FunctionPackage> update(FunctionPackage function);
	public ServiceResult<FunctionPackage> delete(FunctionPackage function);
	public ServiceResult<FunctionPackage> readAll();
	public ServiceResult<FunctionPackage> findByNameAndType(String name, int type);
}
