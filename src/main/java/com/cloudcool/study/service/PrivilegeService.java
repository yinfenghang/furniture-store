package com.cloudcool.study.service;

import java.util.List;
import java.util.Map;

import com.cloudcool.study.entity.Privilege;
import com.cloudcool.study.entity.SysDataResource;

public interface PrivilegeService {
	/* create */
	public Privilege createPrivilege(Privilege privilege);
	/* read */
	public Map<String, List<SysDataResource>> getAllDataResources();
}
