package com.cloudcool.study.service;
import com.cloudcool.study.entity.Order;


public interface OrderService {
	/* create */
	public ServiceResult<Order> createOrder(Order order);
	public ServiceResult<Order> checkAndCreateOrder(Order order) throws RuntimeException;
	
	/* read */
	public ServiceResult<Order> getOrdersByStatePageable(int state, int start, int size);
	public ServiceResult<Order> countOrdersByState(int state);
	public ServiceResult<Order> getOrderByStateAndOrderNo(int state, String orderNo);
	public ServiceResult<Order> getOrderByOrderNo(String orderNo);
	public ServiceResult<Order> getSelfCreateOrderByOrderNo(String orderNo);
	public ServiceResult<Order> getSelfCreateOrderByState(int state, String orderNo);
	public ServiceResult<Order> countUncompleteOrdersPageable();
	public ServiceResult<Order> countCompleteOrdersPageable();
	/* get uncomplete and complete orders */
	public ServiceResult<Order> getUncompleteOrdersPageable(int start, int size);
	public ServiceResult<Order> getCompleteOrdersPageable(int start, int size);
	/* update */
	public ServiceResult<Order> update(int newState, Order order) throws RuntimeException;
	public ServiceResult<Order> orderStoreout(int newState, Order order) throws RuntimeException;

	
}
