package com.cloudcool.study.service;

import java.util.List;

import com.cloudcool.study.entity.Task;

public interface TaskService {
	public Task createTask(Task task);
	public List<Task> getUncompleteTask(String user);
	public Task updateProgress(Task task);
}
