package com.cloudcool.study;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.Privilege;
import com.cloudcool.study.entity.Product.Size;
import com.cloudcool.study.entity.ProductBundle;
import com.cloudcool.study.entity.ProductBundle.ProductItem;
import com.cloudcool.study.entity.ProductSingle;
import com.cloudcool.study.entity.Role;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.Tenant;
import com.cloudcool.study.entity.TenantAgent;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.FunctionPackageService;
import com.cloudcool.study.service.PrivilegeService;
import com.cloudcool.study.service.RoleService;
import com.cloudcool.study.service.SmsService;
import com.cloudcool.study.service.TenantService;
import com.cloudcool.study.service.UserService;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;
import com.cloudcool.study.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

@Component
public class SystemInitializerIgnore {
	
	@Value(value="classpath:users.json")
	private org.springframework.core.io.Resource userResource;
	
	//private String userFileName;
	@Value(value="classpath:role_aftersale_manager.json")
	private org.springframework.core.io.Resource roleAftersaleManagerResource;
	@Value(value="classpath:role_aftersale_order.json")
	private org.springframework.core.io.Resource roleAftersaleOrderResource;
	@Value(value="classpath:role_aftersale_service.json")
	private org.springframework.core.io.Resource roleAftersaleServiceResource;
	@Value(value="classpath:role_customer_manager.json")
	private org.springframework.core.io.Resource roleCustomerManagerResource;
	@Value(value="classpath:role_system_admin.json")
	private org.springframework.core.io.Resource roleSystemAdminResource;
	@Value(value="classpath:role_tenant_admin.json")
	private org.springframework.core.io.Resource roleTenantAdminResource;
	@Value(value="classpath:role_tenant_audit.json")
	private org.springframework.core.io.Resource roleTenantAuditResource;
	@Value(value="classpath:role_tenant_delivery.json")
	private org.springframework.core.io.Resource roleTenantDeliveryResource;
	@Value(value="classpath:role_tenant_installer.json")
	private org.springframework.core.io.Resource roleTenantInstalleresource;
	@Value(value="classpath:role_tenant_seller.json")
	private org.springframework.core.io.Resource roleTenantSellerResource;
	@Value(value="classpath:role_tenant_user.json")
	private org.springframework.core.io.Resource roleTenantUserResource;
	@Value(value="classpath:role_tenant_whmanager.json")
	private org.springframework.core.io.Resource roleTenantWHManagerResource;
	
	//private String roleFileName;
	@Value(value="classpath:permission.json")
	private org.springframework.core.io.Resource permissionResource;
	
	@Value(value="classpath:tenant.json")
	private org.springframework.core.io.Resource tenantResource;
	
	@Value(value="classpath:Package.json")
	private org.springframework.core.io.Resource packageResource;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private SmsService smsService;
	
	private static final String classPrefix = "com.cloudcool.study.entity";
	private String getDataName(String resourceId, String dataCode) {
		if (resourceId.endsWith("Other")) {
			resourceId = resourceId.substring(0, resourceId.indexOf("Other"));
		}
		String className = classPrefix + "." + Utils.UpperPrefixChar(resourceId);
		try {
			Class<?> clazz = Class.forName(className);
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				if (field.getName().equals(dataCode)) {
					Note note = field.getAnnotation(Note.class);
					if (note == null || note.value() == null || note.value().isEmpty())
						return null;
					return note.value();
				}
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		return null;
	}
	
	public List<Role> insertRoles(org.springframework.core.io.Resource resource) throws Exception{
		InputStream inputStream = resource.getInputStream();
		if (inputStream == null) {
			throw new Exception("initialization role file not found: " + resource.getFilename());
		}
		InputStreamReader inputReader = new InputStreamReader(inputStream);
		Type roleTokenType = new TypeToken<ArrayList<Role>>() {}.getType();
		ArrayList<Role> roles  = new Gson().fromJson(inputReader, roleTokenType);
		for (Role role : roles) {
			Map<String, List<SysDataResource>> columnPrivileges = role.getColumnPrivileges();
			for (String key : columnPrivileges.keySet()) {
				List<SysDataResource> dataResources = columnPrivileges.get(key);
				for (SysDataResource dataResource : dataResources) {
					dataResource.setResourceId(key);
				}
			}
		}
		return (ArrayList<Role>) saveAll(roles);
	}
	
	@PostConstruct
	public boolean initialize() throws Exception {
		
		deleteAll();
		
		/* permission */
		InputStream inputStream = permissionResource.getInputStream();
		if (inputStream == null) {
			throw new Exception("initialization permission file not found: " + permissionResource.getFilename());
		}
		InputStreamReader inputReader = new InputStreamReader(inputStream);
		Type permissionTokenType = new TypeToken<ArrayList<Privilege>>() {}.getType();
		ArrayList<Privilege> privileges = new Gson().fromJson(inputReader, permissionTokenType);
		privileges = (ArrayList<Privilege>) saveAll(privileges);
		
		inputStream = packageResource.getInputStream();
		if (inputStream == null) {
			throw new Exception("initialization role file not found: " + packageResource.getFilename());
		}

		inputReader = new InputStreamReader(inputStream);
		Type functionTokenType = new TypeToken<ArrayList<FunctionPackage>>() {}.getType();
		ArrayList<FunctionPackage> functions  = new Gson().fromJson(inputReader, functionTokenType);
		functions = (ArrayList<FunctionPackage>) saveAll(functions);
		
		List<Role> roles = insertRoles(roleAftersaleManagerResource);
		roles.addAll(insertRoles(roleAftersaleOrderResource));
		roles.addAll(insertRoles(roleAftersaleServiceResource));
		roles.addAll(insertRoles(roleCustomerManagerResource));
		roles.addAll(insertRoles(roleSystemAdminResource));
		roles.addAll(insertRoles(roleTenantAdminResource));
		roles.addAll(insertRoles(roleTenantAuditResource));
		roles.addAll(insertRoles(roleTenantDeliveryResource));
		roles.addAll(insertRoles(roleTenantInstalleresource));
		roles.addAll(insertRoles(roleTenantSellerResource));
		roles.addAll(insertRoles(roleTenantUserResource));
		roles.addAll(insertRoles(roleTenantWHManagerResource));
		
		inputStream = tenantResource.getInputStream();
		if (inputStream == null) {
			throw new Exception("initialization tenant file not found: " + tenantResource.getFilename());
		}
		inputReader = new InputStreamReader(inputStream);
		Type tenantTokenType = new TypeToken<ArrayList<Tenant>>() {}.getType();
		ArrayList<Tenant> tenants  = new Gson().fromJson(inputReader, tenantTokenType);
		tenants = (ArrayList<Tenant>) saveAll(tenants);
		Tenant tenant = tenants.get(0);
		/* create agent setting */
		TenantAgent agent = new TenantAgent();
		agent.setTenantId(tenant.getId());
		agent.getAgent().add("双虎家私");
		agent.getAgent().add("香山红叶");
		agent.getAgent().add("杂牌");
		agent.getCategories().add("床具");
		agent.getCategories().add("橱柜");
		agent.getCategories().add("床头柜");
		agent.getCategories().add("茶几");
		agent.getCategories().add("沙发");
		mongoTemplate.save(agent);
		

		inputStream = userResource.getInputStream();
		if (inputStream == null) {
			throw new Exception("initialization user file not found: " + userResource.getFilename());
		}
		inputReader = new InputStreamReader(inputStream);
		Type userTokenType = new TypeToken<ArrayList<User>>() {}.getType();
		ArrayList<User> users  = new Gson().fromJson(inputReader, userTokenType);
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			user.setPassword(user.getPassword());
			user.setBirthday(new Date());
			user.setTenantId(tenants.get(0).getId());
		}
		users = (ArrayList<User>) saveAll(users);
		
		ProductSingle single = new ProductSingle();
		single.setType(Constant.PRODUCT_SINGLE);
		single.setName("银丰行");
		single.setIdentifier("23942394");
		single.setIntroduction("测试产品");
		single.setPrice(23489.0);
		single.setMinPrice(8293.0);
		single.setInventory(23);
		single.setValid(true);
		single.setCreater(users.get(0).getUsername());
		single.setTenantId(tenant.getId());
		single.setCategory("床具");
		single.setBrand("香山红叶");
		single.setMaterial("实木");
		single.setColor("红色");
		single.setSize(new Size("238x389"));
		single = mongoTemplate.save(single);
		
		ProductSingle single1 = new ProductSingle();
		single1.setType(Constant.PRODUCT_SINGLE);
		single1.setName("银丰行111");
		single1.setIdentifier("2394239234");
		single1.setIntroduction("测试产品");
		single1.setPrice(23.0);
		single1.setMinPrice(34.0);
		single1.setInventory(2);
		single1.setValid(true);
		single1.setCreater(users.get(0).getUsername());
		single1.setTenantId(tenant.getId());
		single1.setCategory("床具");
		single1.setBrand("香山红叶");
		single1.setMaterial("实木");
		single1.setColor("红色");
		single1.setSize(new Size("238x389x89"));
		single1 = mongoTemplate.save(single1);
		
		ProductBundle bundle = new ProductBundle();
		bundle.setType(Constant.PRODUCT_COMBINATION);
		bundle.setName("或承诺过");
		bundle.setIdentifier("28304234");
		bundle.setIntroduction("测试产品");
		bundle.setPrice(23489.0);
		bundle.setMinPrice(8293.0);
		bundle.setValid(true);
		bundle.setCreater(users.get(0).getUsername());
		bundle.setTenantId(tenant.getId());
		bundle.getItems().add(new ProductItem(single, 23));
		bundle = mongoTemplate.save(bundle);
		
		Customer customer = new Customer();
		customer.setName("银丰行");
		customer.setAddress("山东省济宁市");
		customer.setPhone("19991208480");
		customer.setBirthday(new Date());
		customer.setSex(0);
		customer.setValid(true);
		customer.setCreater(users.get(0).getUsername());
		customer.setTenantId(tenant.getId());
		customer = mongoTemplate.save(customer);
		
		/* start sms report puller */
		try {
			smsService.startSmsReportPuller();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		System.out.println("yinfenghang --- start sms puller now");
		return true;
	}
	
	@PreDestroy
	public boolean preDestropy() throws Exception {
		deleteAll();
		return true;
	}
	
	public <T> List<T> saveAll(List<T> items) {
		Collection<T> result = mongoTemplate.insertAll(items);
		return new ArrayList<T>(result);
	}
	
	public void deleteAll() {
		mongoTemplate.remove(new Query(), "user");
		mongoTemplate.remove(new Query(), "privilege");
		mongoTemplate.remove(new Query(), "role");
		mongoTemplate.remove(new Query(), "tenant");
		mongoTemplate.remove(new Query(), "tenantAgent");
		mongoTemplate.remove(new Query(), "functionPackage");
		mongoTemplate.remove(new Query(), "accountRecord");
		mongoTemplate.remove(new Query(), "product");
		mongoTemplate.remove(new Query(), "shopcaret");
		mongoTemplate.remove(new Query(), "customer");
		mongoTemplate.remove(new Query(), "order");
	}
}
