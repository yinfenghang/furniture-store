package com.cloudcool.study;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories
@EnableMongoAuditing
public class MongoConfig extends AbstractMongoConfiguration {

	@Override
	public MongoClient mongoClient() {
		// TODO Auto-generated method stub
		return new MongoClient();
	}
	
	@Override
	protected String getDatabaseName() {
		// TODO Auto-generated method stub
		return "test";
	}

	@Override
	protected String getMappingBasePackage() {
		// TODO Auto-generated method stub
		return "com.cloudcool.study.dao";
	}
	
	@Bean
    MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {  
        return new MongoTransactionManager(dbFactory);
    }
}
