package com.cloudcool.study.Security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class FunctionPackagePermissionEvaluation {
	@Autowired
	private UserLoginDao userLoginDao;
	
	private boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	private boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasCreateFunctionPackagePermission() {
		return hasPermission("functionPackage-create");
	}
	
	public boolean hasUpdateFunctionPackagePermission() {
		return hasPermission("functionPackage-update");
	}
	
	public boolean hasReadFunctionPackagePermission() {
		return hasPermission("functionPackage-read");
	}
	
	public boolean hasDeleteFunctionPackagePermission() {
		return hasPermission("functionPackage-delete");
	}

	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get("functionPackage");
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
	
	public List<SysDataResource> getFieldPermissions(String resource) {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get(resource);
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
}
