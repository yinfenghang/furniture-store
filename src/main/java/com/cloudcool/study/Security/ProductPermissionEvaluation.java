package com.cloudcool.study.Security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class ProductPermissionEvaluation {
	@Autowired
	public UserLoginDao userLoginDao;
	
	private boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	private boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasCreateProductPermission() {
		return hasPermission("product-create");
	}
	
	public boolean hasUpdateProductPermission(String tenantId, String newId) {
		if (tenantId == null || tenantId.isEmpty())
			return false;
		
		String temp = userLoginDao.findTenantId();
		if (!temp.equals(tenantId)) {
			return false;
		}
		
		if (!temp.equals(newId)) {
			return false;
		}
		
		return hasPermission("product-update");
	}
	
	public boolean hasReadProductPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty())
			return false;
		
		String temp = userLoginDao.findTenantId();
		if (!temp.equals(tenantId)) {
			return false;
		}
		return hasPermission("product-read");
	}
	
	public boolean hasDeleteProductPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty())
			return false;
		
		String temp = userLoginDao.findTenantId();
		if (!temp.equals(tenantId)) {
			return false;
		}
		
		return hasPermission("product-delete");
	}
	
	public boolean hasStoreinPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty())
			return false;
		
		String temp = userLoginDao.findTenantId();
		if (!temp.equals(tenantId)) {
			return false;
		}
		
		return hasPermission("product-storein");
	}
	
	public boolean hasStoreoutPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty())
			return false;
		
		String temp = userLoginDao.findTenantId();
		if (!temp.equals(tenantId)) {
			return false;
		}
		
		return hasPermission("product-storeout");
	}
	
	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		return user.getColumnAuthorities().get("product");
	}
	
	public List<SysDataResource> getFieldPermissions(String resource) {
		User user = userLoginDao.findUser();
		return user.getColumnAuthorities().get(resource);
	}
}
