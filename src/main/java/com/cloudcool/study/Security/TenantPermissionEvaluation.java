package com.cloudcool.study.Security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class TenantPermissionEvaluation {
	@Autowired
	public UserLoginDao userLoginDao;
	
	private boolean selfBelongTo(String tenantId) {
		String selfTenantId = userLoginDao.findTenantId();
		if (selfTenantId.equals(tenantId))
			return true;
		return false;
	}
	
	private boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	private boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasReadTenantPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-readInfo");
	}
	
	public boolean hasUpdateTenantPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-updateInfo");
	}
	
	public boolean hasReqDeleteTenantPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-deleteReq");
	}
	
	public boolean hasSubscribePackagePermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-subscribe");
	}
	
	public boolean hasRechargePermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-recharge");
	}
	
	public boolean hasReadWaterbillPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-readWaterBill");
	}
	
	public boolean hasCashoutPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-cashout");
	}
	
	public boolean hasCashoutAccountPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-cashoutAccount");
	}
	
	public boolean hasExportDataPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-exportData");
	}
	
	public boolean hasViewReportPermission(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-readReport");
	}
	
	public boolean hasSetAgentPermisison(String tenantId) {
		if (tenantId == null || tenantId.isEmpty()) {
			return false;
		}
		
		if (!selfBelongTo(tenantId)) {
			return false;
		}
		
		return hasPermission("tenant-setAgent");
	}
	
	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get("tenant");
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
	
	public List<SysDataResource> getFieldPermissions(String resource) {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get(resource);
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
}
