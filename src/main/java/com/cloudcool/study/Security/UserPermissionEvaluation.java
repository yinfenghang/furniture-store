package com.cloudcool.study.Security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.UserDao;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class UserPermissionEvaluation {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private UserDao userDao;
	
	private boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	private boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean userBelongTenant(String username) {
		if (username == null || username.isEmpty()) {
			return false;
		}
		
		User user = userLoginDao.findUser();
		return userBelongTenant(user, username);
	}
	
	private boolean userBelongTenant(User user, String username) {
		if (user == null || username == null || username.isEmpty()) {
			return false;
		}
		
		String tenantId = user.getTenantId();
		User target = userDao.findByUsernameAndTenantId(username, tenantId);
		if (target == null) {
			return false;
		}
		
		return true;
	}
	
	public boolean hasCreateUserPermission() {
		return hasPermission("user-create");
	}
	
	public boolean hasDeleteUserPermission(String username) {
		if (username == null || username.isEmpty()) {
			return false;
		}
		
		User user = userLoginDao.findUser();
		if (user.getUsername().equals(username))
			return false;
		
		if (!hasPermission(user, "user-delete")) {
			return false;
		}
		
		if (userBelongTenant(user, username)) {
			return true;
		}
		
		return false;
	}
	
	public boolean hasReadUserPermission(String username) {
		if (username == null || username.isEmpty()) {
			return false;
		}
		
		User user = userLoginDao.findUser();
		if (user == null) {
			return false;
		}
		if (user.getUsername().equals(username)) {
			if (hasPermission(user, "user-read")) {
				return true;
			} else {
				return false;
			}
		} else {
			if (!hasPermission(user, "user-readOther")) {
				return false;
			}	
			
			if (userBelongTenant(user, username)) {
				return true;
			}
			return false;
		}
	}
	
	public boolean hasReadUserPermission() {
		return hasPermission("user-readOther");
	}
	
	public boolean hasUpdateUserPermission(String username) {
		if (username == null || username.isEmpty()) {
			return false;
		}
		
		User user = userLoginDao.findUser();
		if (user.getUsername().equals(username)) {
			if (hasPermission(user, "user-update"))
				return true;
			else
				return false;
		} else {
			if (!hasPermission(user, "user-updateOther"))
				return false;
			
			if (userBelongTenant(user, username)) {
				return true;
			}
			return false;
		}
	}
	
	public List<SysDataResource> getFieldPermissions(String username) {
		if (username == null || username.isEmpty()) {
			return getFieldPermissions();
		}

		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = null;
		if (user.getUsername().equals(username)) {
			dataResources = user.getColumnAuthorities().get("user");
		} else {
			dataResources = user.getColumnAuthorities().get("userOther");
		}
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}

	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get("userOther");
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
}
