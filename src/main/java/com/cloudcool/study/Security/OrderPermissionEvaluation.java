package com.cloudcool.study.Security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.dao.OrderDao;
import com.cloudcool.study.entity.Order;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.util.Constant;

@Component
public class OrderPermissionEvaluation {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private OrderDao orderDao;
	
	public boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	public boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasAddProductPermission(String username) {
		return hasCreateOrderPermission(username);
	}
	
	public boolean hasCreateOrderPermission(String username) {
		String temp = UserLoginDao.getSelfUsername();
		if (!temp.equals(username)) {
			return false;
		}
		
		return hasPermission("order-create");
	}
	
	public boolean hasReadOrderPermission() {
		return hasPermission("order-read") || hasPermission("order-readOther");
	}
	
	public boolean hasReadOrderPermission(String orderNo) {
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		
		if (hasPermission("order-readOther")) {
			Order order = orderDao.findByTenantIdAndOrderNo(tenantId, orderNo);
			if (order != null) {
				return true;
			}
			
			return false;
		}
		if (hasPermission("order-read")) {
			Order order = orderDao.findByTenantIdAndGuiderAndOrderNo(tenantId, username, orderNo);
			if (order != null) {
				return true;
			}
			return false;
		}
		
		return true;
	}
	
	public boolean hasCreateOrderPermission() {
		return hasPermission("order-create");
	}
	
	public boolean hasVerifyOrderPermission() {
		return hasPermission("order-verify");
	}
	
	public boolean hasOperateOrderPermission(int state) {
		if (!Constant.isOrderStatusValid(state)) {
			return false;
		}
		
		String permission = null;
		switch (state) {
		case Constant.ORDER_VERIFY:
			permission = "order-verify";
			break;
		case Constant.ORDER_CASHIN:
			permission = "order-cashin";
			break;
		case Constant.ORDER_STOREOUT:
			permission = "product-storeout";
			break;
		case Constant.ORDER_DELIVERY:
			permission = "order-delivery";
			break;
		case Constant.ORDER_INSTALL:
			permission = "order-install";
			break;
		default:
			return false;
		}
		
		return hasPermission(permission);
	}
	
	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get("order");
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
	
	public List<SysDataResource> getFieldPermissions(String resource) {
		User user = userLoginDao.findUser();
		List<SysDataResource> dataResources = user.getColumnAuthorities().get(resource);
		if (dataResources == null)
			return new ArrayList<>();
		
		return dataResources;
	}
}
