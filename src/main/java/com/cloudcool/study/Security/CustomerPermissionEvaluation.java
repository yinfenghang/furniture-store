package com.cloudcool.study.Security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.CustomerDao;
import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class CustomerPermissionEvaluation {
	@Autowired
	private UserLoginDao userLoginDao;
	@Autowired
	private CustomerDao customerDao;
	
	public boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	private boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasCreateCustomerPermission() {
		return hasPermission("customer-create");
	}
	
	public boolean hasReadCustomerPermission() {
		return hasPermission("customer-read") || hasPermission("customer-readOther");
	}
	
	public boolean hasReadCustomerPermission(String customerId) {
		if ( customerId == null)
			return false;
		
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		Customer customer = customerDao.findByIdAndTenantId(customerId, tenantId);
		if (customer == null)
			return false;
		
		if (username.equals(customer.getCreater())) {
			return hasPermission("customer-read");
		} else {
			return hasPermission("customer-readOther");
		}
	}
	
	public boolean hasSearchCustomerByOrderPermission() {
		return hasPermission("order-create");
	}	
	
	public boolean hasUpdateCustomerPermission(String customerId) {
		if ( customerId == null)
			return false;
		
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		Customer customer = customerDao.findByIdAndTenantId(customerId, tenantId);
		if (customer == null)
			return false;
		
		if (username.equals(customer.getCreater())) {
			return hasPermission("customer-update");
		} else {
			return hasPermission("customer-updateOther");
		}
	}
	
	public boolean hasDeleteCustomerPermission(String customerId) {
		if (customerId == null)
			return false;
		
		String tenantId = userLoginDao.findTenantId();
		String username = UserLoginDao.getSelfUsername();
		Customer customer = customerDao.findByIdAndTenantId(customerId, tenantId);
		if (customer == null)
			return false;
		
		if (username.equals(customer.getCreater())) {
			return hasPermission("customer-delete");
		} else {
			return hasPermission("customer-deleteOther");
		}
	}
	
	public boolean hasVerifyCustomerPermission(String customerId) {
		if ( customerId == null)
			return false;
		
		String tenantId = userLoginDao.findTenantId();
		Customer customer = customerDao.findByIdAndTenantId(customerId, tenantId);
		if (customer == null)
			return false;
		
		return hasPermission("customer-verifyDelete");
	}
	
	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		return user.getColumnAuthorities().get("customer");
	}
	
	public List<SysDataResource> getFieldPermissions(String resource) {
		User user = userLoginDao.findUser();
		return user.getColumnAuthorities().get(resource);
	}
}
