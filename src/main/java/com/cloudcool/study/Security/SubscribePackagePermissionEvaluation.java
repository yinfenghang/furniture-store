package com.cloudcool.study.Security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudcool.study.dao.UserLoginDao;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class SubscribePackagePermissionEvaluation {
	@Autowired
	public UserLoginDao userLoginDao;
	
	private boolean hasPermission(String privilege) {
		User user = userLoginDao.findUser();
		return hasPermission(user, privilege);
	}
	
	private boolean hasPermission(User user, String privilege) {
		if (user == null) {
			return false;
		}
		
		for (String authorize : user.getGrantedAuthorities()) {
			if (authorize.equals(privilege)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasReadSubscribePackagePermission() {
		return hasPermission("subscribePackage-read");
	}
	
	public boolean hasUpdateSubscribePackagePermission() {
		return hasPermission("subscribePackage-update");
	}
	
	public boolean hasDeleteSubscribePackagePermission() {
		return hasPermission("subscribePackage-delete");
	}
	
	public boolean hasCreateSubscribePackagePermission() {
		return hasPermission("subscribePackage-create");
	}
	
	public List<SysDataResource> getFieldPermissions() {
		User user = userLoginDao.findUser();
		return user.getColumnAuthorities().get("subscribePackage");
	}
	
	public List<SysDataResource> getFieldPermissions(String resource) {
		User user = userLoginDao.findUser();
		return user.getColumnAuthorities().get(resource);
	}
}
