package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

@Document
public class Task implements Serializable {
	@Id
	private String id;
	
	private String owner;
	private String creater;
	private String title;
	private String content;
	private int status;
	private int progress;
	private String remark;
	
	@CreatedDate
	private Date createTime;

	public Task() {}
	public Task(String id, String owner, String creater, String title, String content, int status, int progress, String remark) {
		super();
		this.id = id;
		this.owner = owner;
		this.creater = creater;
		this.title = title;
		this.status = status;
		this.content = content;
		this.progress = progress;
		this.remark = remark;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public static boolean isValid(Task task) {
		if (task == null ||
				Constant.isTaskStatusValid(task.getStatus()) || 
				task.getOwner() == null ||
				task.getCreater() == null ||
				task.getProgress() < 0) {
			return false;
		}
		
		return true;
	}
	public static boolean isValid(List<Task> tasks) {
		for (int i = 0; i < tasks.size(); i++) {
			if (Task.isValid(tasks.get(i))) {
				return false;
			}
		}
		
		return true;
	}
	@Override
	public String toString() {
		return "Task [id=" + id + ", user=" + owner + ", creater=" + creater + ", title=" + title + ", content="
				+ content + ", status=" + status + ", progress=" + progress + ", remark=" + remark + ", createTime="
				+ createTime + "]";
	}
}
