package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document
public class Schedule implements Serializable {
	@Id
	private String id;
	
	private String owner;
	private Date startTime;
	private Date endTime;
	private String title;
	private String content;
	private boolean notified;
	
	private String creater;
	private String tenantId;
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifiedTime;

	public Schedule() {}

	public Schedule(String id, String owner, Date startTime, Date endTime, String title, String content, boolean notified,
			String creater, String tenantId) {
		super();
		this.id = id;
		this.owner = owner;
		this.startTime = startTime;
		this.endTime = endTime;
		this.title = title;
		this.content = content;
		this.notified = notified;
		this.creater = creater;
		this.tenantId = tenantId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isNotified() {
		return notified;
	}

	public void setNotified(boolean notified) {
		this.notified = notified;
	}


	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public static boolean isValid(Schedule schedule) {
		if (schedule == null ||
				schedule.getCreateTime() == null ||
				schedule.getOwner() == null ||
				schedule.getCreater() == null ||
				schedule.getTenantId() == null) {
			return false;
		}
		
		return true;
	}
	
	public static boolean isValid(List<Schedule> schedules) {
		for (int i = 0; i < schedules.size(); i++) {
			if (Schedule.isValid(schedules.get(i))) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public String toString() {
		return "Schedule [id=" + id + ", owner=" + owner + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", title=" + title + ", content=" + content + ", notified=" + notified + ", createTime=" + createTime
				+ ", lastModifiedTime=" + lastModifiedTime + ", createrId=" + creater + ", tenantId=" + tenantId
				+ "]";
	}
}
