package com.cloudcool.study.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class OrderRecord {
	@Id
	private String id;
	
	private String orderId;
	private String operator;
	private String record;
	
	@CreatedDate
	private Date createdDate;
	
	public OrderRecord() {}

	public OrderRecord(String id, String orderId, String operator, String record, Date createdDate) {
		super();
		this.id = id;
		this.orderId = orderId;
		this.operator = operator;
		this.record = record;
		this.createdDate = createdDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "OrderRecord [id=" + id + ", orderId=" + orderId + ", operator=" + operator + ", record=" + record
				+ ", createdDate=" + createdDate + "]";
	}
}
