package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

/*
 * account water bill
 */
@Document
public class AccountRecord implements Serializable {
	@Id
	private String id;
	
	private Integer direction;
	private Double sum;
	private Integer type;
	private Date time;
	private String remark;
	private String tenantId;
	@CreatedDate
	private Date createTime;

	public AccountRecord() {}
	
	public AccountRecord(String id, int direction, double sum, Date time, int type, String remark, String tenantId) {
		super();
		this.id = id;
		this.direction = direction;
		this.sum = sum;
		this.time = time;
		this.type = type;
		this.remark = remark;
		this.tenantId = tenantId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getDirection() {
		return direction;
	}

	public void setDirection(Integer direction) {
		this.direction = direction;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public static boolean isValid(AccountRecord record) {
		if (record == null ||
				record.getSum() < 0 ||
				!Constant.isFlowValid(record.getDirection()) ||
				!Constant.isAccountPartValid(record.getType()) ||
				record.getTenantId() == null) {		
			return false;
		}
		
		return true;
	}
	
	public static boolean isValid(List<AccountRecord> records) {
		for (int i = 0; i < records.size(); i++) {
			if (AccountRecord.isValid(records.get(i))) {
				return false;
			}
		}
		
		return true;
	}
	@Override
	public String toString() {
		return "AccountRecord [id=" + id + ", direction=" + direction + ", sum=" + sum + ", type=" + type + ", time="
				+ time + ", remark=" + remark + ", tenantId=" + tenantId + ", createTime=" + createTime + "]";
	}
}
