package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

@Document
public class Notify implements Serializable {
	@Id
	private String id;
	
	private int source;
	private int level;
	private int type;
	private Date sendTime;
	private Date validTime;
	private String title;
	private String content;
	private boolean readed;

	private String sender;
	private String receiver;
	private String tenantId;
	
	@LastModifiedDate
	private Date lastModifiedTime;
	@CreatedDate
	private Date createTime;
	
	public Notify() {}

	public Notify(String id, int source, int level, int type, Date sendTime, Date validTime, String title,
			String content, boolean readed, String senderId, String receiverId,String tenantId) {
		super();
		this.id = id;
		this.source = source;
		this.level = level;
		this.type = type;
		this.sendTime = sendTime;
		this.validTime = validTime;
		this.title = title;
		this.content = content;
		this.readed = readed;
		this.sender = senderId;
		this.tenantId = tenantId;
		this.receiver= receiverId;
	}
	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiverId) {
		this.receiver = receiverId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getSource() {
		return source;
	}
	public void setSource(int source) {
		this.source = source;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isReaded() {
		return readed;
	}
	public void setReaded(boolean readed) {
		this.readed = readed;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public Date getValidTime() {
		return validTime;
	}
	public void setValidTime(Date validTime) {
		this.validTime = validTime;
	}
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static boolean isValid(Notify notify) {
		if (notify == null ||
				notify.getSender() == null ||
				notify.getReceiver() == null ||
				!Constant.isNotifySourceValid(notify.getSource()) ||
				!Constant.isNotifyLevelValid(notify.getLevel()) ||
				!Constant.isNotifyTypeValid(notify.getType()) ||
				notify.getTenantId() == null) {
			return false;
		}
		
		return true;
	}
	
	public static boolean isValid(List<Notify> notifies) {
		for (int i = 0; i < notifies.size(); i++) {
			if (!Notify.isValid(notifies.get(i))) {
				return false;
			}
		}
		
		return true;
	}
	@Override
	public String toString() {
		return "Notify [id=" + id + ", source=" + source + ", level=" + level + ", type=" + type + ", sendTime="
				+ sendTime + ", validTime=" + validTime + ", title=" + title + ", content=" + content + ", readed="
				+ readed + ", senderId=" + sender + ", receiverId=" + receiver + ", tenantId=" + tenantId
				+ ", lastModifiedTime=" + lastModifiedTime + ", createTime=" + createTime + "]";
	}
}
