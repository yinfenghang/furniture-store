package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.entity.Product.Size;
import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

@Document("product")
public class Product implements Serializable {
	@Id
	private String id;
	
	private Integer type;
	private String picture;
	private String identifier;
	private String name;
	private String introduction;
	private Double price;
	private Double minPrice;
	private Integer inventory;
	private String remark;
	private Boolean valid;
	private String creater;
	private String tenantId;
	
	@LastModifiedDate
	private Date lastModifiedTime;
	@CreatedDate
	private Date createTime;
	@Version
	private Integer version;
	
	public boolean isSingle() {
		return type == Constant.PRODUCT_SINGLE;
	}
	
	public Product() {}
	
	public Product(String id, int type, String picture, String identifier, String name, String introduction,
			double price, double minPrice, int inventory, String remark, boolean valid, String creater,
			String tenantId) {
		super();
		this.id = id;
		this.type = type;
		this.picture = picture;
		this.identifier = identifier;
		this.name = name;
		this.introduction = introduction;
		this.price = price;
		this.minPrice = minPrice;
		this.inventory = inventory;
		this.remark = remark;
		this.valid = valid;
		this.creater = creater;
		this.tenantId = tenantId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getName() {
		return name;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}
	public Integer getInventory() {
		return inventory;
	}
	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Boolean isValid() {
		return valid;
	}
	public Boolean getValid() {
		return valid;
	}
	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public static String isProductValid(Product product) {
		if (product == null) {
			return "未指定有效商品";
		}
		
		if (product.getCreater() == null || product.getCreater().isEmpty()) {
			return "未指定商品创建人";
		}
		
		if (product.getTenantId() == null || product.getTenantId().isEmpty()) {
			return "未指定商品所属租户";
		}
		
		if (product.getIdentifier() == null || product.getIdentifier().isEmpty()) {
			return "未指定商品编码";
		}
		
		if (product.getName() == null || product.getName().isEmpty()) {
			return "未指定商品名称";
		}
		
		if (product.getIntroduction() == null || product.getIntroduction().isEmpty()) {
			product.setIntroduction(product.getName());
		}
		
		if (product.getPrice() < 0) {
			return "商品价格不能小于0";
		}
		
		if (product.getMinPrice() < 0) {
			product.setMinPrice(product.getPrice());
		}
		
		if (product.getInventory() < 0) {
			product.setInventory(0);
		}
		
		if (!Constant.isProductTypeValid(product.getType())) {
			return "商品类型无效";
		}
		
		return null;
	}
	
	@Override
	public String toString() {
		return "Product [id=" + id + ", type=" + type + ", picture=" + picture + ", identifier=" + identifier
				+ ", name=" + name + ", introduction=" + introduction + ", price=" + price + ", minPrice=" + minPrice
				+ ", inventory=" + inventory + ", remark=" + remark + ", valid=" + valid + ", createrId=" + creater
				+ ", tenantId=" + tenantId + ", lastModifiedTime=" + lastModifiedTime + ", createTime=" + createTime
				+ ", version=" + version + "]";
	}
	
	public static class Size {
		private double H;
		private double W;
		private double L;
		
		public Size() {}
		
		public Size(String sizeStr) {
			String measurements[] = sizeStr.split("x");
			try {
				if (measurements.length == 3) {
					/* length, width, heigh */
					L = Double.valueOf(measurements[0].trim());
					W = Double.valueOf(measurements[1].trim());
					H = Double.valueOf(measurements[2].trim());
				} else if (measurements.length == 2) {
					L = Double.valueOf(measurements[0].trim());
					W = Double.valueOf(measurements[1].trim());
					H = 0.0;
				} else {
					L = H = W = 0.0;
				}
			} catch (Exception ex) {
				throw new IllegalArgumentException("Size参数错误");
			}
		}
		
		public Size(double h, double w, double l) {
			super();
			H = h;
			W = w;
			L = l;
		}
		public double getH() {
			return H;
		}
		public void setH(double h) {
			H = h;
		}
		public double getW() {
			return W;
		}
		public void setW(double w) {
			W = w;
		}
		public double getL() {
			return L;
		}
		public void setL(double l) {
			L = l;
		}
		
		@Override
		public String toString() {
			return "长：" + L + ", 宽：" + W + ", 高：" + H;
		}
	}
}
