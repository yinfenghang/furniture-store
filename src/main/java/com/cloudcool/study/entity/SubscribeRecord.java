package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document
public class SubscribeRecord implements Serializable {
	@Id
	private String id;
	
	private String creater;
	private FunctionPackage pack;
	private String tenantId;
	
	@CreatedDate
	private Date createTime;

	public SubscribeRecord() {}
	public SubscribeRecord(String id, String creater, FunctionPackage pack, String tenantId) {
		super();
		this.id = id;
		this.creater = creater;
		this.pack = pack;
		this.tenantId = tenantId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public FunctionPackage getPack() {
		return pack;
	}
	public void setPack(FunctionPackage pack) {
		this.pack = pack;
	}
	@Override
	public String toString() {
		return "SubscribeRecord [id=" + id + ", creater=" + creater + ", pack=" + pack + ", tenantId=" + tenantId
				+ ", createTime=" + createTime + "]";
	}
}
