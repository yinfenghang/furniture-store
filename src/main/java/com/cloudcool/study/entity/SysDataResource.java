package com.cloudcool.study.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SysDataResource {
	@Id
	private String id;
	
	private String resourceId;
	private String dataCode;
	private boolean isAccessible;
	private boolean isUpdateable;
	private boolean isEditable;
	
	@CreatedDate
	private Date createTime;
	
	public boolean same(SysDataResource dataResource) {
		if (dataResource.getResourceId().equals(this.resourceId) && dataResource.getDataCode().equals(this.dataCode)) {
			return true;
		}
		return false;
	}
	
	public boolean merge(SysDataResource dataResource) {
		isAccessible |= dataResource.isAccessible();
		isUpdateable |= dataResource.isUpdateable();
		isEditable |= dataResource.isEditable();
		return true;
	}
	
	public SysDataResource(String id, String resourceId, String dataCode, boolean isAccessible,
			boolean isUpdateable, boolean isEditable) {
		super();
		this.id = id;
		this.resourceId = resourceId;
		this.dataCode = dataCode;
		this.isAccessible = isAccessible;
		this.isUpdateable = isUpdateable;
		this.isEditable = isEditable;
	}

	public SysDataResource() {}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getDataCode() {
		return dataCode;
	}
	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public boolean isAccessible() {
		return isAccessible;
	}
	public void setAccessible(boolean isAccessible) {
		this.isAccessible = isAccessible;
	}
	public boolean isUpdateable() {
		return isUpdateable;
	}

	public void setUpdateable(boolean isUpdateable) {
		this.isUpdateable = isUpdateable;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public static boolean isValid(SysDataResource privilege) {
		if (privilege == null ||
				privilege.getResourceId() == null ||
				privilege.getDataCode() == null ||
				privilege.getCreateTime() == null)
		{
			return false;
		}
		
		return true;
	}
	public static boolean isValid(List<SysDataResource> privileges) {
		for (SysDataResource privilege : privileges) {
			if (!SysDataResource.isValid(privilege)) {
				return false;
			}
		}
		
		return true;
	}
	@Override
	public String toString() {
		return "SysDataResource [id=" + id + ", resourceId=" + resourceId + ", dataCode="
				+ dataCode + ", isAccessible=" + isAccessible + ", isUpdateable=" + isUpdateable + ", isEditable="
				+ isEditable + ", createTime=" + createTime + "]";
	}
}
