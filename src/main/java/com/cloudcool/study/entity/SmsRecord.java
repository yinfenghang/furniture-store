package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document
public class SmsRecord implements Serializable {
	@Id
	private String id;
	
	private int status;
	/* phone */
	private String phone;
	/* content */
	private String signName;
	private String templateCode;
	private String templateName;
	private String contentParam;
	/* time */
	private Date sendTime;
	private Date reportTime;
	private String errorCode;
	private String errorMsg;
	/* identifier */
	private String bizId;
	private String requestId;
	private String outId;
	/* remark */
	private String remark;

	private String sender;
	private String tenantId;
	
	@CreatedDate
	private Date createTime;

	public SmsRecord() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getContentParam() {
		return contentParam;
	}

	public void setContentParam(String contentParam) {
		this.contentParam = contentParam;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Date getReportTime() {
		return reportTime;
	}

	public void setReportTime(Date reportTime) {
		this.reportTime = reportTime;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getOutId() {
		return outId;
	}

	public void setOutId(String outId) {
		this.outId = outId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "SmsRecord [id=" + id + ", status=" + status + ", phone=" + phone + ", signName=" + signName
				+ ", templateCode=" + templateCode + ", templateName=" + templateName + ", contentParam=" + contentParam
				+ ", sendTime=" + sendTime + ", reportTime=" + reportTime + ", errorCode=" + errorCode + ", errorMsg="
				+ errorMsg + ", bizId=" + bizId + ", requestId=" + requestId + ", outId=" + outId + ", remark=" + remark
				+ ", sender=" + sender + ", tenantId=" + tenantId + ", createTime=" + createTime + "]";
	}
}
