package com.cloudcool.study.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TenantAgent {
	@Id
	private String id;
	private String tenantId;
	private List<String> agent = new ArrayList<>();
	private List<String> categories = new ArrayList<>();
	
	@CreatedDate
	private Date createTime;
	
	public TenantAgent() {}
	public TenantAgent(String id, String tenantId, List<String> agent, List<String> categories) {
		super();
		this.id = id;
		this.tenantId = tenantId;
		this.agent = agent;
		this.categories = categories;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public List<String> getAgent() {
		return agent;
	}
	public void setAgent(List<String> agent) {
		this.agent = agent;
	}
	public List<String> getCategories() {
		return categories;
	}
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}
	@Override
	public String toString() {
		return "TenantAgent [id=" + id + ", tenantId=" + tenantId + ", agent=" + agent + ", categories=" + categories
				+ ", createTime=" + createTime + "]";
	}
}
