package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document
public class Privilege implements Serializable {
	@Id
	private String id;

	private String resourceId;
	private String resourceName;
	private String resourceDescription;
	private Map<String, String> operations = new HashMap<String, String>();
	
	@CreatedDate
	private Date createTime;
	
	public Privilege() {}
	
	public Privilege(String id, String resourceId, String resourceName, String resourceDescription,
			Map<String, String> operations) {
		super();
		this.id = id;
		this.resourceId = resourceId;
		this.resourceName = resourceName;
		this.resourceDescription = resourceDescription;
		this.operations = operations;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceDescription() {
		return resourceDescription;
	}

	public void setResourceDescription(String resourceDescription) {
		this.resourceDescription = resourceDescription;
	}
	
	public void setOperations(Map<String, String> operations) {
		this.operations = operations;
	}

	public Map<String, String> getOperations() {
		return operations;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public static boolean isValid(Privilege privilege) {
		if (privilege == null ||
				privilege.getResourceId() == null)
			return false;
		
		return true;
	}

	public static boolean isValid(List<Privilege> privileges) {
		for (int i = 0; i < privileges.size(); i++) {
			if (!Privilege.isValid(privileges.get(i)))
				return false;
		}
		
		return true;
	}

	@Override
	public String toString() {
		return "Privilege [id=" + id + ", resourceId=" + resourceId + ", resourceName=" + resourceName
				+ ", resourceDescription=" + resourceDescription + ", operations=" + operations + 
				", createTime=" + createTime + "]";
	}
}
