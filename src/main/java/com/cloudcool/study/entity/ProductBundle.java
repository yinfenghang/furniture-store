package com.cloudcool.study.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document("product")
public class ProductBundle extends Product {
	private List<ProductItem> items = new ArrayList<>();

	public ProductBundle() {}
	
	public ProductBundle(List<ProductItem> items) {
		super();
		this.items = items;
	}

	public List<ProductItem> getItems() {
		return items;
	}

	public void setItems(List<ProductItem> items) {
		this.items = items;
	}
	
	public static String isValid(ProductBundle product) {
		if (product == null)
			return "指定商品未空";
		
		String check = Product.isProductValid(product);
		if (check != null)
			return check;
		
		if (product.getItems().size() <= 0) {
			return "组合商品至少要包含一种单品";
		}
		
		return null;
	}

	@Override
	public String toString() {
		return "ProductBundle [items=" + items + ", isSingle()=" + isSingle() + ", getId()=" + getId() + ", getType()="
				+ getType() + ", getIdentifier()=" + getIdentifier() + ", getName()=" + getName() + ", getPicture()="
				+ getPicture() + ", getIntroduction()=" + getIntroduction() + ", getPrice()=" + getPrice()
				+ ", getMinPrice()=" + getMinPrice() + ", getInventory()=" + getInventory() + ", getRemark()="
				+ getRemark() + ", isValid()=" + isValid() + ", getTenantId()=" + getTenantId() + ", getCreateTime()="
				+ getCreateTime() + ", getCreater()=" + getCreater() + ", getLastModifiedTime()="
				+ getLastModifiedTime() + ", getVersion()=" + getVersion() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	public static class ProductItem {
		@DBRef
		private Product item;
		private Integer number;
		
		public static String isValid(ProductItem temp) {
			if (temp.getItem() == null) {
				return "未指定包含商品";
			}
			
			if (temp.getNumber() <= 0) {
				return "包含的商品数量不能 < 0";
			}
			
			return null;
		}
		
		public ProductItem(Product item, Integer number) {
			this.item = item;
			this.number = number;
		}
		
		public ProductItem() {
			super();
		}
		public Product getItem() {
			return item;
		}
		public void setItem(Product item) {
			this.item = item;
		}
		public Integer getNumber() {
			return number;
		}
		public void setNumber(Integer number) {
			this.number = number;
		}
		@Override
		public String toString() {
			return "ProductItem [item=" + item + ", number=" + number + "]";
		}
	}
}
