package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document
public class Role implements Serializable {
	@Id
	private String id;
	
	private String name;
	private String nickName;
	private String description;
	private boolean builtin;

	private List<Privilege> privileges = new ArrayList<>();
	private Map<String, List<SysDataResource>> columnPrivileges = new HashMap<>();
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifiedTime;

	public Role() {}
	
	public Role(String id, String name, String nickName, String description, boolean builtin,
			List<Privilege> privileges, Map<String, List<SysDataResource>> columnPrivileges) {
		super();
		this.id = id;
		this.name = name;
		this.nickName = nickName;
		this.description = description;
		this.builtin = builtin;
		this.privileges = privileges;
		this.columnPrivileges = columnPrivileges;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public boolean isBuiltin() {
		return builtin;
	}
	public void setBuiltin(boolean builtin) {
		this.builtin = builtin;
	}

	public Map<String, List<SysDataResource>> getColumnPrivileges() {
		return columnPrivileges;
	}

	public void setColumnPrivileges(Map<String, List<SysDataResource>> columnPrivileges) {
		this.columnPrivileges = columnPrivileges;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public static boolean isValid(Role role) {
		if (role == null ||
				role.getName() == null ||
				role.getColumnPrivileges() == null ||
				!Privilege.isValid(role.getPrivileges())) {
			return false;
		}
		
		return true;
	}
	
	public static boolean isValid(List<Role> roles) {
		for (int i = 0; i < roles.size(); i++) {
			if (!Role.isValid(roles.get(i)))
				return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", nickName=" + nickName + ", description=" + description
				+ ", builtin=" + builtin + ", privileges=" + privileges + ", columnPrivileges=" + columnPrivileges
				+ ", createTime=" + createTime + ", lastModifiedTime=" + lastModifiedTime + "]";
	}
}
