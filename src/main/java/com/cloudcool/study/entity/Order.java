package com.cloudcool.study.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.DateValidator;
import com.cloudcool.study.util.Note;
import com.cloudcool.study.util.PhoneValidator;

@Document("order")
public class Order implements Serializable {
	@Id
	private String id;
	
	private String orderNo;
	private Integer state;
	/* 当前处理人 */
	private String currentHandler;
	private String currentPhone;
	private String currentName;
	/* 客户和收货人信息 */
	@DBRef
	private Customer customer;
	private Consignee consignee;
	/* 带单人信息 */
	private Introductor introductor;
	/* 导购信息 */
	private Participant guider;
	/* 审核人信息 */
	private Participant approver;
	/* 配送人信息 */
	private Participant deliveryer;
	/* 安装人信息 */
	private Participant installer;
	/* 出库人信息 */
	private Participant storeouter;
	
	/* 订单备注 */
	private String remark;
	/* 总金额 */
	private Double total;
	/* 已收款 */
	private Double payment;
	/* 商品列表 */
	private List<OrderItem> items = new ArrayList<>();
	
	/* 所属租户 */
	private String tenantId;
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifyTime;
	@Version
	private Integer version;
	
	public int getItemNumber() {
		int total = 0;
		for (int i = 0; i < items.size(); i++) {
			total += items.get(i).getNumber();
		}
		
		return total;
	}
	
	public String getStateStr() {
		switch (state) {
		case Constant.ORDER_VERIFY:
			return "待审核";
		case Constant.ORDER_CASHIN:
			return "待收余款";
		case Constant.ORDER_STOREOUT:
			return "待出库";
		case Constant.ORDER_DELIVERY:
			return "待配送";
		case Constant.ORDER_INSTALL:
			return "待安装";
		case Constant.ORDER_FINISH:
			return "待完成";
		default:
			return "未知状态：" + state;
		}
	}

	public Order() {}
	
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Consignee getConsignee() {
		return consignee;
	}

	public void setConsignee(Consignee consignee) {
		this.consignee = consignee;
	}

	public Introductor getIntroductor() {
		return introductor;
	}

	public void setIntroductor(Introductor introductor) {
		this.introductor = introductor;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public List<OrderItem> getItems() {
		return items;
	}

	public void setItems(List<OrderItem> items) {
		this.items = items;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Participant getGuider() {
		return guider;
	}

	public void setGuider(Participant guider) {
		this.guider = guider;
	}

	public Participant getDeliveryer() {
		return deliveryer;
	}

	public void setDeliveryer(Participant deliveryer) {
		this.deliveryer = deliveryer;
	}

	public Participant getInstaller() {
		return installer;
	}

	public void setInstaller(Participant installer) {
		this.installer = installer;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Participant getApprover() {
		return approver;
	}

	public void setApprover(Participant approver) {
		this.approver = approver;
	}

	public Date getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}
	
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
	public String getCurrentHandler() {
		return currentHandler;
	}

	public void setCurrentHandler(String currentHandler) {
		this.currentHandler = currentHandler;
	}

	public String getCurrentPhone() {
		return currentPhone;
	}

	public void setCurrentPhone(String currentPhone) {
		this.currentPhone = currentPhone;
	}

	public String getCurrentName() {
		return currentName;
	}

	public void setCurrentName(String currentName) {
		this.currentName = currentName;
	}

	public Participant getStoreouter() {
		return storeouter;
	}

	public void setStoreouter(Participant storeouter) {
		this.storeouter = storeouter;
	}

	public Double getPayment() {
		return payment;
	}

	public void setPayment(Double payment) {
		this.payment = payment;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", orderNo=" + orderNo + ", state=" + state + ", currentHandler=" + currentHandler
				+ ", currentPhone=" + currentPhone + ", currentName=" + currentName + ", customer=" + customer
				+ ", consignee=" + consignee + ", introductor=" + introductor + ", guider=" + guider + ", approver="
				+ approver + ", deliveryer=" + deliveryer + ", installer=" + installer + ", storeouter=" + storeouter
				+ ", remark=" + remark + ", total=" + total + ", payment=" + payment + ", items=" + items
				+ ", tenantId=" + tenantId + ", createTime=" + createTime + ", lastModifyTime=" + lastModifyTime
				+ ", version=" + version + "]";
	}
	
	public static class Consignee {
		private String name;
		private String address;
		private String phone;
		private String remark;
		private Date deliveryTime;
		
		public String getDeliveryTimeStr() {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			return format.format(deliveryTime);
		}
		
		public static String isValid(Consignee consignee) {
			if (consignee == null) {
				return "请填写收货人信息";
			}
			
			if (consignee.getName() == null || consignee.getName().isEmpty()) {
				return "收货人姓名不能为空";
			}
			
			if (consignee.getAddress() == null || consignee.getAddress().isEmpty()) {
				return "收货人地址不能为空";
			}
			
			if (consignee.getPhone() == null || consignee.getPhone().isEmpty()) {
				return "收货人电话不能为空";
			}
			
			if (!PhoneValidator.isPhone(consignee.getPhone())) {
				return "收货人电话格式格式错误";
			}
			
			if (consignee.getDeliveryTime() == null) {
				return "收货时间不能为空";
			}
			
			if (!DateValidator.isDeliveryTimeValid(consignee.getDeliveryTime())) {
				return "收货时间不能比当前时间早";
			}
			
			return null;
		}
		
		public Consignee() {}
		public Consignee(String name, String address, String phone, String remark, Date deliveryTime) {
			super();
			this.name = name;
			this.address = address;
			this.phone = phone;
			this.remark = remark;
			this.deliveryTime = deliveryTime;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public Date getDeliveryTime() {
			return deliveryTime;
		}
		public void setDeliveryTime(Date deliveryTime) {
			this.deliveryTime = deliveryTime;
		}
		@Override
		public String toString() {
			return "Consignee [name=" + name + ", address=" + address + ", phone=" + phone + ", remark=" + remark
					+ ", deliveryTime=" + deliveryTime + "]";
		}
	}
	
	public static class Introductor {
		private String name;
		private String phone;
		
		public Introductor() {}
		public Introductor(String name, String phone) {
			super();
			this.name = name;
			this.phone = phone;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		@Override
		public String toString() {
			return "Introductor [name=" + name + ", phone=" + phone + "]";
		}
	}
	
	public static class Participant {
		private String participantId;
		private String participantName;
		private String participantPhone;
		
		public static String isValid(Participant part) {
			if (part.getParticipantId() == null || part.getParticipantId().isEmpty()) {
				return "用户名不能为空";
			}
			if (part.getParticipantName() == null || part.getParticipantName().isEmpty()) {
				return "用户姓名不能为空";
			}
			
			if (part.getParticipantPhone() == null || part.getParticipantPhone().isEmpty()) {
				return "用户电话不能为空";
			}
			
			if (!PhoneValidator.isPhone(part.getParticipantPhone())) {
				return "用户电话格式错误，请修改后重试";
			}
			
			return null;
		}
		
		public String getParticipantId() {
			return participantId;
		}
		public void setParticipantId(String participantId) {
			this.participantId = participantId;
		}
		public String getParticipantName() {
			return participantName;
		}
		public void setParticipantName(String participantName) {
			this.participantName = participantName;
		}
		public String getParticipantPhone() {
			return participantPhone;
		}
		public void setParticipantPhone(String participantPhone) {
			this.participantPhone = participantPhone;
		}
		public Participant(String participantId, String participantName, String participantPhone) {
			super();
			this.participantId = participantId;
			this.participantName = participantName;
			this.participantPhone = participantPhone;
		}
		public Participant() {};
		@Override
		public String toString() {
			return "Participant [participantId=" + participantId + ", participantName=" + participantName
					+ ", participantPhone=" + participantPhone + "]";
		}
	}
}
