package com.cloudcool.study.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;
import com.cloudcool.study.util.PhoneValidator;

@Document()
public class Customer implements Serializable {
	@Id
	private String id;
	
	private String name;
	private String phone;
	private String address;
	private Date birthday;
	private Integer sex;
	private String remark;
	private Boolean valid;
	private String creater;
	private String tenantId;
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifyTime;
	
	public static String isValid(Customer customer, List<SysDataResource> dataResources) {
		if (customer == null) {
			return "参数错误，客户对象为空";
		}
		
		if (dataResources == null) {
			if (customer.getName() == null || customer.getName().isEmpty()) {
				return "客户姓名不能为空";
			}
			
			if (customer.getPhone() == null) {
				return "客户电话不能为空";
			}
			if (!PhoneValidator.isPhone(customer.getPhone())) {
				return "客户电话格式错误，请输入11位手机号码";
			}
			
			if (customer.getCreater() == null) {
				return "客户创建人为空";
			}
			
			if (customer.getTenantId() == null) {
				return "未指定租户信息，请刷新后重试";
			}
		} else {
			Map<String, Boolean> limits = dataResources.stream().collect(Collectors.toMap(SysDataResource::getDataCode, p->p.isEditable()));
			if (limits.get("name") && customer.getName() != null && customer.getName().isEmpty()) {
				return "客户姓名不能为空";
			}
			
			if (limits.get("phone") && customer.getPhone() != null && !PhoneValidator.isPhone(customer.getPhone())) {
				return "客户电话格式错误，请输入11位手机号码";
			}
		}
		
		return null;
	}
	
	public Customer() {}
	public Customer(String id, String name, String phone, String address, Date birthday, int sex, String remark,
			boolean valid, String creater, String tenantId) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.birthday = birthday;
		this.sex = sex;
		this.remark = remark;
		this.valid = valid;
		this.creater = creater;
		this.tenantId = tenantId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getBirthday() {
		return birthday;
	}
	public String getBirthdayStr() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(birthday);
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Integer getSex() {
		return sex;
	}
	public String getSexStr() {
		if (sex == Constant.SEX_MAN) {
			return "男";
		} else {
			return "女";
		}
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Boolean isValid() {
		return valid;
	}
	public Boolean getValid() {
		return valid;
	}
	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getLastModifyTime() {
		return lastModifyTime;
	}
	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", phone=" + phone + ", address=" + address + ", birthday="
				+ birthday + ", sex=" + sex + ", remark=" + remark + ", valid=" + valid + ", createrId=" + creater
				+ ", createTime=" + createTime + ", lastModifyTime=" + lastModifyTime + ", tenantId=" + tenantId + "]";
	}
}
