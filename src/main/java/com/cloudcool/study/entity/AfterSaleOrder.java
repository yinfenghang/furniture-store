package com.cloudcool.study.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.PhoneValidator;

@Document
public class AfterSaleOrder {
	@Id
	private String id;
	
	@DBRef
	private Order order;
	private String customerName;
	private String customerPhone;
	private String detail;
	@DBRef
	private User server;
	private String creater;
	private String tenantId;
	
	@CreatedDate
	private Date createdTime;
	
	public static String isValid(AfterSaleOrder order) {
		if (order == null) {
			return "参数错误";
		}
		
		if (order.getCustomerName() == null || order.getCustomerName().isEmpty()) {
			return "客户姓名不能为空";
		}
		
		if (order.getCustomerPhone() == null || order.getCustomerPhone().isEmpty()) {
			return "客户电话不能为空";
		}
		
		if (!PhoneValidator.isPhone(order.getCustomerPhone())) {
			return "客户电话格式错误";
		}
		
		return null;
	}
	
	public AfterSaleOrder() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public User getServer() {
		return server;
	}

	public void setServer(User server) {
		this.server = server;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Override
	public String toString() {
		return "AfterSaleOrder [id=" + id + ", order=" + order + ", customerName=" + customerName + ", customerPhone="
				+ customerPhone + ", detail=" + detail + ", server=" + server + ", creater=" + creater + ", tenantId="
				+ tenantId + ", createdTime=" + createdTime + "]";
	}
}
