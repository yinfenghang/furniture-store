package com.cloudcool.study.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;
import com.cloudcool.study.util.PhoneValidator;

@Document
public class User  {
	@Transient
	public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
	
	@Id
	private String id;
	
	private String username;
	private String password;
	private String name;
	private String address;
	private String phone;
	private Date birthday;
	private String picture;
	private String position;
	private Double salary;
	private Integer sex;
	private Boolean valid;
	private List<String> roles = new ArrayList<>();
	
	private String tenantId;
	
	@Transient
	private List<String> grantedAuthorities;
	@Transient
	private Map<String, List<SysDataResource>> columnAuthorities;
	
	@LastModifiedDate
	private Date lastModifiedTime;
	@CreatedDate
	private Date createTime;
	@Version
	private Integer version;
	
	public User() {
		this.createTime = new Date();
	}

	public User(String id, String username, String password, String name, String address, String phone, Date birthday,
			String picture, String position, Double salary, Integer sex, Boolean valid, List<String> roles, String tenantId,
			List<String> grantedAuthorities, Map<String, List<SysDataResource>> columnAuthorities) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.birthday = birthday;
		this.picture = picture;
		this.position = position;
		this.salary = salary;
		this.sex = sex;
		this.valid = valid;
		this.roles = roles;
		this.tenantId = tenantId;
		this.grantedAuthorities = grantedAuthorities;
		this.columnAuthorities = columnAuthorities;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public void setPassword(String password) {
		try {
			this.password = PASSWORD_ENCODER.encode(password);
		} catch (NullPointerException ex) {
			//ex.printStackTrace();
			this.password = null;
		}
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	public Boolean isValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public List<String> getGrantedAuthorities() {
		return grantedAuthorities;
	}

	public void setGrantedAuthorities(List<String> grantedAuthorities) {
		this.grantedAuthorities = grantedAuthorities;
	}

	public Map<String, List<SysDataResource>> getColumnAuthorities() {
		return columnAuthorities;
	}

	public void setColumnAuthorities(Map<String, List<SysDataResource>> columnAuthorities) {
		this.columnAuthorities = columnAuthorities;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	public String getRoleStr(String roleId) {
		return Constant.getRoleStr(roleId);
	}
	
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getValid() {
		return valid;
	}

	public boolean hasRole(String role) {
		for (String roleStr : roles) {
			if (roleStr.equals(role)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static String isValid(User user, List<SysDataResource> dataResource) {
		if (user == null) {
			return "参数无效";
		}
		if (dataResource == null) {
			if (user.getUsername() == null || user.getUsername().isEmpty()) {
				return "用户名不能为空";
			}
			if (user.getName() == null || user.getName().isEmpty()) {
				return "用户姓名不能为空";
			}
			if (user.getPhone() == null || user.getPhone().isEmpty()) {
				return "用户电话不能空";
			}
			if(!PhoneValidator.isPhone(user.getPhone())) {
				return "输入的手机号码无效";
			}
			if (user.getBirthday() == null || user.getBirthday().after(new Date())) {
				return "生日无效";
			}
			if (user.getSex() == null || !Constant.isSexValid(user.getSex())) {
				return "性别无效";
			}
			if (user.getTenantId() == null || user.getTenantId().isEmpty()) {
				return "未指定租户信息";
			}
		} else {
			Map<String, Boolean> limits = dataResource.stream().collect(Collectors.toMap(SysDataResource::getDataCode, (p)->(p.isEditable())));
			if (limits.get("username") && (user.getUsername() != null && user.getUsername().isEmpty())) {
				return "用户名不能为空";
			}
			if (limits.get("name") &&(user.getName() != null && user.getName().isEmpty())) {
				return "用户姓名不能为空";
			}
			if (limits.get("phone") && (user.getPhone() != null && user.getPhone().isEmpty())) {
				return "用户电话不能空";
			}
			if (limits.get("phone") && (!PhoneValidator.isPhone(user.getPhone()))) {
				return "输入的手机号码无效";
			}
			if (limits.get("birthday") && (user.getBirthday() != null && user.getBirthday().after(new Date()))) {
				return "生日无效";
			}
			if (limits.get("sex") && (user.getSex() != null && !Constant.isSexValid(user.getSex()))) {
				return "性别无效";
			}
			if (limits.get("tenantId") && (user.getTenantId() != null && user.getTenantId().isEmpty())) {
				return "未指定租户信息";
			}
			if (user.getVersion() == null) {
				return "参数（版本号）错误";
			}
		}

		return null;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", name=" + name + ", address="
				+ address + ", phone=" + phone + ", birthday=" + birthday + ", picture=" + picture + ", position="
				+ position + ", salary=" + salary + ", sex=" + sex + ", valid=" + valid + ", roles=" + roles
				+ ", tenantId=" + tenantId + ", grantedAuthorities=" + grantedAuthorities + ", columnAuthorities="
				+ columnAuthorities + ", lastModifiedTime=" + lastModifiedTime + ", createTime=" + createTime
				+ ", version=" + version + "]";
	}
}
