package com.cloudcool.study.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

@Document
public class SubscribePackage {
	@Id
	private String id;

	private String name;
	private String nickname;
	private String introduction;
	private double price;
	private double unit;
	private int type;
	private String creater;
	private List<FunctionPackage> functions = new ArrayList<>();
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifiedTime;
	
	public static String isValid(SubscribePackage pack, List<SysDataResource> dataResources) {
		if (pack == null)
			return "参数错误";
		
		if (dataResources == null) {
			if (pack.getName() == null || pack.getName().isEmpty()) {
				return "订阅包ID不能为空";
			}
			
			if (pack.getNickname() == null || pack.getNickname().isEmpty()) {
				pack.setNickname(pack.getName());
			}
			
			if (pack.getPrice() < 0) {
				return "价格不能为负数";
			}
			
			if (pack.getUnit() < 0) {
				return "订阅单位不能为负数";
			}
			
			if (Constant.isPackTypeValid(pack.getType())) {
				return "订阅包类型错误";
			}
			
			if (pack.getCreater() == null || pack.getCreater().isEmpty()) {
				return "创建者不能空";
			} 
			
			if (pack.getFunctions() == null || pack.getFunctions().size() <= 0) {
				return "订阅的功能不能为空";
			}
		} else {
			Map<String, Boolean> limits = dataResources.stream().collect(Collectors.toMap(SysDataResource::getDataCode, (p)->(p.isEditable())));
			if (limits.get("name") && pack.getName() != null && pack.getName().isEmpty()) {
				return "订阅包ID不能为空";
			}
			
			if (limits.get("nickname") && pack.getNickname() != null && pack.getNickname().isEmpty()) {
				pack.setNickname(pack.getName());
			}
			
			if (limits.get("price") && pack.getPrice() < 0) {
				return "价格不能为负数";
			}
			
			if (limits.get("unit") && pack.getUnit() < 0) {
				return "订阅单位不能为负数";
			}
			
			if (limits.get("type") && Constant.isPackTypeValid(pack.getType())) {
				return "订阅包类型错误";
			}
			
			if (limits.get("creater") && pack.getCreater() != null && pack.getCreater().isEmpty()) {
				return "创建者不能空";
			} 
			
			if (limits.get("functions") && pack.getFunctions() != null && pack.getFunctions().size() <= 0) {
				return "订阅的功能不能为空";
			}
		}
		
		return null;
	}
	
	public SubscribePackage(String id, String name, String nickname, String introduction, double price, double unit,
			int type, String creater, List<FunctionPackage> functions) {
		super();
		this.id = id;
		this.name = name;
		this.nickname = nickname;
		this.introduction = introduction;
		this.price = price;
		this.unit = unit;
		this.type = type;
		this.creater = creater;
		this.functions = functions;
	}

	public SubscribePackage() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getUnit() {
		return unit;
	}

	public void setUnit(double unit) {
		this.unit = unit;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public List<FunctionPackage> getFunctions() {
		return functions;
	}

	public void setFunctions(List<FunctionPackage> functions) {
		this.functions = functions;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	@Override
	public String toString() {
		return "SubscribePackage [id=" + id + ", name=" + name + ", nickname=" + nickname + ", introduction="
				+ introduction + ", price=" + price + ", unit=" + unit + ", type=" + type + ", creater=" + creater
				+ ", functions=" + functions + ", createTime=" + createTime + ", lastModifiedTime=" + lastModifiedTime
				+ "]";
	}
}
