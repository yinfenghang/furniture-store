package com.cloudcool.study.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SmsRegisterCodeRecord {
	@Id
	private String id;
	private String phone;
	private String registerCode;
	@LastModifiedDate
	private Date lastModifiedTime;
	@CreatedDate
	private Date createdTime;
	
	public SmsRegisterCodeRecord() {}
	public SmsRegisterCodeRecord(String phone, String code) {
		this.phone = phone;
		this.registerCode = code;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRegisterCode() {
		return registerCode;
	}
	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}
	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	@Override
	public String toString() {
		return "SmsRegisterCodeRecord [id=" + id + ", phone=" + phone + ", registerCode=" + registerCode
				+ ", createdTime=" + createdTime + "]";
	}
}
