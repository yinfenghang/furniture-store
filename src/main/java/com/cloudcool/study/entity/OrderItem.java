package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.List;

import com.cloudcool.study.util.Note;

public class OrderItem implements Serializable {
	private String identifier;
	private Product product;
	private double price;
	private int number;
	private String remark;
	
	public OrderItem() {}
	public OrderItem(String identifier, Product product, double price, String remark, int number) {
		super();
		this.identifier = identifier;
		this.product = product;
		this.price = price;
		this.remark = remark;
		this.number = number;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public static boolean isValid(OrderItem item) {
		if (item == null ||
				item.getIdentifier() == null ||
				item.getProduct() == null ||
				item.getNumber() < 0) {
			return false;
		}
		
		return true;
	}
	@Override
	public String toString() {
		return "OrderItem [identifier=" + identifier + ", product=" + product + ", price=" + price + ", number=" + number
				+ ", remark=" + remark + "]";
	}
}