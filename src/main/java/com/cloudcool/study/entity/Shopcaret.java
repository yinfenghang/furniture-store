package com.cloudcool.study.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document
public class Shopcaret {
	@Id
	private String id;
	
	@DBRef
	private Product product;
	private Integer number;
	private String creater;
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifiedTime;
	
	public Shopcaret() {}
	
	public Shopcaret(Product product, int number, String userId) {
		this.product = product;
		this.number = number;
		this.creater = userId;
	}

	public Shopcaret(String id, Product product, int number, String creater) {
		super();
		this.id = id;
		this.product = product;
		this.number = number;
		this.creater = creater;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String createrId) {
		this.creater = createrId;
	}
	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	@Override
	public String toString() {
		return "Shopcaret [id=" + id + ", product=" + product + ", number=" + number + ", creater=" + creater
				+ ", createTime=" + createTime + ", lastModifiedTime=" + lastModifiedTime + "]";
	}	
}
