package com.cloudcool.study.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Note;

@Document("product")
public class ProductSingle extends Product {

	private String category;
	private String brand;
	private String material;
	private String color;
	private Size size;
	
	public ProductSingle() {}

	public ProductSingle(String category, String brand, String material, String color, Size size) {
		super();
		this.category = category;
		this.brand = brand;
		this.material = material;
		this.color = color;
		this.size = size;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Size getSize() {
		return size;
	}
	public void setSize(Size size) {
		this.size = size;
	}
	
	public static String isValid(ProductSingle single) {
		if (single == null) {
			return "未指定有效商品";
		}
		
		String check = Product.isProductValid(single);
		if (check != null)
			return check;
		
		if (single.getCategory() == null || single.getCategory().isEmpty()) {
			return "未指定有效商品品类";
		}
		
		if (single.getBrand() == null || single.getBrand().isEmpty()) {
			return "未指定有效商品品牌";
		}
		
		if (single.getMaterial() == null || single.getMaterial().isEmpty()) {
			return "未指定商品材质";
		}
		
		if (single.getColor() == null || single.getColor().isEmpty()) {
			return "未指定商品颜色";
		}
		
		return null;
	}

	@Override
	public String toString() {
		return "ProductSingle [category=" + category + ", brand=" + brand + ", material=" + material + ", color="
				+ color + ", size=" + size + ", isSingle()=" + isSingle() + ", getId()=" + getId() + ", getType()="
				+ getType() + ", getIdentifier()=" + getIdentifier() + ", getName()=" + getName() + ", getPicture()="
				+ getPicture() + ", getIntroduction()=" + getIntroduction() + ", getPrice()=" + getPrice()
				+ ", getMinPrice()=" + getMinPrice() + ", getInventory()=" + getInventory() + ", getRemark()="
				+ getRemark() + ", isValid()=" + isValid() + ", getTenantId()=" + getTenantId() + ", getCreateTime()="
				+ getCreateTime() + ", getCreater()=" + getCreater() + ", getLastModifiedTime()="
				+ getLastModifiedTime() + ", getVersion()=" + getVersion() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
