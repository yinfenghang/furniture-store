package com.cloudcool.study.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.Note;

@Document
public class FunctionPackage {
	@Id
	private String id;

	private String name;
	private String nickName;
	private String introduction;
	private Double price;
	private Double unit;
	private Integer type;
	private Boolean subscribeable;
	private List<Privilege> privileges;
	
	@CreatedDate
	private Date createdTime;
	
	public static String isValid(FunctionPackage pack, List<SysDataResource> dataResources) {
		if (pack == null)
			return "参数为空";
		
		if (dataResources == null) {
			if (pack.getName() == null || pack.getName().isEmpty())
				return "功能包名称为空";
			
			if (pack.getNickName() == null || pack.getNickName().isEmpty())
				pack.setNickName(pack.getName());
			
			if (Constant.isPackTypeValid(pack.getType())) {
				return "功能包类型错误";
			}
			
			if (pack.getPrice() < 0) {
				return "价格不能<0";
			}
			
			if (pack.getUnit() < 0) {
				return "单位不能<0";
			}
			
			if (pack.getPrivileges() == null || pack.getPrivileges().size() <= 0) {
				return "功能包中至少包含一个权限";
			}
		} else {
			Map<String, Boolean> limits = dataResources.stream().collect(Collectors.toMap(SysDataResource::getDataCode, (p)->(p.isEditable())));
			if (limits.get("name") && pack.getName() != null && pack.getName().isEmpty())
				return "功能包名称为空";
			
			if (limits.get("nickName") && pack.getNickName() != null && pack.getNickName().isEmpty())
				pack.setNickName(pack.getName());
			
			if (limits.get("type") && Constant.isPackTypeValid(pack.getType())) {
				return "功能包类型错误";
			}
			
			if (limits.get("price") && pack.getPrice() < 0) {
				return "价格不能<0";
			}
			
			if (limits.get("unit") && pack.getUnit() < 0) {
				return "单位不能<0";
			}
			
			if (limits.get("privileges") && pack.getPrivileges() != null && pack.getPrivileges().size() <= 0) {
				return "功能包中至少包含一个权限";
			}
		}
		
		return null;
	}

	public FunctionPackage() {}
	public FunctionPackage(String id, String name, String nickName, String introduction, double price, double unit,
			int type, List<Privilege> privileges) {
		super();
		this.id = id;
		this.name = name;
		this.nickName = nickName;
		this.introduction = introduction;
		this.price = price;
		this.unit = unit;
		this.type = type;
		this.privileges = privileges;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Privilege> getPrivileges() {
		return privileges;
	}
	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}	

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getUnit() {
		return unit;
	}

	public void setUnit(Double unit) {
		this.unit = unit;
	}

	public Boolean getSubscribeable() {
		return subscribeable;
	}
	
	public boolean isSubscribeable() {
		return subscribeable;
	}

	public void setSubscribeable(Boolean subscribeable) {
		this.subscribeable = subscribeable;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String makePrice() {
		if (getType() == Constant.PACK_DATE) {
			return getPrice() + "￥/" + getUnit() + "天";
		} else {
			return getPrice() + "￥/" + getUnit() + '条';
		}
	}
	
	public boolean isDateType() {
		return getType() == Constant.PACK_DATE;
	}
	
	public String getUnitStr() {
		if (getType() == Constant.PACK_DATE) {
			return "" + getUnit() + "天";
		} else {
			return "" + getUnit() + '条';
		}
	}
	
	@Override
	public String toString() {
		return "FunctionPackage [id=" + id + ", name=" + name + ", nickName=" + nickName + ", introduction="
				+ introduction + ", price=" + price + ", unit=" + unit + ", type=" + type + ", subscribeable="
				+ subscribeable + ", privileges=" + privileges + ", createdTime=" + createdTime + "]";
	}
}
