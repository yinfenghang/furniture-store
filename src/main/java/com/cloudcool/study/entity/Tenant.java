package com.cloudcool.study.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cloudcool.study.util.Constant;
import com.cloudcool.study.util.EmailValidator;
import com.cloudcool.study.util.Note;
import com.cloudcool.study.util.PhoneValidator;

@Document
public class Tenant implements Serializable {

	@Id
	public String id;
	
	private String name;
	private String address;
	private String phone;
	private String scope;
	private String license;
	private Double amount;
	private Boolean valid;
	
	private Contactor contactor;
	private List<DatePack> featurePacks = new ArrayList<>();
	private List<NumPack> dataPacks = new ArrayList<>();
	private CashoutAccount cashoutAccount;
	
	@CreatedDate
	private Date createTime;
	@LastModifiedDate
	private Date lastModifiedTime;
	@Version
	private Integer version;
	
	public static String isValid(Tenant tenant, List<SysDataResource> dataResources) {
		if (tenant == null) {
			return "参数错误";
		}
		if (dataResources == null) {
			if (tenant.getName() == null || tenant.getName().isEmpty()) {
				return "公司名称为空";
			}
			
			if (tenant.getAddress() == null || tenant.getAddress().isEmpty()) {
				return "公司地址为空";
			}
			
			if (!PhoneValidator.isPhone(tenant.getPhone())) {
				return "公司电话格式错误";
			}
			
			if (tenant.getScope() == null || tenant.getScope().isEmpty()) {
				return "公司经营范围为空";
			}
	
			if (tenant.getAmount() == null || tenant.getAmount() < 0) {
				return "公司余额错误: " + tenant.getAmount();
			}
			
			if (tenant.getContactor() == null) {
				return "公司联系人不能为空";
			}
			String contactorCheck = Contactor.isValid(tenant.getContactor());
			if (contactorCheck != null) {
				return contactorCheck;
			}
		} else {
			Map<String, Boolean> limits = dataResources.stream().collect(Collectors.toMap(SysDataResource::getDataCode, (p)->(p.isEditable())));
			if (limits.get("name") && tenant.getName() != null && tenant.getName().isEmpty()) {
				return "公司名称为空";
			}
			
			if (limits.get("address") && tenant.getAddress() != null && tenant.getAddress().isEmpty()) {
				return "公司地址为空";
			}
			
			if (limits.get("phone") && !PhoneValidator.isPhone(tenant.getPhone())) {
				return "公司电话格式错误";
			}
			
			if (limits.get("scope") && tenant.getScope() != null && tenant.getScope().isEmpty()) {
				return "公司经营范围为空";
			}
	
			if (limits.get("amount") && tenant.getAmount() != null && tenant.getAmount() < 0) {
				return "公司余额错误: " + tenant.getAmount();
			}
			
			if (limits.get("contactor") && tenant.getContactor() != null) {
				String contactorCheck = Contactor.isValid(tenant.getContactor());
				if (contactorCheck != null) {
					return contactorCheck;
				}
				return "公司联系人不能为空";
			}
		}
		
		return null;
	}
	
	public Tenant() {}

	public Tenant(String id, String name, String address, String phone, String scope, String license, double amount,
			boolean valid, Contactor contactor, List<DatePack> featurePacks, List<NumPack> dataPacks,
			CashoutAccount cashoutAccount) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.scope = scope;
		this.license = license;
		this.amount = amount;
		this.valid = valid;
		this.contactor = contactor;
		this.featurePacks = featurePacks;
		this.dataPacks = dataPacks;
		this.cashoutAccount = cashoutAccount;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public Contactor getContactor() {
		return contactor;
	}

	public void setContactor(Contactor contactor) {
		this.contactor = contactor;
	}
	
	public List<DatePack> getFeaturePacks() {
		return featurePacks;
	}

	public void setFeaturePacks(List<DatePack> featurePacks) {
		this.featurePacks = featurePacks;
	}

	public List<NumPack> getDataPacks() {
		return dataPacks;
	}

	public void setDataPacks(List<NumPack> dataPacks) {
		this.dataPacks = dataPacks;
	}
	
	public String getScope() {
		return scope;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public CashoutAccount getCashoutAccount() {
		return cashoutAccount;
	}

	public void setCashoutAccount(CashoutAccount cashoutAccount) {
		this.cashoutAccount = cashoutAccount;
	}

	public Boolean isValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return "Tenant [id=" + id + ", name=" + name + ", address=" + address + ", phone=" + phone + ", scope=" + scope
				+ ", license=" + license + ", amount=" + amount + ", valid=" + valid + ", contactor=" + contactor
				+ ", featurePacks=" + featurePacks + ", dataPacks=" + dataPacks + ", cashoutAccount=" + cashoutAccount
				+ ", createTime=" + createTime + ", lastModifiedTime=" + lastModifiedTime + ", version=" + version
				+ "]";
	}

	public static class Contactor {
		@Note("姓名")
		private String name;
		@Note("电话")
		private String phone;
		@Note("邮箱")
		private String email;
		@Note("性别")
		private int sex;
		
		public static String isValid(Contactor contactor) {
			if (contactor == null) {
				return "参数错误";
			}
			
			if (contactor.getName() == null || contactor.getName().isEmpty()) {
				return "联系人姓名不能为空";
			}
			
			if (!PhoneValidator.isPhone(contactor.getPhone())) {
				return "联系人电话不能为空";
			}
			
			if (!EmailValidator.isEmail(contactor.getEmail())) {
				return "联系人邮箱为空";
			}
			
			return null;
		}
		
		public Contactor() {}

		public Contactor(String name, String phone, String email, int sex) {
			super();
			this.name = name;
			this.phone = phone;
			this.email = email;
			this.sex = sex;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public int getSex() {
			return sex;
		}
		public void setSex(int sex) {
			this.sex = sex;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}
		
		@Override
		public String toString() {
			return "Contactor [name=" + name + ", phone=" + phone + ", email=" + email + ", sex=" + sex + "]";
		}
	}
	
	public static class DatePack {
		@Note("代号")
		private String name;
		@Note("名称")
		private String nickName;
		@Note("起始时间")
		private Date startTime;
		@Note("过期时间")
		private Date endTime;
		
		public static String isValid(DatePack pack) {
			if (pack == null)
				return "参数错误";
			
			if (pack.getName() == null || pack.getName().isEmpty()) {
				return "功能包代码不能为空";
			}
			
			if (pack.getNickName() == null || pack.getNickName().isEmpty()) {
				return "功能包名称不能为空";
			}
			
			if (pack.getStartTime() == null) {
				return "功能包起始时间不能为空";
			}
			
			if (pack.getEndTime() == null) {
				return "功能包失效时间不能为空";
			}
			
			if (pack.getStartTime().after(pack.getEndTime())) {
				return "功能包起始时间大于失效时间";
			}
			
			return null;
		}
		
		public DatePack() {}
		
		public DatePack(String name, String nickName, Date startTime, Date endTime) {
			super();
			this.name = name;
			this.nickName = nickName;
			this.startTime = startTime;
			this.endTime = endTime;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Date getStartTime() {
			return startTime;
		}
		public void setStartTime(Date startTime) {
			this.startTime = startTime;
		}
		public Date getEndTime() {
			return endTime;
		}
		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}
		
		public String getNickName() {
			return nickName;
		}

		public void setNickName(String nickName) {
			this.nickName = nickName;
		}
		
		@Override
		public String toString() {
			return "DatePack [name=" + name + ", nickName=" + nickName + ", startTime=" + startTime + ", endTime="
					+ endTime + "]";
		}
	}
	
	public static class NumPack {
		@Note("代号")
		private String name;
		@Note("名称")
		private String nickName;
		@Note("余量")
		private int remainder = 0;
		
		public static String isValid(NumPack pack) {
			if (pack == null)
				return "参数错误";
			
			if (pack.getName() == null || pack.getName().isEmpty()) {
				return "功能包代码不能为空";
			}
			
			if (pack.getNickName() == null || pack.getNickName().isEmpty()) {
				return "功能包名称不能为空";
			}
			
			if (pack.getRemainder() < 0) {
				return "功能包剩余量不能<0";
			}
			
			return null;
		}
		
		public NumPack() {}
		
		public NumPack(String name, String nickName, int remainder) {
			super();
			this.name = name;
			this.nickName = nickName;
			this.remainder = remainder;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getRemainder() {
			return remainder;
		}
		public void setRemainder(int remainder) {
			this.remainder = remainder;
		}

		public String getNickName() {
			return nickName;
		}

		public void setNickName(String nickName) {
			this.nickName = nickName;
		}

		@Override
		public String toString() {
			return "NumPack [name=" + name + ", nickName=" + nickName + ", remainder=" + remainder + "]";
		}
	}
	
	public static class CashoutAccount {
		@Note("收款人姓名")
		private String name;
		@Note("银行卡号")
		private String cardNo;
		@Note("开户行")
		private String bankName;
		
		public static String isValid(CashoutAccount account) {
			if (account == null)
				return "参数错误";
			
			if (account.getName() == null || account.getName().isEmpty())
				return "收款人为空";
			
			if (account.getCardNo() == null || account.getCardNo().isEmpty()) {
				return "银行卡号为空";
			}
			
			if (account.getBankName() == null || account.getBankName().isEmpty())
				return "开户行为空";
			
			return null;
		}
		public CashoutAccount(String name, String cardNo, String bankName) {
			super();
			this.name = name;
			this.cardNo = cardNo;
			this.bankName = bankName;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCardNo() {
			return cardNo;
		}
		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		@Override
		public String toString() {
			return "CashoutAccount [name=" + name + ", cardNo=" + cardNo + ", bankName=" + bankName + "]";
		}
	}
}
