package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Notify;
import com.mongodb.client.result.DeleteResult;
@Component
public class NotifyDao extends CRMBaseDao<Notify> {

	public Notify create(Notify notify) {
		notify = mongoTemplate.insert(updateFilteredItem(notify));
		return getFilteredItem(notify);
	}
	
	public List<Notify> findByReceiver(String user) {
		List<Notify> notifies = mongoTemplate.find(new Query(Criteria.where("receiver").is(user)), Notify.class);
		return getFilteredList(notifies);
	}
	
	public List<Notify> findBySender(String user) {
		List<Notify> notifies = mongoTemplate.find(new Query(Criteria.where("sender").is(user)), Notify.class);
		return getFilteredList(notifies);
	}

	public Notify findById(String id) {
		Notify notify = mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)), Notify.class);
		return getFilteredItem(notify);
	}

	public boolean delete(Notify notify) {
		if (notify == null || notify.getId() == null || notify.getId().isEmpty())
			return false;
		
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("_id").is(notify.getId())), Notify.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}
	
	public Notify update(Notify notify) {
		if (notify == null || notify.getId() == null || notify.getId().isEmpty())
			return notify;
		
		Query query = new Query(Criteria.where("_id").is(notify.getId()));
		Update update = createUpdate(notify, null);
		notify = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Notify.class);
		return getFilteredItem(notify);
	}
}
