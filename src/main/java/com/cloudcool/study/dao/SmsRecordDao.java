package com.cloudcool.study.dao;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.SmsRecord;

@Component
public class SmsRecordDao extends CRMBaseDao<SmsRecord> {

	public SmsRecord create(SmsRecord record) {
		return mongoTemplate.insert(record);
	}
	
	public SmsRecord updateById(String tenantId, SmsRecord record) {
		Query query = new Query(Criteria.where("_id").is(record.getId()).and("tenantId").is(tenantId));
		Update update = createUpdate(record);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), SmsRecord.class);
	}

	public SmsRecord findByOutId(String id) {
		Query query = new Query(Criteria.where("outId").is(id));
		return mongoTemplate.findOne(query, SmsRecord.class);
	}
}
