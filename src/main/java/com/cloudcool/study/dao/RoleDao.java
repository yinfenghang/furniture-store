package com.cloudcool.study.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cloudcool.study.entity.Role;
import com.mongodb.client.result.DeleteResult;
@Component
public class RoleDao extends CRMBaseDao<Role> {

	public Role create(Role role) {
		role = mongoTemplate.insert(updateFilteredItem(role));
		return getFilteredItem(role);
	}

	public Role findByName(String name) {
		Role role = mongoTemplate.findOne(new Query(Criteria.where("name").is(name)), Role.class);
		return getFilteredItem(role);
	}

	public Role findById(String id) {
		Role role = mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)), Role.class);
		return getFilteredItem(role);
	}

	public boolean delete(Role role) {
		if (role == null || role.getId() == null || role.getId().isEmpty())
			return false;
		
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("_id").is(role.getId())), Role.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}
	
	public Role update(Role role) {
		if (role == null || role.getId() == null || role.getId().isEmpty())
			return role;
		
		Query query = new Query(Criteria.where("_id").is(role.getId()));
		Update update = createUpdate(role, null);
		role = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Role.class);
		return getFilteredItem(role);
	}

	public List<Role> findAll() {
		return mongoTemplate.findAll(Role.class);
	}
}
