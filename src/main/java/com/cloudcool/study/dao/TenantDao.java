package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.Tenant;
import com.cloudcool.study.entity.Tenant.CashoutAccount;
import com.mongodb.client.result.DeleteResult;
@Component
public class TenantDao extends CRMBaseDao<Tenant> {
	
	public Tenant create(Tenant tenant) {
		tenant.setValid(true);
		return mongoTemplate.insert(updateFilteredItem(tenant));
	}
	
	public boolean delete(String tenantId) {
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("_id").is(tenantId)), Tenant.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}
	
	public boolean invalid(String tenantId) {
		Query query = new Query(Criteria.where("_id").is(tenantId));
		Update update = new Update();
		update.set("valid", false);
		Tenant tenant = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Tenant.class);
		return !tenant.isValid();
	}

	public List<Tenant> findByName(String name) {
		return mongoTemplate.find(new Query(Criteria.where("name").is(name).and("valid").ne(false)), Tenant.class);
	}

	public Tenant findById(String tenantId) {
		return mongoTemplate.findOne(new Query(Criteria.where("_id").is(tenantId).and("valid").ne(false)), Tenant.class);
	}

	public Tenant update(Tenant tenant, List<SysDataResource> limits)  throws UpdateParamInvalidException, UpdateOptimisticLockException {
		if (tenant.getVersion() == null) {
			throw new UpdateParamInvalidException("租户信息版本号不能为空");
		}
		Query query = new Query(Criteria.where("_id").is(tenant.getId()).and("version").is(tenant.getVersion()));
		Update update = createUpdate(tenant, limits);
		int version = tenant.getVersion();
		tenant = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true),  Tenant.class);
		if (tenant == null || version == tenant.getVersion()) {
			throw new UpdateOptimisticLockException("租户信息被其他人修改，请刷新页面后重试");
		}
		
		return tenant;
	}
	
	public Tenant addAccount(String tenantId, double account) {
		Query query = new Query(Criteria.where("_id").is(tenantId));
		Update update = new Update();
		update.inc("amount", account);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Tenant.class);
	}
	
	public Tenant descAccount(String tenantId, double account) {
		Query query = new Query(Criteria.where("_id").is(tenantId));
		Update update = new Update();
		update.inc("amount", account * -1);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Tenant.class);
	}
}
