package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SubscribePackage;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.service.ServiceResult;
import com.mongodb.client.result.DeleteResult;

@Component
public class SubscribePackageDao extends CRMBaseDao<SubscribePackage> {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public SubscribePackage create(SubscribePackage pack) {
		return mongoTemplate.insert(pack);
	}
	
	public SubscribePackage update(SubscribePackage pack, List<SysDataResource> limits) {
		Query query = null;
		if (pack.getId() != null) {
			query =  new Query(Criteria.where("_id").is(pack.getId()));
		} else if (pack.getName() != null) {
			query = new Query(Criteria.where("name").is(pack.getName()));
		} else {
			return null;
		}
		Update update = createUpdate(pack, limits);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), SubscribePackage.class);
	}
	
	public boolean delete(SubscribePackage pack) {
		Query query = null;
		if (pack.getId() != null) {
			query =  new Query(Criteria.where("_id").is(pack.getId()));
		} else if (pack.getName() != null) {
			query = new Query(Criteria.where("name").is(pack.getName()));
		} else {
			return false;
		}
		
		DeleteResult result = mongoTemplate.remove(query, SubscribePackage.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}
	
	public SubscribePackage findById(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		return mongoTemplate.findOne(query, SubscribePackage.class);
	}

	public List<SubscribePackage> findAll() {
		return mongoTemplate.findAll(SubscribePackage.class);
	}

}
