package com.cloudcool.study.dao;

public class UpdateOptimisticLockException extends Exception {
	private String msg;

	public UpdateOptimisticLockException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "UpdateOptimisticLockException [msg=" + msg + "]";
	}
}
