package com.cloudcool.study.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cloudcool.study.entity.Shopcaret;
import com.mongodb.client.result.DeleteResult;
@Component
public class ShopcaretDao extends CRMBaseDao<Shopcaret> {

	public List<Shopcaret> findByCreater(String user) {
		List<Shopcaret> shopcarets = mongoTemplate.find(new Query(Criteria.where("creater").is(user)), Shopcaret.class);
		return getFilteredList(shopcarets);
	}

	public Shopcaret create(Shopcaret caret) {
		caret = mongoTemplate.insert(updateFilteredItem(caret));
		return getFilteredItem(caret);
	}
	
	public Shopcaret update(Shopcaret caret) {
		if (caret == null || caret.getId() == null || caret.getId().isEmpty()) {
			return caret;
		}
		
		Query query = new Query(Criteria.where("_id").is(caret.getId()));
		Update update = createUpdate(caret, null);
		caret = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Shopcaret.class);
		return getFilteredItem(caret);
	}
	
	public boolean delete(Shopcaret caret) {
		if (caret == null || caret.getId() == null || caret.getId().isEmpty())
			return false;
		
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("_id").is(caret.getId())), Shopcaret.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}

	public long deleteByCreater(String user) {
		if (user == null || user.isEmpty())
			return 0;
		
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("creater").is(user)), Shopcaret.class);
		return result.getDeletedCount();
	}
}
