package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Task;
import com.mongodb.client.result.DeleteResult;
@Component
public class TaskDao extends CRMBaseDao<Task> {
	
	public Task create(Task task) {
		task = updateFilteredItem(task);
		task = mongoTemplate.insert(task);
		return getFilteredItem(task);
	}
	
	public List<Task> findByOwner(String owner) {
		List<Task> tasks = mongoTemplate.find(new Query(Criteria.where("owner").is(owner)), Task.class);
		return getFilteredList(tasks);
	}
	
	public List<Task> findByCreater(String creater) {
		List<Task> tasks = mongoTemplate.find(new Query(Criteria.where("creater").is(creater)), Task.class);
		return getFilteredList(tasks);
	}

	public List<Task> findByOwnerAndStatus(String user, int taskStatus) {
		List<Task> tasks = mongoTemplate.find(new Query(Criteria.where("user").is(user).and("status").is(taskStatus)), Task.class);
		return getFilteredList(tasks);
	}

	public Task findById(String id) {
		Task task = mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)), Task.class);
		return getFilteredItem(task);
	}
	
	public boolean delete(Task task) {
		if (task == null || task.getId() == null || task.getId().isEmpty())
			return false;
		
		Query query = new Query(Criteria.where("_id").is(task.getId()));
		DeleteResult result = mongoTemplate.remove(query, Task.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}
	
	public Task update(Task task) {
		if (task == null || task.getId() == null || task.getId().isEmpty())
			return task;
		
		Query query = new Query(Criteria.where("_id").is(task.getId()));
		Update update = createUpdate(task, null);
		task = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Task.class);
		return getFilteredItem(task);
	}
}
