package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Order;
import com.cloudcool.study.entity.SysDataResource;

@Component
public class OrderDao extends CRMBaseDao<Order> {

	public Order create(Order order) {
		return mongoTemplate.insert(order);
	}

	public List<Order> findByTenantIdAndStatePageable(String tenantId, int state, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").is(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}

	public int countOrdersByTenandIdAndState(String tenantId, int state) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").is(state));
		List<Order> orders = mongoTemplate.find(query, Order.class);
		if (orders == null) {
			return 0;
		}
		
		return orders.size();
	}

	public Order findByTenantIdAndStateAndOrderNo(String tenantId, int state, String orderNo) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").is(state).and("orderNo").is(orderNo));
		return mongoTemplate.findOne(query, Order.class);
	}

	public Order findByTenantIdAndOrderNo(String tenantId, String orderNo) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("orderNo").is(orderNo));
		return mongoTemplate.findOne(query, Order.class);
	}

	public List<Order> findByTenandIdAndGuiderAndStatePageable(String tenantId, String username, int state, int start,
			int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("guider.participantId").is(username).and("state").is(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}
	
	public int countByTenandIdAndGuiderAndStatePageable(String tenantId, String username, int state) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("guider.participantId").is(username).and("state").is(state));
		List<Order> orders =  mongoTemplate.find(query, Order.class);
		if (orders == null) {
			return 0;
		}
		
		return orders.size();
	}
	
	public List<Order> findByTenandIdAndGuiderAndExStatePageable(String tenantId, String username, int state, int start,
			int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("guider.participantId").is(username).and("state").ne(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}
	
	public int countByTenandIdAndGuiderAndExStatePageable(String tenantId, String username, int state) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("guider.participantId").is(username).and("state").ne(state));
		List<Order> orders = mongoTemplate.find(query, Order.class);
		if (orders == null) {
			return 0;
		}
		
		return orders.size();
	}

	public Order findByTenantIdAndGuiderAndState(String tenantId, String username, int state) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("guider.participantId").is(username).and("state").is(state));
		return mongoTemplate.findOne(query, Order.class);
	}

	public List<Order> findByTenantIdAndCurrentAndStatePageable(String username, String tenantId, int state, int start,
			int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("currentHandler").is(username).and("state").is(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}
	
	public List<Order> findByTenantIdAndDeliveryAndStatePageable(String tenantId, String username, int state, int start,
			int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("deliveryer.participantId").is(username).and("state").is(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}
	
	public List<Order> findByTenantIdAndInstallerAndStatePageable(String tenantId, String username, int state, int start,
			int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("installer.participantId").is(username).and("state").is(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}

	public List<Order> findByTenandIdAndStatePageable(String tenantId, int state, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").is(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}
	
	public int countByTenandIdAndStatePageable(String tenantId, int state) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").is(state));
		List<Order> orders = mongoTemplate.find(query, Order.class);
		if (orders == null) {
			return 0;
		}
		
		return orders.size();
	}
	
	public List<Order> findByTenandIdAndExStatePageable(String tenantId, int state, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").ne(state)).skip(start).limit(size);
		return mongoTemplate.find(query, Order.class);
	}
	
	public int countByTenandIdAndExStatePageable(String tenantId, int state) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").ne(state));
		List<Order> orders =  mongoTemplate.find(query, Order.class);
		if (orders == null) {
			return 0;
		}
		
		return orders.size();
	}
	
	public int countByTenandIdAndExStatePageable(String tenantId, int state, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("state").ne(state)).skip(start).limit(size);
		List<Order> orders =  mongoTemplate.find(query, Order.class);
		if (orders == null) {
			return 0;
		}
		return orders.size();
	}

	public Order findByTenantIdAndGuiderAndOrderNo(String tenantId, String username, String orderNo) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("guider.participantId").is(username).and("orderNo").is(orderNo));
		return mongoTemplate.findOne(query, Order.class);
	}
	
	public Order update(String tenantId, Order order, int oldState, List<SysDataResource> limit) throws UpdateParamInvalidException, UpdateOptimisticLockException {
		if (order.getVersion() == null) {
			throw new UpdateParamInvalidException("用户版本号不能为空");
		}
		
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("orderNo").is(order.getOrderNo()).and("version").is(order.getVersion()).and("state").is(oldState));
		int version = order.getVersion();
		order.setVersion(null);
		Update update = createUpdate(order, limit);
		order = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Order.class);
		if (order == null || version == order.getVersion()) {
			throw new UpdateOptimisticLockException("订单信息被其他用户修改，请刷新重试");
		}
		
		return order;
	}
}
