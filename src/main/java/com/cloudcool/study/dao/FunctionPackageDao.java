package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.FunctionPackage;
import com.cloudcool.study.entity.SysDataResource;
import com.mongodb.client.result.DeleteResult;

@Component
public class FunctionPackageDao extends CRMBaseDao<FunctionPackage> {

	public FunctionPackage create(FunctionPackage pack) {
		return mongoTemplate.insert(updateFilteredItem(pack));
	}
	
	public FunctionPackage update(FunctionPackage pack, List<SysDataResource> limits) {
		Query query = null;
		if (pack.getId() != null) {
			query =  new Query(Criteria.where("_id").is(pack.getId()));
		} else if (pack.getName() != null) {
			query = new Query(Criteria.where("name").is(pack.getName()));
		} else {
			return null;
		}
		Update update = createUpdate(pack, limits);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), FunctionPackage.class);
	}
	
	public boolean delete(FunctionPackage pack) {
		Query query = null;
		if (pack.getId() != null) {
			query =  new Query(Criteria.where("_id").is(pack.getId()));
		} else if (pack.getName() != null) {
			query = new Query(Criteria.where("name").is(pack.getName()));
		} else {
			return false;
		}
		
		DeleteResult result = mongoTemplate.remove(query, FunctionPackage.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}

	public List<FunctionPackage> findAll() {
		return mongoTemplate.findAll(FunctionPackage.class);
	}

	public FunctionPackage findByNameAndType(String name, int type) {
		Query query = new Query(Criteria.where("name").is(name).and("type").is(type));
		return mongoTemplate.findOne(query, FunctionPackage.class);
	}
}
