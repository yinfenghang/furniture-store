package com.cloudcool.study.dao;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.Class;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Update;

import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

public class CRMBaseDao<T> {
	@Autowired
	protected MongoTemplate mongoTemplate;
	
	/* read */
	public List<T> getFilteredList(List<T> items) {
//		if (items == null || items.size() <= 0)
//			return items;
//		
//		List<PropertyDescriptor> invalidProperties = getReadInvalidProperties(items.get(0).getClass());
//		for (T item : items) {
//			for (PropertyDescriptor propertyDescriptor : invalidProperties) {
//				try {
//					propertyDescriptor.getWriteMethod().invoke(item, new Object[] {null});
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				}
//			}
//		}
		return items;
	}
	
	public T getFilteredItem(T item) {
//		if (item == null)
//			return item;
//		
//		List<PropertyDescriptor> invalidProperties = getReadInvalidProperties(item.getClass());
//		for (PropertyDescriptor propertyDescriptor : invalidProperties) {
//			try {
//				propertyDescriptor.getWriteMethod().invoke(item, new Object[] {null});
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//		}
		return item;
	}
	
	/* update */
	public List<T> updateFilteredList(List<T> items) {
//		if (items == null || items.size() <= 0)
//			return items;
//		
//		List<PropertyDescriptor> invalidProperties = getWriteInvalidProperties(items.get(0).getClass());
//		for (T item : items) {
//			for (PropertyDescriptor propertyDescriptor : invalidProperties) {
//				try {
//					propertyDescriptor.getWriteMethod().invoke(item, new Object[] {null});
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				}
//			}
//		}
		return items;
	}
	public T updateFilteredItem(T item) {
//		if (item == null)
//			return item;
//
//		List<PropertyDescriptor> invalidProperties = getWriteInvalidProperties(item.getClass());
//		for (PropertyDescriptor propertyDescriptor : invalidProperties) {
//			try {
//				propertyDescriptor.getWriteMethod().invoke(item, new Object[] {null});
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//		}
		return item;
	}
	
	public Update createUpdate(T item, List<SysDataResource> dataResources) {
		if (dataResources == null) {
			return createUpdate(item);
		}
		
		/* get limits for update */
		Map<String, Boolean> limits = new HashMap<>();
		for (SysDataResource dataResource : dataResources) {
			limits.put(dataResource.getDataCode(), dataResource.isUpdateable());
		}
		
		/* get property limits */
		Update update = new Update();
		PropertyDescriptor[] propertyDescriptor = BeanUtils.getPropertyDescriptors(item.getClass());
		for (PropertyDescriptor property : propertyDescriptor) {
			String fieldName = property.getName();
			if (limits.get(fieldName) == Boolean.TRUE) {
				try {
					Method method = property.getReadMethod();
					if (method != null) {
						Object value = method.invoke(item);
						if (value != null)
							update.set(fieldName, value);
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					System.out.println("Called read method of field " + fieldName + " failed");
					e.printStackTrace();
				}
			}
		}
		
		return update;
	}
	
	public Update createUpdate(T item) {
		/* get property limits */
		Update update = new Update();
		Class<?> clazz = item.getClass();
		PropertyDescriptor[] propertyDescriptor = BeanUtils.getPropertyDescriptors(clazz);
		Field[] fields = clazz.getDeclaredFields();
		Set<String> fieldNames = new HashSet<String>();
		for (Field field : fields) {
			fieldNames.add(field.getName());
		}
		for (PropertyDescriptor property : propertyDescriptor) {
			String fieldName = property.getName();
			try {
				if (!fieldNames.contains(fieldName)) {
					continue;
				}
				Method method = property.getReadMethod();
				if (method != null) {
					Object value = method.invoke(item);
					if (value != null)
						update.set(fieldName, value);
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) {
				System.out.println("Called read method of field " + fieldName + " failed");
				e.printStackTrace();
			}
		}
		
		return update;
	}
}
