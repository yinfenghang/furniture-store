package com.cloudcool.study.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Privilege;
import com.cloudcool.study.entity.Role;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;

@Component
public class UserLoginDao {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public User findUserByUsername(String username) {
		User user = mongoTemplate.findOne(new Query(Criteria.where("username").is(username)), User.class);
		List<String> roleIds = user.getRoles();
		Set<String> grantedAuthorities = new HashSet<String>();
		Map<String, List<SysDataResource>> columnAuthorities = new HashMap<String, List<SysDataResource>>();
		for (String roleId : roleIds) {
			Role role = mongoTemplate.findOne(new Query(Criteria.where("name").is(roleId)), Role.class);
			if (role == null) {
				continue;
			}
			List<Privilege> privileges = role.getPrivileges();
			grantedAuthorities.add(role.getName());
			for (Privilege privilege : privileges) {
				Map<String, String> operations = privilege.getOperations();
				for (Entry<String, String> entry : operations.entrySet()) {
					grantedAuthorities.add(String.format("%s-%s", privilege.getResourceId(), entry.getKey()));
				}
			}
			Map<String, List<SysDataResource>> columns = role.getColumnPrivileges();
			for (String key : columns.keySet()) {
				List<SysDataResource> dataResources = columnAuthorities.get(key);
				if (dataResources == null) {
					columnAuthorities.put(key, columns.get(key));
					continue;
				}
				
				for (SysDataResource tmp : columns.get(key)) {
					for (SysDataResource dataResource : dataResources) {
						if (dataResource.same(tmp)) {
							dataResource.merge(tmp);
							break;
						}
					}
				}
				columnAuthorities.put(key, dataResources);
			}
		}
		user.setGrantedAuthorities(new ArrayList<>(grantedAuthorities));
		user.setColumnAuthorities(columnAuthorities);
		return user;
	}
	
	public User findUser() {
		String username = getSelfUsername();
		if (username == null || username.isEmpty()) {
			return null;
		}
		return findUserByUsername(username);
	}
	
	public static String getSelfUsername() {
		SecurityContext cxt = SecurityContextHolder.getContext();
		if (cxt == null)
			return null;

		Object principal =  cxt.getAuthentication().getPrincipal();
		if (principal instanceof org.springframework.security.core.userdetails.User) {
			org.springframework.security.core.userdetails.User userInternal = (org.springframework.security.core.userdetails.User)principal;
			return userInternal.getUsername();
		}
		
		return null;
	}
	
	public static boolean isSelf(String username) {
		String self = getSelfUsername();
		if (self.equals(username)) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isUserLogin() {
		SecurityContext cxt = SecurityContextHolder.getContext();
		if (cxt == null)
			return false;
		
		Object principal =  cxt.getAuthentication().getPrincipal();
		if (principal != null)
			return true;
		
		return false;
	}

	public String findTenantId() {
		String username = getSelfUsername();
		Query query = new Query(Criteria.where("username").is(username));
		User user = mongoTemplate.findOne(query, User.class);
		if (user == null)
			return null;
		
		return user.getTenantId();
	}
}
