package com.cloudcool.study.dao;

import java.util.Arrays;

public class UpdateParamInvalidException extends Exception {
	private String msg;
	
	public UpdateParamInvalidException(String msg) {
		super(msg);
		this.msg = msg;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		return "UpdateParamInvalidException [msg=" + msg + "]";
	}
}
