package com.cloudcool.study.dao;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.mongodb.client.result.DeleteResult;

@Component
public class UserDao extends CRMBaseDao<User> {
	
	public String getPhoneByUsername(String username) {
		Query query = new Query(Criteria.where("username").is(username));
		User user = mongoTemplate.findOne(query, User.class);
		if (user == null) {
			return null;
		}
		
		return user.getPhone();
	}
	
	public int countByUsername(String username) {
		List<User> result = mongoTemplate.find(new Query(Criteria.where("username").is(username)), User.class);
		return result.size();
	}
	
	public User findByUsername(String username) {
		return mongoTemplate.findOne(new Query(Criteria.where("username").is(username).and("valid").ne(false)), User.class);
	}
	
	public User findById(String id) {
		return mongoTemplate.findOne(new Query(Criteria.where("_id").is(id).and("valid").ne(false)), User.class);
	}
	
	public User create(User user) {
		user.setValid(true);
		return mongoTemplate.insert(user);
	}
	
	public boolean delete(String username) {
		Query query = new Query(Criteria.where("username").is(username));
		DeleteResult result = mongoTemplate.remove(query, User.class);
		if (!result.wasAcknowledged()) {
			return false;
		}
		return result.getDeletedCount() > 0 ? true : false;
	}
	
	public boolean delete(User user) {
		Query query = new Query(Criteria.where("_id").is(user.getId()));
		DeleteResult result = mongoTemplate.remove(query, User.class);
		if (!result.wasAcknowledged()) {
			return false;
		}
		return result.getDeletedCount() > 0 ? true : false;
	}

	public boolean invalid(User user) {
		Query query = new Query(Criteria.where("_id").is(user.getId()));
		Update update = new Update();
		update.set("valid", false);
		user = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), User.class);
		return !user.isValid();
	}
	
	public boolean invalid(String username) {
		Query query = new Query(Criteria.where("username").is(username));
		Update update = new Update();
		update.set("valid", false);
		User user = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), User.class);
		return !user.isValid();
	}
	
	public User update(User user, List<SysDataResource> limits) throws UpdateParamInvalidException, UpdateOptimisticLockException {
		if ((user.getId() == null || user.getId().isEmpty()) 
				&& (user.getUsername() == null || user.getUsername().isEmpty())) {
			throw new UpdateParamInvalidException("用户id和用户名不能同时为空");
		}
		
		if (user.getVersion() == null) {
			throw new UpdateParamInvalidException("用户版本号不能为空");
		}
		
		Query query = null;
		if (user.getUsername() != null && !user.getUsername().isEmpty()) {
			query = new Query(Criteria.where("username").is(user.getUsername()).and("version").is(user.getVersion()));
		} else {
			query = new Query(Criteria.where("_id").is(user.getId()).and("version").is(user.getVersion()));
		}
		Update update = createUpdate(user, limits);
		int version = user.getVersion();
		user = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), User.class);
		if (user == null || version == user.getVersion()) {
			throw new UpdateOptimisticLockException("用户信息被其他用户修改，请刷新重试");
		}
		return user;
	}
	
	public User findByUsernameAndTenantId(String username, String tenantId) {
		Query query = new Query(Criteria.where("username").is(username).and("tenantId").is(tenantId).and("valid").ne(false));
		return mongoTemplate.findOne(query, User.class);
	}
	
	public List<User> findAll(String tenantId) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").ne(false));
		return mongoTemplate.find(query, User.class);
	}

	public int count(String tenantId) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").ne(false));
		List<User> users = mongoTemplate.find(query, User.class);
		return users.size();
	}

	public List<User> findAll(String tenantId, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").ne(false)).skip(start).limit(size);
		return mongoTemplate.find(query, User.class);
	}
	
	public List<User> search(String tenantId, String content) {
		Pattern pattern = Pattern.compile("^.*" + content + ".*$", Pattern.CASE_INSENSITIVE);
		Criteria criteria = new Criteria();
		Criteria contentMatch = new Criteria();
		criteria.andOperator(Criteria.where("tenantId").is(tenantId).and("valid").ne(false), 
				contentMatch.orOperator(Criteria.where("name").regex(pattern), 
				Criteria.where("phone").regex(pattern)));
		return mongoTemplate.find(new Query(criteria), User.class);
	}

	public List<User> search(String tenantId, String role, String content) {
		Pattern pattern = Pattern.compile("^.*" + content + ".*$", Pattern.CASE_INSENSITIVE);
		Criteria criteria = new Criteria();
		Criteria contentMatch = new Criteria();
		criteria.andOperator(Criteria.where("tenantId").is(tenantId).and("valid").ne(false), 
				Criteria.where("roles").in(role),
				contentMatch.orOperator(Criteria.where("name").regex(pattern), 
				Criteria.where("phone").regex(pattern)));
		return mongoTemplate.find(new Query(criteria), User.class);
	}
}
