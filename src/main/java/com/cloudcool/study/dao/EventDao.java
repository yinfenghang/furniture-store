package com.cloudcool.study.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Schedule;

@Component
public class EventDao extends CRMBaseDao<Schedule> {

	public List<Schedule> create(List<Schedule> schedules) {
		return mongoTemplate.save(schedules);
	}
	
	public List<Schedule> update(List<Schedule> schedules) {
		return mongoTemplate.save(schedules);
	}
}
