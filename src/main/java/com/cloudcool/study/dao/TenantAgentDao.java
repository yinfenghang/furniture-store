package com.cloudcool.study.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.TenantAgent;

@Component
public class TenantAgentDao {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public TenantAgent create(TenantAgent agent) {
		return mongoTemplate.insert(agent);
	}
	public TenantAgent update(TenantAgent agent) {
		Query query = new Query(Criteria.where("tenantId").is(agent.getTenantId()));
		return mongoTemplate.findAndReplace(query, agent, new FindAndReplaceOptions().returnNew());
	}
	
	public TenantAgent addAgent(String tenantId, String agent) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId));
		Update update =new Update();
		update.addToSet("agent", agent);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), TenantAgent.class);
	}
	
	public TenantAgent removeAgent(String tenantId, String agent) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId));
		Update update =new Update();
		update.pull("agent", agent);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), TenantAgent.class);
	}
	
	public TenantAgent addcategory(String tenantId, String category) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId));
		Update update =new Update();
		update.addToSet("categories", category);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), TenantAgent.class);
	}
	
	public TenantAgent removeCategory(String tenantId, String category) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId));
		Update update =new Update();
		update.pull("categories", category);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), TenantAgent.class);
	}
	public TenantAgent findByTenantId(String tenantId) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId));
		return mongoTemplate.findOne(query, TenantAgent.class);
	}
}
