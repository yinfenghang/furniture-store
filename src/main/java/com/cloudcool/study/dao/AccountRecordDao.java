package com.cloudcool.study.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.AccountRecord;
import com.cloudcool.study.util.Constant;

@Component
public class AccountRecordDao {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public AccountRecord addRechargeRecord(String tenantId, double account, String mark) {
		AccountRecord record = new AccountRecord();
		record.setDirection(Constant.FLOW_IN);
		record.setSum(account);
		record.setType(Constant.ACCOUNT_RECHARGE);
		record.setTime(new Date());
		record.setRemark(mark);
		record.setTenantId(tenantId);
		return mongoTemplate.insert(record);
	}
	
	public List<AccountRecord> queryRechargeRecordPageable(String tenantId, int start, int size) {
		return mongoTemplate.find(new Query(Criteria.where("tenantId").is(tenantId).and("type").is(Constant.ACCOUNT_RECHARGE)).skip(start).limit(size), AccountRecord.class);
	}
	
	public AccountRecord addSubsribeRecord(String tenantId, double account, String mark) {
		AccountRecord record = new AccountRecord();
		record.setDirection(Constant.FLOW_OUT);
		record.setSum(account);
		record.setType(Constant.ACCOUNT_SUBSCRIBE);
		record.setTime(new Date());
		record.setRemark(mark);
		record.setTenantId(tenantId);
		return mongoTemplate.insert(record);
	}
	
	public List<AccountRecord> querySubscribeRecord(String tenantId) {
		return mongoTemplate.find(new Query(Criteria.where("tenantId").is(tenantId).and("type").is(Constant.ACCOUNT_SUBSCRIBE)), AccountRecord.class);
	}
	
	public AccountRecord addCashoutRecord(String tenantId, double account, String mark) {
		AccountRecord record = new AccountRecord();
		record.setDirection(Constant.FLOW_OUT);
		record.setSum(account);
		record.setType(Constant.ACCOUNT_CASHOUT);
		record.setTime(new Date());
		record.setRemark(mark);
		record.setTenantId(tenantId);
		return mongoTemplate.insert(record);
	}
	
	public List<AccountRecord> queryCashoutRecordPageable(String tenantId, int start, int size) {
		return mongoTemplate.find(new Query(Criteria.where("tenantId").is(tenantId).and("type").is(Constant.ACCOUNT_CASHOUT)).skip(start).limit(size), AccountRecord.class);
	}
	
	public List<AccountRecord> queryWaterbill(String tenantId) {
		return mongoTemplate.find(new Query(Criteria.where("tenantId").is(tenantId)), AccountRecord.class);
	}
	
	public List<AccountRecord> queryWaterbillPageable(String tenantId, int start, int size) {
		return mongoTemplate.find(new Query(Criteria.where("tenantId").is(tenantId)).skip(start).limit(size), AccountRecord.class);
	}

	public int countWaterbill(String tenantId) {
		List<AccountRecord> records = queryWaterbill(tenantId);
		return records.size();
	}
}
