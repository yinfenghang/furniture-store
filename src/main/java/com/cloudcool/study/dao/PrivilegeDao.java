package com.cloudcool.study.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cloudcool.study.entity.Privilege;
import com.mongodb.client.result.DeleteResult;
@Component
public class PrivilegeDao extends CRMBaseDao<Privilege> {

	public Privilege create(Privilege privilege) {
		privilege = mongoTemplate.insert(updateFilteredItem(privilege));
		return getFilteredItem(privilege);
	}

	public Privilege findByResourceId(String resourceId) {
		Privilege privilege = mongoTemplate.findOne(new Query(Criteria.where("resourceId").is(resourceId)), Privilege.class);
		return getFilteredItem(privilege);
	}
	
	public Privilege findById(String id) {
		Privilege privilege = mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)), Privilege.class);
		return getFilteredItem(privilege);
	}

	public boolean delete(Privilege privilege) {
		if (privilege == null || privilege.getId() == null || privilege.getId().isEmpty())
			return false;
		
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("_id").is(privilege.getId())), Privilege.class);
		return (result.getDeletedCount() > 0) ? true : false;
	}
	
	public Privilege update(Privilege privilege) {
		if (privilege == null || privilege.getId() == null || privilege.getId().isEmpty())
			return privilege;
		
		Query query = new Query(Criteria.where("_id").is(privilege.getId()));
		Update update = createUpdate(privilege, null);
		privilege = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Privilege.class);
		return getFilteredItem(privilege);
	}

	public Set<String> getAllCollections() {
		return mongoTemplate.getCollectionNames();
	}
}
