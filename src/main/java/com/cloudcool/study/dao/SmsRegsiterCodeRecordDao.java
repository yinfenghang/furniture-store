package com.cloudcool.study.dao;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.SmsRegisterCodeRecord;
import com.mongodb.client.result.DeleteResult;

@Component
public class SmsRegsiterCodeRecordDao extends CRMBaseDao<SmsRegisterCodeRecord> {
	public SmsRegisterCodeRecord create(SmsRegisterCodeRecord record) {
		Query query = new Query(Criteria.where("phone").is(record.getPhone()));
		SmsRegisterCodeRecord temp = mongoTemplate.findOne(query, SmsRegisterCodeRecord.class);
		if (temp == null) {
			return mongoTemplate.save(record);
		} else {
			Update update = createUpdate(record);
			return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), SmsRegisterCodeRecord.class);
		}
	}
	
	public SmsRegisterCodeRecord findByPhone(String phone) {
		return mongoTemplate.findOne(new Query(Criteria.where("phone").is(phone)), SmsRegisterCodeRecord.class);
	}
	
	public boolean removeByPhone(String phone) {
		DeleteResult result = mongoTemplate.remove(new Query(Criteria.where("phone").is(phone)), SmsRegisterCodeRecord.class);
		if (result.wasAcknowledged() && result.getDeletedCount() >= 1) {
			return true;
		}
		
		return false;
	}
}
