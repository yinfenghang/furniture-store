package com.cloudcool.study.dao;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Customer;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.ServiceResult;

@Component
public class CustomerDao extends CRMBaseDao<Customer> {

	public Customer findByIdAndTenantId(String customerId, String tenantId) {
		Query query = new Query(Criteria.where("_id").is(customerId).and("tenantId").is(tenantId).and("valid").is(true));
		return mongoTemplate.findOne(query, Customer.class);
	}
	
	public Customer create(Customer customer) {
		return mongoTemplate.insert(customer);
	}

	public Customer update(Customer customer, List<SysDataResource> limit) {
		Query query = new Query(Criteria.where("_id").is(customer.getId()).and("tenantId").is(customer.getTenantId()));
		Update update = createUpdate(customer, limit);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Customer.class);
	}

	public List<Customer> findCustomersPageable(String tenantId, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").is(true)).skip(start).limit(size);
		return mongoTemplate.find(query, Customer.class);
	}

	public List<Customer> findSelfCustomersPageable(String username, String tenantId, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("creater").is(username).and("valid").is(true)).skip(start).limit(size);
		return mongoTemplate.find(query, Customer.class);
	}

	public int countCustomers(String tenantId) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").is(true));
		List<Customer> customers = mongoTemplate.find(query, Customer.class);
		return customers.size();
	}

	public int countSelfCustomers(String username, String tenantId) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("creater").is(username).and("valid").is(true));
		List<Customer> customers = mongoTemplate.find(query, Customer.class);
		return customers.size();
	}

	public boolean invalid(String tenantId, String customer_id) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("_id").is(customer_id));
		Update update = new Update();
		update.set("valid", false);
		Customer customer = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Customer.class);
		if (customer == null) {
			return false;
		}
		
		if (customer.getValid() == true) {
			return false;
		}
		
		return true;
	}

	public List<Customer> searchByNameOrPhone(String tenantId, String content) {
		Pattern pattern = Pattern.compile("^.*" + content + ".*$", Pattern.CASE_INSENSITIVE);
		Criteria criteria = new Criteria();
		Criteria contentMatch = new Criteria();
		criteria.andOperator(Criteria.where("tenantId").is(tenantId).and("valid").ne(false), 
				contentMatch.orOperator(Criteria.where("name").regex(pattern), 
				Criteria.where("phone").regex(pattern)));
		return mongoTemplate.find(new Query(criteria), Customer.class);
	}
}
