package com.cloudcool.study.dao;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.cloudcool.study.entity.Product;
import com.cloudcool.study.entity.SysDataResource;
import com.cloudcool.study.entity.User;
import com.cloudcool.study.service.ServiceResult;

@Component
public class ProductDao extends CRMBaseDao<Product>{
	public Product create(Product product) {
		product.setValid(true);
		return mongoTemplate.insert(product);
	}

	public boolean isIdentifierExist(String identifier, String tenantId) {
		Query query = new Query(Criteria.where("identifier").is(identifier).and("tenantId").is(tenantId));
		List<Product> products = mongoTemplate.find(query, Product.class);
		if (products.size() > 0) {
			return true;
		}
		
		return false;
	}

	public Product findByIdentifierAndTenantId(String identifier, String tenantId) {
		Query query = new Query(Criteria.where("identifier").is(identifier).and("tenantId").is(tenantId).and("valid").is(true));
		return mongoTemplate.findOne(query, Product.class);
	}

	public List<Product> findProductPageable(String tenantId, int start, int size) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").is(true)).skip(start).limit(size);
		return mongoTemplate.find(query, Product.class);
	}

	public int countProduct(String tenantId) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("valid").is(true));
		List<Product> products = mongoTemplate.find(query, Product.class);
		if (products == null) {
			return 0;
		}
		return products.size();
	}

	public List<Product> searchByIdentifierOrName(String tenantId, String content) {
		Pattern pattern = Pattern.compile("^.*" + content + ".*$", Pattern.CASE_INSENSITIVE);
		Criteria criteria = new Criteria();
		Criteria contentMatch = new Criteria();
		criteria.andOperator(Criteria.where("tenantId").is(tenantId).and("valid").ne(false), 
				contentMatch.orOperator(Criteria.where("name").regex(pattern), 
				Criteria.where("identifier").regex(pattern)));
		return mongoTemplate.find(new Query(criteria), Product.class);
	}

	public boolean invalid(String tenantId, String identifier) {
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("identifier").is(identifier));
		Update update = new Update();
		update.set("valid", false);
		Product product = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Product.class);
		return !product.isValid();
	}

	public Product update(Product product, List<SysDataResource> dataResources) throws UpdateParamInvalidException, UpdateOptimisticLockException {
		if ((product.getId() == null || product.getId().isEmpty()) 
				&& (product.getIdentifier() == null || product.getIdentifier().isEmpty())) {
			throw new UpdateParamInvalidException("商品编码不能为空");
		}
		
		if (product.getVersion() == null) {
			throw new UpdateParamInvalidException("商品版本号不能为空");
		}
		
		Query query = null;
		if (product.getIdentifier() != null && !product.getIdentifier().isEmpty()) {
			query = new Query(Criteria.where("identifier").is(product.getIdentifier()).and("version").is(product.getVersion()));
		} else {
			query = new Query(Criteria.where("_id").is(product.getId()).and("version").is(product.getVersion()));
		}
		Update update = createUpdate(product, dataResources);
		int version = product.getVersion();
		product = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Product.class);
		if (product == null || version == product.getVersion()) {
			throw new UpdateOptimisticLockException("用户信息被其他用户修改，请刷新重试");
		}
		return product;
	}

	public Product storeoutByIdentifier(String tenantId, String identifier, int number) {
		if (number > 0) {
			number *= -1;
		}
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("identifier").is(identifier).and("valid").is(true));
		Update update = new Update();
		update.inc("inventory", number);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Product.class);
	}

	public Product storeinByIdentifier(String tenantId, String identifier, int number) {
		if (number < 0) {
			number *= -1;
		}
		Query query = new Query(Criteria.where("tenantId").is(tenantId).and("identifier").is(identifier).and("valid").is(true));
		Update update = new Update();
		update.inc("inventory", number);
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Product.class);
	}
}
