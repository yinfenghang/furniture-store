package com.cloudcool.study.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateValidator {

	public static boolean isBirthdayValid(Date birthday) {
		Date now = new Date();
		if (birthday.before(now)) {
			return true;
		}
		return false;
	}
	
	public static  boolean isDeliveryTimeValid(Date deliveryTime) {
		Date now = new Date();
		if (deliveryTime.after(now)) {
			return true;
		}
		
		return false;
	}
}
