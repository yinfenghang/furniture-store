package com.cloudcool.study.util;

public class EmailValidator {
	public static boolean isEmail(String email) {
		if (email == null)
			return false;

		String regex = "^[A-Za-z]{1,40}@[A-Za-z0-9]{1,40}\\.[A-Za-z]{2,3}$";
	    return email.matches(regex);
	}
}
