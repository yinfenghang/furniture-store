package com.cloudcool.study.util;

public interface Validator<T> {
	public boolean isValid(T item);
}
