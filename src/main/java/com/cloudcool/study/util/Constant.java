package com.cloudcool.study.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constant {

	/* sex */
	public static final int SEX_MAN = 0;
	public static final int SEX_WOMAN = 1;
	public static boolean isSexValid(int sex) {
		if (sex != SEX_MAN && sex != SEX_WOMAN)
			return false;
		
		return true;
	}
	
	/* package */
	public static final int PACK_DATE = 0;
	public static final int PACK_NUM = 1;
	public static boolean isPackTypeValid(int type) {
		if (type != PACK_DATE && type != PACK_NUM) {
			return false;
		}
		
		return true;
	}
	
	/* product type */
	public static final int PRODUCT_PART = 0;
	public static final int PRODUCT_SINGLE= 1;
	public static final int PRODUCT_COMBINATION = 2;
	public static boolean isProductTypeValid(int type) {
		if (type != PRODUCT_PART &&
				type != PRODUCT_SINGLE &&
				type != PRODUCT_COMBINATION) {
			return false;
		}
		
		return true;
	}
	
	/* order status */
	public static final int ORDER_INVALID = 0;
	public static final int ORDER_VERIFY = 1;
	public static final int ORDER_CASHIN = 2;
	public static final int ORDER_STOREOUT = 3;
	public static final int ORDER_DELIVERY = 4;
	public static final int ORDER_INSTALL = 5;
	public static final int ORDER_FINISH = 6;
	public static final int ORDER_MAX = ORDER_FINISH;
	public static boolean isOrderStatusValid(int status) {
		if (status <= ORDER_INVALID || status > ORDER_MAX) {
			return false;
		}
		
		return true;
	}
	
	public static final String orderStateStrs[] = {
		"invalid", "verify", "cashin", "storeout", "delivery", "install", "finish"	
	};
	public static boolean isOrderStateStrValid(String type) {
		if (type == null || type.isEmpty()) {
			return false;
		}
		
		for (int i = 1; i < orderStateStrs.length; i++) {
			if (type.equals(orderStateStrs[i])) {
				return true;
			}
		}
		
		return false;
	}
	
	public static int getOrderStateInt(String type) {
		for (int i = 0; i < orderStateStrs.length; i++) {
			if (type.equals(orderStateStrs[i])) {
				return i;
			}
		}
		
		return 0;
	}
	
	/* captical flow direction */
	public static final int FLOW_OUT = 0;
	public static final int FLOW_IN = 1;
	public static boolean isFlowValid(int flow) {
		if (flow != FLOW_OUT && flow != FLOW_IN) {
			return false;
		}
		
		return true;
	}
	
	/* account participant */
	public static final int ACCOUNT_RECHARGE = 0;
	public static final int ACCOUNT_ORDER = 1;
	public static final int ACCOUNT_SUBSCRIBE = 2;
	public static final int ACCOUNT_CASHOUT = 3;
	public static final int ACCOUNT_MAX = ACCOUNT_CASHOUT;
	public static boolean isAccountPartValid(int part) {
		if (part < 0 || part > ACCOUNT_MAX) {
			return false;
		}
		
		return true;
	}
	
	/* notifier source */
	public static final int NOTIFY_SRC_SYSTEM = 0;
	public static final int NOTIFY_SRC_TENANT = 1;
	public static final int NOTIFY_SRC_USER = 2;
	public static final int NOTIFY_SRC_MAX = NOTIFY_SRC_USER;
	public static boolean isNotifySourceValid(int source) {
		if (source < 0 || source > NOTIFY_SRC_MAX) {
			return false;
		}
		
		return true;
	}
	
	/* notifier level */
	public static final int NOTIFY_LVL_LOW = 0;
	public static final int NOTIFY_LVL_NORMAL = 1;
	public static final int NOTIFY_LVL_MID = 2;
	public static final int NOTIFY_LVL_HIGH = 3;
	public static boolean isNotifyLevelValid(int level) {
		if (level < 0 || level > NOTIFY_LVL_HIGH) {
			return false;
		}
		
		return true;
	}
	
	/* notify type */
	public static final int NOTIFY_TYPE_USER = 0;
	public static final int NOTIFY_TYPE_WARNING = 1;
	public static final int NOTIFY_TYPE_RANK = 2;
	public static final int NOTIFY_TYPE_KEY = 3;
	public static final int NOTIFY_TYPE_SMILE = 4;
	public static final int NOTIFY_TYPE_MAX = NOTIFY_TYPE_SMILE;
	public static boolean isNotifyTypeValid(int type) {
		if (type < 0 || type > NOTIFY_TYPE_MAX) {
			return false;
		}
		
		return true;
	}
	
	private static final String notifyLabel[] = {
			"fa-users text-aqua",
			"fa-warning text-yellow",
			"fa-sort-amount-desc text-red",
			"fa-key text-red",
			"fa-smile-o text-red"
	};
	public static String getNotifyLable(int type) {
		if (isNotifyTypeValid(type)) {
			return notifyLabel[type];
		}
		
		return "";
	}

	/* debug feature */
	public static final int OP_TYPE_TENANT = 0;
	public static final int OP_TYPE_ORDER = 1;
	public static final int OP_TYPE_CUSTOM = 2;
	public static final int OP_TYPE_NOTIFY = 3;
	public static final int OP_TYPE_SMS = 4;
	public static final int OP_TYPE_SCHEDULE = 5;
	public static final int OP_TYPE_USER = 6;
	public static final int OP_TYPE_MAX = 6;
	public static boolean isOperationTypeValid(int type) {
		if (type < 0 || type > OP_TYPE_MAX) {
			return false;
		}
		
		return true;
	}
	
	/* builtin role */
	public static final String ROLE_TENANT_ADMIN = "ROLE_TENANT_ADMIN";
	public static final String ROLE_SYSTEM_ADMIN = "ROLE_SYSTEM_ADMIN";
	public static final String ROLE_TENANT_USER = "ROLE_TENANT_USER";
	public static final String ROLE_TENANT_SELLER = "ROLE_TENANT_SELLER";
	public static final String ROLE_TENANT_AUDIT = "ROLE_TENANT_AUDIT";
	public static final String ROLE_TENANT_WHMANAGER = "ROLE_TENANT_WHMANAGER";
	public static final String ROLE_TENANT_DELIVERY = "ROLE_TENANT_DELIVERY";
	public static final String ROLE_TENANT_INSTALLER = "ROLE_TENANT_INSTALLER";
	public static final String ROLE_TENANT_AFTERSALE_ORDER = "ROLE_TENANT_AFTERSALE_ORDER";
	public static final String ROLE_TENANT_AFTERSALE_SERVICE = "ROLE_TENANT_AFTERSALE_SERVICE";
	public static final String ROLE_TENANT_AFTERSALE_MANAGE = "ROLE_TENANT_AFTERSALE_MANAGE";
	public static final Map<String, String> roleStr = new HashMap<String, String>(){{
			put("ROLE_TENANT_ADMIN", "租户管理");
			put("ROLE_SYSTEM_ADMIN", "系统管理");
			put("ROLE_TENANT_USER", "普通用户");
			put("ROLE_TENANT_SELLER", "导购");
			put("ROLE_TENANT_AUDIT", "订单审核");
			put("ROLE_TENANT_WHMANAGER", "库存管理");
			put("ROLE_TENANT_DELIVERY", "派送员");
			put("ROLE_TENANT_INSTALLER", "安装工");
			put("ROLE_TENANT_AFTERSALE_ORDER", "售后下单");
			put("ROLE_TENANT_AFTERSALE_SERVICE", "售后服务");
			put("ROLE_TENANT_AFTERSALE_MANAGE", "售后管理");
	}};
	public static boolean isBuiltinRole(String role) {
		if (role.equals(ROLE_TENANT_ADMIN) || role.equals(ROLE_SYSTEM_ADMIN) || role.equals(ROLE_TENANT_USER)) {
			return true;
		}
		
		return false;
	}
	public static String getRoleStr(String roleId) {
		return roleStr.get(roleId);
	}
	public static List<String> getTenantAdmin() {
		List<String> roles = new ArrayList<>();
		roles.add(ROLE_TENANT_ADMIN);
		roles.add(ROLE_TENANT_USER);
		roles.add(ROLE_TENANT_AUDIT);
		roles.add(ROLE_TENANT_WHMANAGER);
		roles.add(ROLE_TENANT_AFTERSALE_MANAGE);
		return roles;
	}
	
	/* task status */
	public static final int TASK_STATUS_COMPLETE = 0;
	public static final int TASK_STATUS_UNCOMPLETE = 1;
	public static boolean isTaskStatusValid(int status) {
		// TODO Auto-generated method stub
		if (status != TASK_STATUS_COMPLETE && status != TASK_STATUS_UNCOMPLETE) {
			return false;
		}
		
		return true;
	}
	
	/* ajax result */
    public static final int ZERO = 0;  
    public static final String SUCCESS = "success";  
    public static final String ERROR = "error";  
    public static final String OPERATE_SUCCESS = "操作成功！";  
    public static final String OPERATE_ERROR = "操作失败！";  
    public static final String NOT_PERMITTED = "权限不足！";  
    public static final String STATE1 = "1";  
    public static final String STATE2 = "2";  
    public static final int AFFECTED_LINE_0 = 0;  
    public static final int AFFECTED_LINE_1 = 1; 
    
    /* SMS sending status */
    public static final int SMS_CREATE = 0;
    public static final int SMS_SENDING = 1;
	public static final int SMS_SEND_SUCCESS = 2;
	public static final int SMS_SEND_FAILED = 3;
	public static final int SMS_RECEIVE_SUCCESS = 4;
	public static final int SMS_RECEIVE_FAILED = 5;
	public static String getSmsStsStr(int status) {
		if (status == SMS_SENDING) {
			return "smsSending";
		}
 		if (status == SMS_SEND_SUCCESS) {
			return "sendSuccess";
		}
		if (status == SMS_SEND_FAILED) {
			return "sendFailed";
		}
		if (status == SMS_RECEIVE_SUCCESS) {
			return "receiveSuccess";
		}
		if (status == SMS_RECEIVE_FAILED) {
			return "receiveFailed";
		}
		if (status == SMS_CREATE) {
			return "smsCreating";
		}
		return "invalid status";
	}
	/* SMS type */
	public static final int SMS_TYPE_REGISTER = 0;
	public static final int SMS_TYPE_DELIVERY = 1;
	public static final int SMS_TYPE_INSTALLER = 2;
	public static final int SMS_TYPE_EVENT_NOTIFY = 3;
}
