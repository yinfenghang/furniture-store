package com.cloudcool.study.util;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.cloudcool.study.entity.SysDataResource;

public class Utils {
	public static <T> List<T> filterFields(List<T> items, Map<String, Boolean> limits) {
		if (items == null || items.size() == 0) {
			return items;
		}
		
		Map<String, PropertyDescriptor> properties = new HashMap<>();
		PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(items.get(0).getClass());
		for (PropertyDescriptor property : propertyDescriptors) {
			String propertyName = property.getName();
			properties.put(propertyName, property);
		}

		for (T item : items) {
			for (String key : properties.keySet()) {
				PropertyDescriptor property = properties.get(key);
				try {
					Boolean limit = limits.get(key);
					if (limit == null || limit == Boolean.FALSE) {
						Method method = property.getWriteMethod();
						if (method != null)
							method.invoke(item, new Object[] {null});
					}
				} catch (Exception e) {
					System.out.println("Write filed " + key + " failed");
					e.printStackTrace();
				}
			}
		}
		
		return items;
	}
	
	public static <T> List<T> filterViewFields(List<T> items, Map<String, Boolean> dataResources) {
		return filterFields(items, dataResources);
	}
	
	public static <T> List<T> filterEditFields(List<T> items, Map<String, Boolean> dataResources) {
		return filterFields(items, dataResources);
	}
	
	public static <T> List<T> filterUpdateFields(List<T> items, Map<String, Boolean> dataResources) {
		return filterFields(items, dataResources);
	}
	
	public static <T> T filterViewFields(T item, Map<String, Boolean> dataResources) {
		List<T> items = new ArrayList<T>();
		items.add(item);
		return filterFields(items, dataResources).get(0);
	}
	
	public static <T> T filterEditFields(T item, Map<String, Boolean> dataResources) {
		List<T> items = new ArrayList<T>();
		items.add(item);
		return filterFields(items, dataResources).get(0);
	}
	
	public static <T> T filterUpdateFields(T item, Map<String, Boolean> dataResources) {
		List<T> items = new ArrayList<T>();
		items.add(item);
		return filterFields(items, dataResources).get(0);
	}
	
	public static String UpperPrefixChar(String resource) {
		char[] chars = resource.toCharArray();
		chars[0] = (char)(chars[0] - 'a' + 'A');
		return new String(chars);
	}
	
    public static List<Class<?>> getClasssFromPackage(String packageName) {
        List<Class<?>> clazzs = new ArrayList<>();
        // 是否循环搜索子包
        boolean recursive = false;
        // 包名对应的路径名称
        String packageDirName = packageName.replace('.', '/');
        Enumeration<URL> dirs;

        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
            while (dirs.hasMoreElements()) {

                URL url = dirs.nextElement();
                String protocol = url.getProtocol();

                if ("file".equals(protocol)) {
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    findClassInPackageByFile(packageName, filePath, recursive, clazzs);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return clazzs;
    }
    
    public static void findClassInPackageByFile(String packageName, String filePath, final boolean recursive,
            List<Class<?>> clazzs) {
		File dir = new File(filePath);
		if (!dir.exists() || !dir.isDirectory()) {
			return;
		}
		// 在给定的目录下找到所有的文件，并且进行条件过滤
		File[] dirFiles = dir.listFiles(new FileFilter() {
		
			public boolean accept(File file) {
			boolean acceptDir = recursive && file.isDirectory();// 接受dir目录
			boolean acceptClass = file.getName().endsWith("class");// 接受class文件
			return acceptDir || acceptClass;
		}
		});
		
		for (File file : dirFiles) {
			if (file.isDirectory()) {
				findClassInPackageByFile(packageName + "." + file.getName(), file.getAbsolutePath(), recursive, clazzs);
			} else {
				String className = file.getName().substring(0, file.getName().length() - 6);
				try {
					clazzs.add(Thread.currentThread().getContextClassLoader().loadClass(packageName + "." + className));
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			}
		}
}
