package com.cloudcool.study.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

public class DefaultIdGenerator {

	public static String generate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssS");
		return format.format(new Date());
	}

}
